﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Helpers;
using TruckManagementSystem.Models;
using TruckManagementSystem.ViewModels;

namespace TruckManagementSystem.Controllers
{
    public class LocationsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Locations
        public ActionResult Index()
        {
            return View(db.Location.ToList());
        }

        // GET: Locations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Location.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // GET: Locations/Create
        public ActionResult Create()
        {

            var lvm = new LocationViewModel();


            var listItems = new List<SelectListItem>
          {

           new SelectListItem { Text = "Stock Location", Value = Convert.ToString(Values.LOCATION_STOCK)},
           new SelectListItem { Text = "Non Stock Location", Value = Convert.ToString(Values.LOCATION_NON_STOCK)}
};


            lvm.location = new Location();

            lvm.LocationTypes = listItems;



            return View(lvm);
        }

        // POST: Locations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LocationViewModel model)
        {
            if (ModelState.IsValid)
            {

                
                db.Location.Add(model.location);
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            



            var listItems = new List<SelectListItem>
          {

           new SelectListItem { Text = "Stock Location", Value = Convert.ToString(Values.LOCATION_STOCK)},
           new SelectListItem { Text = "Non Stock Location", Value = Convert.ToString(Values.LOCATION_NON_STOCK)}
};




            model.LocationTypes = listItems;


            return View(model);
        }

        // GET: Locations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Location.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }


            var lvm = new LocationViewModel();

            
            lvm.location = location;

            
            var listItems = new List<SelectListItem>
          {

           new SelectListItem { Text = "Stock Location", Value = Convert.ToString(Values.LOCATION_STOCK)},
           new SelectListItem { Text = "Non Stock Location", Value = Convert.ToString(Values.LOCATION_NON_STOCK)}
};




            lvm.LocationTypes = listItems;

            return View(lvm);
        }

        // POST: Locations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LocationViewModel model)
        {
            if (ModelState.IsValid)
            {

                db.Entry(model.location).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }



            var listItems = new List<SelectListItem>
          {

           new SelectListItem { Text = "Stock Location", Value = Convert.ToString(Values.LOCATION_STOCK)},
           new SelectListItem { Text = "Non Stock Location", Value = Convert.ToString(Values.LOCATION_NON_STOCK)}
};




            model.LocationTypes = listItems;




            return View();
        }

        // GET: Locations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Location.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        // POST: Locations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Location location = db.Location.Find(id);
            db.Location.Remove(location);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
