﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Qty_Bal_Tracker
    {


        [Key]
        public int Id { get; set; }

        public int Order_Id { get; set; }

        public int Item_Id { get; set; }

        public decimal Qty_Bal { get; set; }

        public int Location_Id { get; set; }

    }
}