﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Transit_Type
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
    }
}