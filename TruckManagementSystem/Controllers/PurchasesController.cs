﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Helpers;
using TruckManagementSystem.Models;
using TruckManagementSystem.ViewModels;
using System.Globalization;
using Newtonsoft.Json;
using Microsoft.AspNet.Identity;
using System.Data.Entity;

namespace TruckManagementSystem.Controllers
{
    [Authorize]
    public class PurchasesController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: Purchases
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create_Purchase_Order()
        {

            var createvm = new LPOCreateViewModel();

            createvm.Item_List = db.Price_List.ToList();

            createvm.Vedor_List = db.Vendor.Where(x => x.Supp_Name != Values.ISSUE_NOTE_VENDOR).ToList();            

            return View(createvm);
        }

        [HttpPost]
        public ActionResult GetItemListByVendorId(int vendorid)
        {
            List<Price_List> plist = new List<Price_List>();
            plist = db.Price_List.Where(m => m.Vendor_Id == vendorid).ToList();

            var a = new List<PriceListViewModel>();

     //       var i = db.Items;

            foreach (var item in plist)
            {
                var obj = new PriceListViewModel();



                obj.Item_Id = item.Item_Id;

             

                a.Add(obj);

            }





            SelectList selectlist = new SelectList(a, "Item_Id", "Item_Name");
            return Json(selectlist);
        }


        [HttpPost]
        public ActionResult GetItemDetails(int itemid, int vendorid)
        {
            var obj = db.Price_List.Where(x => x.Item_Id == itemid).Select(x => new PriceListViewModel
            {
                Vendor_id = x.Vendor_Id,
                Item_Id = x.Item_Id,
                price = x.Buying_Price,
                Buying_Price = x.Buying_Price,
                Buying_Price_No_Tax = x.Buying_Price_No_Tax,
                Currency_Id = x.Currency,
                UoM_Id = x.UoM_Id,
                Selling_Price = x.Selling_Price,
                Part_Number = x.Part_Number
               
            }
          ).FirstOrDefault();

            return Json(obj);

        }

        [HttpPost]
        public ActionResult Create_Purchase_Order(LPOCreateViewModel model)
        {

          
            model.LPO_Header.UserName = System.Web.HttpContext.Current.User.Identity.Name;
            model.LPO_Header.CreatedAt = DateTime.Now;
            model.LPO_Header.Status = Values.STATUS_PENDING;
            model.LPO_Header.Currency = model.LPO_Details.Select(x => x.Currency).FirstOrDefault();
            model.LPO_Header.Order_Total = model.LPO_Details.Sum(x => x.Total);
            model.LPO_Header.LPO_Type = Values.LPO_FROM_DIRECT_PURCHASE;            

            db.LPO_Header.Add(model.LPO_Header);

            db.SaveChanges();

            model.LPO_Header.LPO_Number = "LP"+ model.LPO_Header.Id;

            db.SaveChanges();

            var detailslist = new List<LPO_Details>();
            foreach (var item in model.LPO_Details)
            {

                if(item.Item_Id != 0)
                {
                    item.LPO_Header_Id = model.LPO_Header.Id;

                    item.Item_Name = db.Items.Find(item.Item_Id).Item_Name;




                    detailslist.Add(item);


                }

            }



            db.LPO_Details.AddRange(detailslist);
            db.SaveChanges();


            //var createvm = new LPOCreateViewModel();

            //createvm.Item_List = db.Items.ToList();

            //createvm.Vedor_List = db.Vendor.Where(x => x.Supp_Name != Values.ISSUE_NOTE_VENDOR).ToList();

            return RedirectToAction("LPOIndex");

         //   return View(createvm);

     
        }


        [HttpGet]
        public ActionResult LPOIndex(int? page)
        {

            var list = db.LPO_Header.ToList().OrderByDescending(x => x.Id);

            int pageSize = 20;
            int pageNumber = (page ?? 1);

            return View(list.ToPagedList(pageNumber, pageSize));
        }


        public ActionResult Edit_Purchase_Order(int? idparam)
        {

            if (idparam == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            var lpoentry = db.LPO_Header.Find(idparam);
            if (lpoentry == null)
            {
                return HttpNotFound();
            }

            var lpoentrydetails = db.LPO_Details.Where(x => x.LPO_Header_Id == idparam).ToList();
         

            var createvm = new LPOCreateViewModel();

            createvm.Item_List = db.Price_List.ToList();

            createvm.LPO_Header = lpoentry;

            createvm.LPO_Details = lpoentrydetails;

            createvm.Vedor_List = db.Vendor.Where(x => x.Supp_Name != Values.ISSUE_NOTE_VENDOR).ToList();


            //if (Request.IsAjaxRequest())
            //{
            //    return RedirectToAction("Edit_Purchase_Order", "Purchases", new { idparam });
            //}
            //else
            //{

                return View("Edit_Purchase_Order", createvm);

    //        }




        }

        [HttpPost]
        public ActionResult Edit_Purchase_Order(LPOCreateViewModel model)
        {


            model.LPO_Header.UserName = System.Web.HttpContext.Current.User.Identity.Name;
            model.LPO_Header.CreatedAt = DateTime.Now;
            model.LPO_Header.Status = Values.STATUS_PENDING;
            model.LPO_Header.Currency = model.LPO_Details.Select(x => x.Currency).FirstOrDefault();
            model.LPO_Header.Order_Total = model.LPO_Details.Sum(x => x.Total);

            db.Entry(model.LPO_Header).State = System.Data.Entity.EntityState.Modified;


            var prevlpodetails = db.LPO_Details.Where(x => x.LPO_Header_Id == model.LPO_Header.Id).ToList();

            if (prevlpodetails.Count > 0)
            {
                db.LPO_Details.RemoveRange(prevlpodetails);
           }
            var detailslist = new List<LPO_Details>();
            foreach (var item in model.LPO_Details)
            {

                if (item.Item_Id != 0)
                {
                    item.LPO_Header_Id = model.LPO_Header.Id;

                    item.Item_Name = db.Items.Find(item.Item_Id).Item_Name;

                   
              

                 
                        detailslist.Add(item);
              //          db.LPO_Details.Add(item);
                    
                }

            }


        //    db.Entry(detailslist).State = System.Data.Entity.EntityState.Modified;

            db.LPO_Details.AddRange(detailslist);
            db.SaveChanges();


            //var createvm = new LPOCreateViewModel();

            //createvm.Item_List = db.Items.ToList();

            //createvm.Vedor_List = db.Vendor.Where(x => x.Supp_Name != Values.ISSUE_NOTE_VENDOR).ToList();

            return RedirectToAction("LPOIndex");


        }


        [HttpGet]
        public ActionResult Create_GRN(int id)
        {

            var LPO_Head = db.LPO_Header.Find(id);

            var lpodetails = db.LPO_Details.Where(x => x.LPO_Header_Id == id).ToList();


            var vm = new GRNViewModel();

            vm.GRN_Header = new GRN_Header();

            vm.GRN_Details = new List<GRN_Details>();

            vm.LPO_Header = new LPO_Header();

            vm.LPO_Details = new List<LPO_Details>();

      

            vm.GRN_Header.PLO_Number = LPO_Head.LPO_Number;
            vm.GRN_Header.UserName = System.Web.HttpContext.Current.User.Identity.Name;

            vm.GRN_Header.Vendor_Id = LPO_Head.Vendor_Id;
            vm.GRN_Header.Currency = LPO_Head.Currency;
            vm.GRN_Header.Order_Total = LPO_Head.Order_Total;
            vm.GRN_Header.Created_At = DateTime.Now;


            foreach(var item in lpodetails)
            {
                var detail = new GRN_Details();
             
                detail.Item_Id = item.Item_Id;
                detail.Item_Name = item.Item_Name;
                detail.Quantity_Received = item.quantity;
                detail.Price = item.Price;
                detail.quantity = item.quantity;
                detail.Total = item.Total;
                detail.Unit = item.Unit;
                detail.Currency = item.Currency;
                detail.Conversion_Unit = item.Conversion_Unit;

                vm.GRN_Details.Add(detail);


            }


            return View(vm);
        }

        [HttpPost]
        public ActionResult Create_GRN(GRNViewModel model)
        {


            if(model.GRN_Header != null && model.GRN_Details.Count > 0)
            {


            
                model.GRN_Header.UserName = System.Web.HttpContext.Current.User.Identity.Name;

    
            
                model.GRN_Header.Order_Total = model.GRN_Details.Sum(x => x.Total);


                db.GRN_Header.Add(model.GRN_Header);

                db.SaveChanges();

                model.GRN_Header.GRN_Number = "GRN" + model.GRN_Header.Id;

                db.Entry(model.GRN_Header).State = System.Data.Entity.EntityState.Modified;


                foreach (var item in model.GRN_Details)
                {

                    item.GRN_Header_Id = model.GRN_Header.Id;



                }

                db.GRN_Details.AddRange(model.GRN_Details);
                db.SaveChanges();


                AddStock(model);


                //Update LPO Status

                var lponumber = Regex.Match(model.GRN_Header.PLO_Number, @"\d+\.*\d+").Value;

                var lponumberint = Convert.ToInt32(lponumber);
                var lpoobject = db.LPO_Header.Find(lponumberint);

                lpoobject.Status = Values.STATUS_RECEIVED;


                db.Entry(lpoobject).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();


            




            }






            return RedirectToAction("GRNIndex");

        }

        public ActionResult GRNIndex(int? page)
        {

            var list = db.GRN_Header.ToList().OrderByDescending(x => x.Id);

            int pageSize = 20;
            int pageNumber = (page ?? 1);

            return View(list.ToPagedList(pageNumber, pageSize));



         
        }


        public ActionResult GRNView(int? id)
        {
            var lpo = db.GRN_Header.Where(x => x.Id == id).FirstOrDefault();


            var lpodetails = db.GRN_Details.Where(x => x.GRN_Header_Id == lpo.Id).ToList();


            var vm = new GRNViewModel();

            vm.GRN_Header = new GRN_Header();
            vm.GRN_Details = new List<GRN_Details>();

            vm.GRN_Header.Id = lpo.Id;

            vm.GRN_Header.GRN_Number = lpo.GRN_Number;

            vm.GRN_Header.PLO_Number = lpo.PLO_Number;

            vm.GRN_Header.Invoice_Number = lpo.Invoice_Number;

            vm.GRN_Header.Created_At = lpo.Created_At;

            vm.GRN_Header.UserName = lpo.UserName;

            vm.GRN_Header.Vendor_Id = lpo.Vendor_Id;

            vm.GRN_Header.Cash_Vendor_Name= lpo.Cash_Vendor_Name;

            vm.GRN_Header.VRN_No = lpo.VRN_No;

            vm.GRN_Header. Currency = lpo.Currency;

            vm.GRN_Header.Order_Total = lpo.Order_Total;

            vm.GRN_Details = lpodetails;

            // ViewBag.items = SelectItemsList();

            return View(vm);


        }

        public ActionResult PrintGRN(int? idparam)
        {


            if (idparam == null)
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            var lpoentry = db.GRN_Header.Find(idparam);
            if (lpoentry == null)
            {
                return HttpNotFound();
            }

            var lpoentrydetails = db.GRN_Details.Where(x => x.GRN_Header_Id == idparam).ToList();
            var lpoLineItemList = new List<GRN_Line_Item>();

            foreach (var item in lpoentrydetails)
            {
                var itemline = new GRN_Line_Item();


                itemline.Item_Name = item.Item_Name;
                itemline.quantity = item.quantity;
                itemline.Price = item.Price;
                itemline.Total = item.Total;
                itemline.Unit = item.Unit;
                itemline.Currency = item.Currency;

                lpoLineItemList.Add(itemline);


            }



            var repairvm = new GRNPrintViewModel();
            repairvm.Id = lpoentry.Id;
            repairvm.CreatedAt = lpoentry.Created_At;
            repairvm.GRNNumber = lpoentry.GRN_Number;
            repairvm.LPO_Number = lpoentry.PLO_Number;
            repairvm.Order_Total = lpoentry.Order_Total;
            repairvm.Vendor_Name = lpoentry.Vendor_Name;
            repairvm.Currency = lpoentry.Currency;
            repairvm.UserName = lpoentry.UserName;
            repairvm.Status = lpoentry.Status;
            repairvm.GRN_Line_Item = lpoLineItemList;


            ViewBag.Vendors = db.Vendor.ToList();


            //      return View("LPOPdf", repairvm);

            return new Rotativa.ViewAsPdf("GRNPdf", repairvm)
            {
                PageSize = Rotativa.Options.Size.A4,
                PageOrientation = Rotativa.Options.Orientation.Portrait,
                // CustomSwitches = "--disable-smart-shrinking",
                CustomSwitches = "--viewport-size 1920x1080",
                PageMargins = new Rotativa.Options.Margins(0, 2, 0, 2),
                //PageWidth = 210,
                //PageHeight = 297

                //   PageWidth = 58, // it's in millimeters
            };




        }

        public void AddStock(GRNViewModel p)
        {
         

            if (p != null)
            {
                //new Supply object using the data from the viewmodel : SendSupplyBarViewModel
                var intlocd = db.Location.Where(x => x.Name == Values.MAIN_STORE).Select(x => x.Id).FirstOrDefault();
                var Local_Fuel_Vendor = db.Vendor.Where(x => x.Supp_Name == Values.LOCAL_FUEL_VENDOR).Select(x => x.Supp_Id).FirstOrDefault();
               if(p.GRN_Header.Vendor_Id == Local_Fuel_Vendor)
                {
                    intlocd = db.Location.Where(x => x.Name == Values.LOCAL_FUEL_STORE).Select(x => x.Id).FirstOrDefault();
                }

             
                Purchases_Header supply = new Purchases_Header
                {

                    TranCreatedTime = DateTime.Now,
                    Supplier_Id = p.GRN_Header.Vendor_Id,
                    TranTotal = p.GRN_Header.Order_Total ?? 0,
                    User_Name = System.Web.HttpContext.Current.User.Identity.Name,                 

                    Location_Id = intlocd,
                    Cash_Vendor_Name = p.GRN_Header.Cash_Vendor_Name,
                    Invoice_Number = p.GRN_Header.Invoice_Number,
                    VRN_No = p.GRN_Header.VRN_No,
                    GRN_ID = p.GRN_Header.Id,

                    //IsPaid = p.IsPaid,
                   
                    Last_Updated = DateTime.Now
                };

                db.Purchases_Header.Add(supply);

                db.SaveChanges();

             
                var SuppDetails = new List<Purchases_Details>();
                var st = new List<Stock>();

                var exchangelist = db.Exchange_Rate.Where(x => x.rate_status == 1).ToList();

                var itempricelist = db.Price_List.ToList();

                int currencyid = db.Currencies.Where(x => x.Name == p.GRN_Header.Currency).Select(x => x.Id).FirstOrDefault();

                var uomobj = db.IC_UoM.ToList();

                foreach (var i in p.GRN_Details)
                {
                    Purchases_Details SuppDetailsloop = new Purchases_Details();


                    SuppDetailsloop.Purchases_Header_Id = supply.Id;
                 
                    SuppDetailsloop.Item_Id = i.Item_Id;

                    var itemcheck = itempricelist.Where(x => x.Item_Id == SuppDetailsloop.Item_Id).Select(x => x.Taxable).FirstOrDefault();

                    var pricecheck = itempricelist.Where(x => x.Item_Id == SuppDetailsloop.Item_Id).FirstOrDefault();

                    SuppDetailsloop.Item_Name = i.Item_Name;
                    SuppDetailsloop.Currency_Id = currencyid;
                    SuppDetailsloop.Currency_Name = i.Currency;

                    SuppDetailsloop.BuyPrice = i.Price;// itempricelist.Where(x => x.Item_Id == SuppDetailsloop.Item_Id).Select(x => x.Buying_Price_No_Tax).FirstOrDefault();
                    SuppDetailsloop.SellPrice = 0;
                    
                    SuppDetailsloop.SupplyQuantity = i.Quantity_Received ?? 0;
                   
                    SuppDetailsloop.Location_Id = supply.Location_Id;
                    if (SuppDetailsloop.Currency_Name == "USD")
                    {
                        SuppDetailsloop.ExchangeRate = exchangelist.Where(x => x.from_currency == SuppDetailsloop.Currency_Id).Select(x => x.rate).FirstOrDefault();
                    }
                    else
                    {
                        SuppDetailsloop.ExchangeRate = 1;
                    }

                    if (SuppDetailsloop.SellPrice != null)
                    {

                        SuppDetailsloop.SellPriceUSD = Convert.ToDecimal(SuppDetailsloop.SellPrice);
                    }
                    else
                    {
                        SuppDetailsloop.SellPriceUSD = 0;
                    }
                                                                          
                    SuppDetailsloop.Total_Cost = (SuppDetailsloop.SupplyQuantity * SuppDetailsloop.BuyPrice);

                    if (itemcheck == 1)
                    {
                        SuppDetailsloop.Net_Amount = ((SuppDetailsloop.Total_Cost ?? 0) / (db.Tax.Where(x => x.Name == "TRA").Select(x => x.Tax_Rate).FirstOrDefault() + 100)) * 100;

                        SuppDetailsloop.VAT = Math.Round((SuppDetailsloop.Total_Cost ?? 0) - (SuppDetailsloop.Net_Amount ?? 0), 2);
                                                                     
                    }
                    else
                    {
                        SuppDetailsloop.VAT = 0;

                        SuppDetailsloop.Net_Amount = SuppDetailsloop.Total_Cost;

                    }
                                                                          
                    // + (SuppDetailsloop.VAT ?? 0);


                    SuppDetails.Add(SuppDetailsloop);

                    if (i.Quantity_Received != 0)
                    {

                        Stock stock = new Stock();

                        stock.Item_Id = i.Item_Id;
                        stock.Part_Number = pricecheck.Part_Number;
                        stock.Item_Name = i.Item_Name;
                        stock.Currency_Id = currencyid;
                        stock.Currency_Name = i.Currency;
                        stock.Location_Id = supply.Location_Id;
                      
                        stock.Buy_Price = SuppDetailsloop.BuyPrice;
             
                        stock.Sell_Price = 0;
                      
                        stock.Type = "Supply";

                        stock.SellPriceUSD = SuppDetailsloop.SellPriceUSD;
                        stock.ExchangeRate = SuppDetailsloop.ExchangeRate;


                        var ItemUom = db.Price_List.Where(s => s.Item_Id == stock.Item_Id).FirstOrDefault();

                        var UoMint = ItemUom.UoM_Id;

                        //     var UomName = db.IC_UoM.Where(s => s.UoM_Id == UoMint).FirstOrDefault();

                        stock.UoM = uomobj.Where(x => x.UoM_Id == UoMint).Select(x => x.UoM).FirstOrDefault();

                        List<Stock> _checkItem = (from s in db.Stock
                                                  where s.Item_Id == stock.Item_Id && s.Location_Id == stock.Location_Id
                                                  select s).ToList();
                        decimal qtybal = 0;

                        if (_checkItem.Count > 0)
                        {
                            var supplycheck = _checkItem.Where(x => x.Item_Id == stock.Item_Id).LastOrDefault();


                            qtybal = supplycheck.Qty_Bal;
                            stock.Qty_In = i.Quantity_Received ?? 0;
                            stock.Cost_In = Math.Round((stock.Qty_In * stock.Buy_Price), 2);

                            decimal costin = supplycheck.Cost_In;
                            decimal costout = supplycheck.Cost_Out;
                            decimal costinandout = costin - costout;
                            decimal balcost = 0;
                            if (stock.Currency_Name == "TZS")
                            {
                                balcost = supplycheck.Bal_Cost_TZS;
                                stock.Bal_Cost_TZS = balcost + stock.Cost_In;
                            }
                            else if (stock.Currency_Name == "USD")
                            {
                                balcost = supplycheck.Bal_Cost_USD;
                                stock.Bal_Cost_USD = balcost + stock.Cost_In;
                            }


                            stock.Qty_Bal = qtybal + stock.Qty_In;


                            stock.Created_Date = DateTime.Now;




                            st.Add(stock);
                        }
                        else
                        {

                            stock.Qty_In = i.Quantity_Received ?? 0 ;

                            stock.Qty_Bal = stock.Qty_In;
                            stock.Cost_In = Math.Round((stock.Qty_In * stock.Buy_Price), 2);

                            if (stock.Currency_Name == "TZS")
                            {

                                stock.Bal_Cost_TZS = stock.Cost_In;
                            }
                            else if (stock.Currency_Name == "USD")
                            {

                                stock.Bal_Cost_USD = stock.Cost_In;
                            }

                            stock.Created_Date = DateTime.Now;
                            st.Add(stock);

                        }

                    }

                }

                db.Purchases_Details.AddRange(SuppDetails);

             
                
                db.SaveChanges();
            

                    //InsertSupplyTransactionDetails(SuppDetails);

                db.Stock.AddRange(st);

                db.SaveChanges();
            
              

            }

       //     return Json(new { status = status });
                            


        }



        public ActionResult Create_Fuel_Purchase_Order()
        {

            var createvm = new LPOCreateViewModel();

         
            var locid = db.Location.Where(x => x.Name == Values.LOCAL_FUEL_STORE).Select(x => x.Id).FirstOrDefault();

            createvm.Item_List = db.Price_List.Where(x=> x.Location_Id == locid).ToList();

            createvm.Vedor_List = db.Vendor.Where(x => x.Supp_Name != Values.ISSUE_NOTE_VENDOR).ToList();

            return View(createvm);
        }


        public ActionResult StoreReconciliationIndex()
        {
            var recon = new List<Reconciliation_Header>();

           
                recon = db.Reconciliation_Header.Where(x => x.Location_Id == Values.STORE_LOCATION).OrderByDescending(x => x.Id).ToList();
            
            return View(recon);
        }


        public ActionResult StoreReconciliationCreate()
        {

            var bs = new StoreReconciliationViewModel();

            bs.date = db.Reconciliation_Header.Where(x => x.Location_Id == Values.STORE_LOCATION).Select(x => x.Settlement_Date).ToList().LastOrDefault();

            return View(bs);
        }


        public ActionResult GetStock(string search)
        {

            var location = db.Location.Where(x => x.Name == Values.MAIN_STORE).Select(x => x.Id).FirstOrDefault();

            var stockobj = db.Stock.Where(X => X.Item_Name.Contains(search) || X.Part_Number.Contains(search)).Select(x => new StockLedgerViewModel()

            {

                Stock_Id = x.Stock_Id,
                Item_Name = x.Item_Name,

                Location_Id = x.Location_Id,
                Item_Id = x.Item_Id,
                Qty_Bal = x.Qty_Bal,
                Qty_In = x.Qty_In,
                settled = x.Settled

            });

            var stocklocation = stockobj.Where(l => l.Location_Id == location).ToList();

            var trackerobject = db.Qty_Bal_Tracker.ToList();

            List<int> la = new List<int>();

            foreach (var q in stocklocation.GroupBy(x => x.Item_Id))
            {

                var t = q.Select(x => new { Qty_Bal = x.Qty_Bal }).Last().Qty_Bal;

                var k = q.Select(x => x.Item_Id).LastOrDefault();

                var sum = db.Qty_Bal_Tracker.Where(x => x.Item_Id == k && x.Location_Id == location).ToList().Sum(x => x.Qty_Bal);


                if ((t - sum) <= 0)
                {
                    var p = q.Select(x => x.Item_Id).LastOrDefault();

                    la.Add(p);
                }

            }



            var barItems = stockobj.Where(t => !la.Contains(t.Item_Id) && t.Location_Id == location).ToList();



            var groupedList = new List<Stock>();


            foreach (var item in barItems.GroupBy(x => x.Item_Id))
            {
                var tempstock = new Stock();

                var stockcheck = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Id).FirstOrDefault();
                if (stockcheck != 0)
                {



                    tempstock.Stock_Id = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Stock_Id).FirstOrDefault();
                    tempstock.Item_Id = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Id).FirstOrDefault();
                    tempstock.Item_Name = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Name).FirstOrDefault();


                    groupedList.Add(tempstock);
                }
                else
                {
                    tempstock.Stock_Id = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Stock_Id).LastOrDefault();
                    tempstock.Item_Id = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Item_Id).LastOrDefault();
                    tempstock.Item_Name = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Item_Name).LastOrDefault();




                    groupedList.Add(tempstock);

                }
            }


            var json = string.Empty;

            var result = new List<Dictionary<string, string>>();


            var barItem = new[] { new { label = "", value = "" } }.AsEnumerable();

            var culture = CultureInfo.CurrentCulture;


            barItem = groupedList.Where(x => culture.CompareInfo.IndexOf(x.Item_Name ?? "", search, CompareOptions.IgnoreCase) >= 0).ToList().Select(x => new { label = x.Item_Name, value = x.Item_Id.ToString() + "," + x.Stock_Id.ToString() });


        
            return Json(barItem.Take(15), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetStockDetails(string id)
        {

            string[] stringarr = id.Split(',');
            int itemid = 0;
            int stockid = 0;
            int pricelistid = 0;



            var location = db.Location.Where(x => x.Name == Values.MAIN_STORE).Select(x => x.Id).FirstOrDefault();


            if (id.ToLower().Contains(','))
            {
                itemid = Convert.ToInt32(stringarr[0]);


                var isNumeric = int.TryParse(stringarr[1], out int n);

                if (isNumeric)
                {

                    stockid = Convert.ToInt32(stringarr[1]);
                }
                else
                {
                    stockid = 0;

                    string extract = Regex.Replace(stringarr[1], "[^0-9]+", string.Empty);

                    pricelistid = Convert.ToInt32(extract);

                }
            }
            else
            {
                itemid = Convert.ToInt32(id);

            }



            var fleetobjectbefore = db.Stock.Where(x => x.Stock_Id == stockid).FirstOrDefault();


            var fleetobject = new StockDetailsViewModel();

            if (fleetobjectbefore != null)
            {

                fleetobject.Item_Name = fleetobjectbefore.Item_Name;

                fleetobject.Item_Id = fleetobjectbefore.Item_Id;

                fleetobject.Stock_Id = fleetobjectbefore.Stock_Id;

                fleetobject.Part_Number = fleetobjectbefore.Part_Number;
                fleetobject.Unit = fleetobjectbefore.UoM;


                fleetobject.Qty_Bal = db.Stock.Where(y => y.Item_Id == fleetobjectbefore.Item_Id && y.Location_Id == location).OrderByDescending(y => y.Stock_Id).Select(y => y.Qty_Bal).Take(1).FirstOrDefault() - (db.Qty_Bal_Tracker.Where(y => y.Item_Id == fleetobjectbefore.Item_Id && y.Location_Id == location).ToList().Sum(y => (int?)y.Qty_Bal) ?? 0);


                fleetobject.Buy_Price = fleetobjectbefore.Buy_Price;

                if (fleetobjectbefore.Qty_Bal > 0)
                {

                    fleetobject.Vendor_Id = db.Vendor.Where(x => x.Supp_Name == "Issue Note").Select(x => x.Supp_Id).FirstOrDefault();
                }
                else
                {



                }
            }
            else
            {

                var priceobjectbefore = db.Price_List.Find(pricelistid);


                fleetobject.Item_Name = priceobjectbefore.Item_Name;

                fleetobject.Stock_Id = 0;

                fleetobject.Item_Id = priceobjectbefore.Item_Id;

                fleetobject.Qty_Bal = 0;

                string uomname = db.IC_UoM.Find(priceobjectbefore.UoM_Id).UoM;

                fleetobject.Unit = uomname;

                fleetobject.Part_Number = priceobjectbefore.Part_Number;


                fleetobject.Buy_Price = priceobjectbefore.Buying_Price;

                fleetobject.Vendor_Id = priceobjectbefore.Vendor_Id;





            }

            return Json(fleetobject, JsonRequestBehavior.AllowGet);

        }



        [HttpPost]
        public ActionResult StoreReconciliationCreate(ReconciliationViewModel model)
        {

            var a = 1;


            var reconhead = new Reconciliation_Header();

            reconhead.Created_Date = DateTime.Now;

            reconhead.Settlement_Date = DateTime.Now;

            reconhead.User_Id = User.Identity.GetUserId();


            reconhead.Location_Id = Values.STORE_LOCATION;

            db.Reconciliation_Header.Add(reconhead);


            db.SaveChanges();

            var list = new List<Reconciliation_Details>();

            foreach (var item in model.Line_item)
            {

                var detail = new Reconciliation_Details();

                detail.Item_Id = item.Item_Id;
                detail.Item_name = item.Item_name;
                detail.Used_Quantity = item.Used_Quantity;
                detail.Buy_Price = item.Buy_Price;
                detail.Currency = item.Currency;
                detail.UoM = item.UoM;
                detail.StockID = item.StockID;
                detail.Remarks = item.Remarks;
                detail.ReconcilliationHeaderId = reconhead.Id;
                detail.Initial_Quantity = item.Initial_Quantity;
                detail.Amount = item.Amount;
                detail.Balance = (item.Initial_Quantity - item.Used_Quantity);

                list.Add(detail);
            }


            db.Reconciliation_Details.AddRange(list);
            db.SaveChanges();

            return View("");
        
        
        }

        [HttpGet]
        public ActionResult StoreReconciliationEdit(int id)
        {

            var reconhead = db.Reconciliation_Header.Find(id);

            var recondetails = db.Reconciliation_Details.Where(x => x.ReconcilliationHeaderId == reconhead.Id).ToList();


            var vm = new ReconciliationViewModel();

            vm.Created_Date = reconhead.Created_Date;
            vm.Id = reconhead.Id;
            vm.Status_Int = reconhead.Status;
            vm.UserName = reconhead.Username;
            vm.Location = reconhead.LOcation_String;
            vm.Line_item = recondetails;

            return View(vm);


        }

        [HttpPost]
        public ActionResult StoreReconciliationEdit(ReconciliationViewModel model)
        {


            var details = db.Reconciliation_Details.Where(x => x.ReconcilliationHeaderId == model.Id).ToList();

            using (var dbContextTransaction = db.Database.BeginTransaction())
            {


                try
                {



                    var locationid = Values.STORE_LOCATION;


                    //  var date = Convert.ToDateTime(formzCollection["Date"]);

                   

                    var reconhead = db.Reconciliation_Header.Find(model.Id);

                    var recondetails = db.Reconciliation_Details.Where(x => x.ReconcilliationHeaderId == model.Id).ToList();


                    var sti = new List<Stock>();

                    var uomobj = db.IC_UoM.ToList();

                    var currencyobj = db.Currencies.ToList();


                    foreach (var item in recondetails)
                    {
                        if (item.Used_Quantity != 0)
                        {

                            Stock stock = new Stock();

                            stock.Stock_Id = (item.StockID ?? 0);
                            stock.Item_Id = item.Item_Id;
                            stock.Item_Name = item.Item_name;
                            stock.Buy_Price = (item.Buy_Price ?? 0);
                            stock.Type = "Main Store Reconciliation";
                            stock.Sell_Price = item.Sell_Price;
                            stock.Currency_Name = item.Currency;
                            stock.Currency_Id = currencyobj.Where(x => x.Name.Trim() == item.Currency).Select(x => x.Id).FirstOrDefault();
                            stock.Location_Id = locationid;


                            var ItemUom = db.Price_List.Where(s => s.Item_Id == stock.Item_Id).First();

                            var UoMint = ItemUom.UoM_Id;

                            //    var UomName =  uom  db.IC_UoM.Where(s => s.UoM_Id == UoMint).First();

                            stock.UoM = uomobj.Where(x => x.UoM_Id == UoMint).Select(x => x.UoM).FirstOrDefault();

                            List<Stock> _checkItem = (from s in db.Stock
                                                      where s.Item_Id == stock.Item_Id && s.Location_Id == locationid
                                                      select s).ToList();
                            decimal qtybal = 0;

                            if (_checkItem.Count > 0)
                            {
                                var supplycheck = _checkItem.Where(x => x.Item_Id == stock.Item_Id).Last();

                                qtybal = supplycheck.Qty_Bal;
                                stock.Qty_Out = item.Used_Quantity;
                                stock.Cost_Out = Math.Round((stock.Qty_Out * stock.Buy_Price), 2);
                                stock.Sent_Location = stock.Location_Id;
                                decimal balcost = 0;

                                if (stock.Currency_Name == "TZS")
                                {
                                    balcost = supplycheck.Bal_Cost_TZS;
                                    stock.Bal_Cost_TZS = supplycheck.Bal_Cost_TZS - stock.Cost_Out;
                                }
                                else if (stock.Currency_Name == "USD")
                                {
                                    balcost = supplycheck.Bal_Cost_USD;
                                    stock.Bal_Cost_USD = supplycheck.Bal_Cost_USD - stock.Cost_Out;
                                }

                                stock.Qty_Bal = qtybal - stock.Qty_Out;


                                if (stock.Qty_Bal < 0)
                                {

                                    dbContextTransaction.Rollback();

                                    return Json(new { response = false, message = stock.Item_Name.ToString() + " will be less than zero. Current stock is ( " + qtybal.ToString() + " )" });

                                }



                                stock.Created_Date = DateTime.Now;
                                sti.Add(stock);


                            }
                            else
                            {

                                stock.Qty_Out = item.Used_Quantity;

                                stock.Qty_Bal = 0;
                                stock.Cost_Out = Math.Round((stock.Qty_Out * stock.Buy_Price), 2);

                                if (stock.Currency_Name == "TZS")
                                {

                                    stock.Bal_Cost_TZS = stock.Cost_In - stock.Cost_Out;
                                }
                                else if (stock.Currency_Name == "USD")
                                {

                                    stock.Bal_Cost_USD = stock.Cost_In - stock.Cost_Out;
                                }



                                stock.Created_Date = DateTime.Now;
                                sti.Add(stock);

                            }


                            var FIFO = db.Stock.Find(stock.Stock_Id);

                            var quant = (FIFO.FIFO_Quantity ?? 0) + stock.Qty_Out;
                            var diffsupply = FIFO.Qty_In - quant;
                            if (diffsupply > 0)
                            {
                                FIFO.FIFO_Quantity = quant;
                                db.Entry(FIFO).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            else
                            {

                                FIFO.FIFO_Quantity = FIFO.Qty_In;

                                FIFO.Settled = 1;
                                db.Entry(FIFO).State = EntityState.Modified;
                                db.SaveChanges();


                                var stop = false;

                                while (stop == false)
                                {
                                    var fifonew = db.Stock.Where(x => x.Location_Id == stock.Location_Id && x.Qty_In != 0 && x.Settled == 0 && x.Item_Id == stock.Item_Id).FirstOrDefault();
                                    if (fifonew != null)
                                    {
                                        var quant1 = (fifonew.FIFO_Quantity ?? 0) + Math.Abs(diffsupply);
                                        var diff1 = fifonew.Qty_In - quant1;
                                        if (diff1 > 0)
                                        {
                                            fifonew.FIFO_Quantity = quant1;
                                            db.Entry(fifonew).State = EntityState.Modified;
                                            db.SaveChanges();

                                            stop = true;

                                        }
                                        else
                                        {
                                            fifonew.FIFO_Quantity = fifonew.Qty_In;

                                            fifonew.Settled = 1;
                                            db.Entry(fifonew).State = EntityState.Modified;
                                            db.SaveChanges();

                                            diffsupply = Math.Abs(diff1);

                                        }
                                    }
                                    else
                                    {
                                        stop = true;
                                    }

                                }

                            }

                        }


                    }



                    db.Stock.AddRange(sti);
                    db.SaveChanges();


                    string user = System.Web.HttpContext.Current.User.Identity.GetUserId();

                    reconhead.Approve_User_Id = user;

                    reconhead.Status = Values.STATUS_APPROVED;

                    db.Entry(reconhead).State = EntityState.Modified;

                    db.SaveChanges();



                    dbContextTransaction.Commit();

                    return RedirectToAction("StoreReconciliationIndex");


                }
                catch (Exception ex)
                {

                    dbContextTransaction.Rollback();
                    Helpers.Helpers.TraceService(ex.Message + "    INNER EXCEPTION " + ex.InnerException + "   STACK TRACE  " + ex.StackTrace);

                    return View();

                }

            }




           
        }
    }
}