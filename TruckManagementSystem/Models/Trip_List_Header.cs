﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Trip_List_Header
    {

        [Key]
        public int Id { get; set; }
        public DateTime Created_Date { get; set; }

        public string Created_By { get; set; }

        public int  Transit_Type {get; set;}

        public string Transit_Type_String
        {

            get
            {

                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    string TransitType = db.Transit_Type.Where(x => x.Id == Transit_Type).Select(x => x.Name).FirstOrDefault();

                    return TransitType;


                }


            }


        }

    }
}