﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Trucks
    {

        [Key]
        public int Id { get; set; }

        public string Reg_No { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public int Type { get; set; }

    }
}