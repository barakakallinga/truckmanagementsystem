﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.ViewModels
{
    public class GRNPrintViewModel
    {

        public int Id { get; set; }



        public string UserName { get; set; }

        public DateTime CreatedAt { get; set; }

        public string GRNNumber { get; set; }

        public string LPO_Number { get; set; }

        public string Vendor_Name { get; set; }

        public decimal? Order_Total { get; set; }

        public int Status { get; set; }

        public string Currency { get; set; }


        public List<GRN_Line_Item> GRN_Line_Item { get; set; }


    }
}