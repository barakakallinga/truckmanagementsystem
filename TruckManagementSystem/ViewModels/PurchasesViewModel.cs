﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.ViewModels
{
    public class PurchasesViewModel
    {

        public Purchases_Header Purchases_Header { get; set; }

        public List<Purchases_Details> Purchases_Details { get; set; }

    }
}