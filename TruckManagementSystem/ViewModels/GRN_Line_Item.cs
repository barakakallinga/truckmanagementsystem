﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.ViewModels
{
    public class GRN_Line_Item
    {

        public string Item_Name { get; set; }

        public decimal quantity { get; set; }

        public int GRN_Header_Id { get; set; }

        public string Unit { get; set; }

        public Decimal Price { get; set; }

        public string Currency { get; set; }

        public Decimal Total { get; set; }

    }
}