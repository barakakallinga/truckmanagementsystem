﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.ViewModels
{
    public class SparesAndExpensesViewModel
    {

        public int Id { get; set; }
         
        public DateTime Date { get; set; }

        public string Fleet_No { get; set; }

        public string Truck_No { get; set; }

        public string Trailer_No { get; set; }


        public string Transit_Type { get; set; }

        public string Job_Card_No { get; set; }

        public string Order_No { get; set; }
        
        public string Item_Name { get; set; }

        public decimal Qty { get; set; }

        public decimal Rate { get; set; }

        public decimal Amount { get; set; }

        public string Supplier
        {
            get

            {

                using (var db = new ApplicationDbContext())
                {
                    string name = db.Vendor.Where(x => x.Supp_Id == Supplier_Int).Select(x => x.Supp_Name).FirstOrDefault();

                    return name;
                }




            }



        }

        public int Supplier_Int { get; set; }

        public string Remarks { get; set; }
    }
}