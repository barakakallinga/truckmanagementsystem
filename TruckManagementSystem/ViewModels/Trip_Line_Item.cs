﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.ViewModels
{
    public class Trip_Line_Item
    {


        public string Fleet_No { get; set; }

        public string Truck_Head { get; set; }

        public string Truck_Trailer { get; set; }

        public string Driver_Name { get; set; }

        public string Licence_No { get; set; }

        public string Product { get; set; }

        public string Loading_Point { get; set; }
        

        public string Mobile_No { get; set; }
        public decimal Requested_Qty { get; set; }

        public string  Unit { get; set; }

        public string Destination { get; set; }

    }
}