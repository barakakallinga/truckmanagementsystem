﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.ViewModels
{
    public class RepairViewModel
    {

        public string Make { get; set; }
        public int Header_Id { get; set; }
        public string Fleet_No { get; set; }

        public string Truck_Head_No { get; set; }

        public string Truck_Trailer_No { get; set; }

        public string Driver_Name { get; set; }

        public string Created_Date { get; set; }

        public string Licence_No { get; set; }

        public string Message { get; set; }

        public List<Repair_Line_Item> Line_Item { get; set; }


        public int Status { get; set; }

        public string Job_Card_Reference_No { get; set; }

        public decimal? Kilometers { get; set; }



    }
}