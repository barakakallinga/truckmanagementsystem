﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Helpers;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.Controllers
{
    [Securities.RolesAuthorization(Roles = Values.ADMIN_ROLE + "," + Values.TRANSPORT_OFFICER_ROLE)]

    public class FleetsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Fleets
        public ActionResult Index(int? page, string searchString, string currentFilter)
        {

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            int pageSize = 20;
            int pageNumber = (page ?? 1);

            var fleets = new List<Fleets>();
            if (searchString != null)
            {
                fleets = db.Fleets.Where(x => x.Fleet_No.Contains(searchString)).ToList();

            }
            else
            {
                fleets = db.Fleets.ToList();
            }


            return View(fleets.OrderByDescending(x => x.Id).ToList().ToPagedList(pageNumber, pageSize));


        }

        // GET: Fleets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fleets fleets = db.Fleets.Find(id);
            if (fleets == null)
            {
                return HttpNotFound();
            }
            return View(fleets);
        }

        // GET: Fleets/Create
        public ActionResult Create()
        {

            int head = db.Truck_Type.Where(x => x.Name.Equals("Hose")).Select(x => x.Id).FirstOrDefault();

            int trailer = db.Truck_Type.Where(x => x.Name.Equals("Trailer")).Select(x => x.Id).FirstOrDefault();




            ViewBag.TruckHeads = new SelectList(db.Trucks.Where(x => x.Type == head), "Id", "Reg_No");

            ViewBag.TruckTrailers = new SelectList(db.Trucks.Where(x => x.Type == trailer), "Id", "Reg_No");

            ViewBag.Transit_Types = new SelectList(db.Transit_Type, "Id", "Name");



            return View();

        }

        // POST: Fleets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Fleet_No,Truck_Head_Id,Truck_Trailer_Id,Transit_Type")] Fleets fleets)
        {
            if (ModelState.IsValid)
            {
                db.Fleets.Add(fleets);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(fleets);
        }

        // GET: Fleets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int head = db.Truck_Type.Where(x => x.Name.Equals("Hose")).Select(x => x.Id).FirstOrDefault();

            int trailer = db.Truck_Type.Where(x => x.Name.Equals("Trailer")).Select(x => x.Id).FirstOrDefault();




        

            Fleets fleets = db.Fleets.Find(id);
            if (fleets == null)
            {
                return HttpNotFound();
            }

            ViewBag.TruckHeads = new SelectList(db.Trucks.Where(x => x.Type == head), "Id", "Reg_No", fleets.Truck_Head_Id);

            ViewBag.TruckTrailers = new SelectList(db.Trucks.Where(x => x.Type == trailer), "Id", "Reg_No", fleets.Truck_Trailer_Id);

            ViewBag.Transit_Types = new SelectList(db.Transit_Type, "Id", "Name",fleets.Transit_Type);


            return View(fleets);
        }

        // POST: Fleets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Fleet_No,Truck_Head_Id,Truck_Trailer_Id,Transit_Type")] Fleets fleets)
        {
            if (ModelState.IsValid)
            {
                db.Entry(fleets).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(fleets);
        }

        // GET: Fleets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Fleets fleets = db.Fleets.Find(id);
            if (fleets == null)
            {
                return HttpNotFound();
            }
            return View(fleets);
        }

        // POST: Fleets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Fleets fleets = db.Fleets.Find(id);
            db.Fleets.Remove(fleets);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
