﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    [Table("IC_UoM")]
    public class IC_UoM
    {


        [Key]

        public int UoM_Id { get; set; }
        public string UoM { get; set; }
        public int Conversion { get; set; }

    }
}