﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace TruckManagementSystem.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {

            Database.SetInitializer(new NullDatabaseInitializer<ApplicationDbContext>());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }


        public DbSet<Trucks> Trucks { get; set; }

        public DbSet<Truck_Type> Truck_Type { get; set; }

        public DbSet<Truck_Status> Truck_Status { get; set; }

         public DbSet<Fleets> Fleets { get; set; }

        public DbSet<Transit_Type> Transit_Type { get; set; }

        public DbSet<Maintenance_Book_Header> Maintenance_Book_Header { get; set; }

        public DbSet<Maintenance_Book_Details> Maintenance_Book_Details { get; set; }

        public DbSet<IC_UoM> IC_UoM { get; set; }

        public DbSet<Items> Items { get; set; }

        public DbSet<Stock> Stock { get; set; }

        public DbSet<Job_Card_Header> Job_Card_Header { get; set; }

        public DbSet<Job_Card_Details> Job_Card_Details { get; set; }

        public DbSet<Vendor> Vendor { get; set; }

        public DbSet<Currency> Currencies { get; set; }

        public DbSet<LPO_Header> LPO_Header { get; set; }

        public DbSet<LPO_Details> LPO_Details { get; set; }

        public DbSet<Payment_Header> Payment_Header { get; set; }

        public DbSet<Payment_Details> Payment_Details { get; set; }



        public DbSet<Issue_Note_Header> Issue_Note_Header { get; set; }

        public DbSet<Issue_Note_Details> Issue_Note_Details { get; set; }

        public DbSet<Price_List> Price_List { get; set; }

        public DbSet<GRN_Header> GRN_Header { get; set; }

        public DbSet<GRN_Details> GRN_Details { get; set; }

        public DbSet<Purchases_Header> Purchases_Header { get; set; }

        public DbSet<Purchases_Details> Purchases_Details { get; set; }

        public DbSet<Location> Location { get; set; }
        public DbSet<Exchange_Rate> Exchange_Rate { get; set; }

        public DbSet<Tax> Tax { get; set;  }

        public DbSet<Qty_Bal_Tracker> Qty_Bal_Tracker { get; set; }

        public DbSet<Trip_List_Header> Trip_List_Header { get; set; }

        public DbSet<Trip_List_Details> Trip_List_Details { get; set; }

        public DbSet<Products> Products { get; set; }

        public DbSet<Trip_Details> Trip_Details { get; set; }

        public DbSet<Categories> Categories { get; set; }

        public DbSet<Reconciliation_Header> Reconciliation_Header { get; set; }

        public DbSet<Reconciliation_Details> Reconciliation_Details { get; set; }
    }
}