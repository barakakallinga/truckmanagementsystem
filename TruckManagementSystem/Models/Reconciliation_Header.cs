﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TruckManagementSystem.Helpers;

namespace TruckManagementSystem.Models
{
    public class Reconciliation_Header
    {

        [Key]
        public int Id { get; set; }

        [DisplayName("Settlement Date")]
        public DateTime Settlement_Date { get; set; }

        [DisplayName("Created Date")]
        public DateTime Created_Date { get; set; }
        [DisplayName("Location")]
        public int Location_Id { get; set; }
        [DisplayName("User")]
        public string User_Id { get; set; }

        public string Approve_User_Id { get; set; }

        public int Status { get; set; }



        public string Username
        {
            get
            {



                using (var db = new ApplicationDbContext())
                {

                    var u = db.Users.Find(User_Id).UserName;

                    return u;
                }

            }

        }

        public string Approved_Username
        {

            get
            {
                using (var db = new ApplicationDbContext())
                {
                    if (Approve_User_Id != null)
                    {
                        var u = db.Users.Find(Approve_User_Id).UserName;

                        return u;
                    }

                    return "";
                }

            }

        }

        public string Status_String
        {
            get
            {
                var s = Values.StatusString(Status);

                return s;
            }
        }

        public string LOcation_String
        {
            get
            {
                using (var db = new ApplicationDbContext())
                {
                    if (Location_Id != 0)
                    {

                        var u = db.Location.Find(Location_Id).Name;

                        return u;
                    }

                    return "";

                }
            }
        }


    }
}