﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using TruckManagementSystem.Helpers;

namespace TruckManagementSystem.Models
{
    public class LPO_Header
    {
        [Key]
        public int Id { get; set; }
         


        public string UserName { get; set; }

        public DateTime CreatedAt { get; set; }

        public string JOBNumber { get; set; }

        public string LPO_Number { get; set; }

        public int Vendor_Id { get; set; }

        public decimal? Order_Total { get; set; }

        public int Status { get; set; }

        public string Currency { get; set; }


        public int LPO_Type { get; set;}

        public string Status_String
        {

            get
            {
                return Values.STATUS_STRING(Status);

            }

        }

        public string Job_Number_String
        {


            get
            {
                if(!string.IsNullOrEmpty(JOBNumber))
                {
                    return "JB" + JOBNumber;
                }else
                {
                    return "Direct";
                }

                 

            }

        }

        public string Vendor_Name
        {

            get
            {

                using(var db = new ApplicationDbContext())
                {
                    string name = db.Vendor.Where(x => x.Supp_Id == Vendor_Id).Select(x => x.Supp_Name).FirstOrDefault();

                        return name;
                }

            }
        }

    }
}