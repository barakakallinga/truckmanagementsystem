﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Purchases_Details
    {

        [Key]
        public int Id { get; set; }
        public int Purchases_Header_Id { get; set; }


        public decimal PTranCost { get; set; }

        public string ReferenceNo { get; set; }


        public int Item_Id { get; set; }

        public string Item_Name { get; set; }

        public int Currency_Id { get; set; }


    
  
        public decimal BuyPrice { get; set; }


   
        public decimal? SellPrice { get; set; }
        public string Barcode { get; set; }
        public decimal SupplyQuantity { get; set; }
        public string Currency_Name { get; set; }

        public int Stock_Id { get; set; }

        public int Location_Id { get; set; }
        public DateTime? ExpiryDate { get; set; }
        public Decimal BuyPriceUSD { get; set; }

        public Decimal SellPriceUSD { get; set; }

        public decimal ExchangeRate { get; set; }

        public decimal? VAT { get; set; }

        public decimal? Net_Amount { get; set; }

        public decimal? Total_Cost { get; set; }

        public string Unit { get; set; }

    }
}