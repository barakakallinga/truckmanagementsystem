﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using TruckManagementSystem.Helpers;

namespace TruckManagementSystem.Models
{
    [Table("Location")]
    public class Location
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }

        [Display(Name = "Location Type")]
        public int? Location_Type { get; set; }

        [Display(Name = "Location Type")]
        public string Location_Type_String
        {
            get
            {
                var s = string.Empty;
                if (Location_Type == Values.LOCATION_STOCK)
                {
                    s = "Stock Location";
                }
                else
                {
                    s = "NON Stock Location";
                }

                return s;
            }
        }

    }
}