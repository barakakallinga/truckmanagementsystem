﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.Controllers
{
    public class IC_UoMController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: IC_UoM
        public ActionResult Index()
        {
            return View(db.IC_UoM.ToList());
        }

        // GET: IC_UoM/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IC_UoM iC_UoM = db.IC_UoM.Find(id);
            if (iC_UoM == null)
            {
                return HttpNotFound();
            }
            return View(iC_UoM);
        }

        // GET: IC_UoM/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: IC_UoM/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UoM_Id,UoM,Conversion")] IC_UoM iC_UoM)
        {
            if (ModelState.IsValid)
            {
                db.IC_UoM.Add(iC_UoM);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(iC_UoM);
        }

        // GET: IC_UoM/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IC_UoM iC_UoM = db.IC_UoM.Find(id);
            if (iC_UoM == null)
            {
                return HttpNotFound();
            }
            return View(iC_UoM);
        }

        // POST: IC_UoM/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UoM_Id,UoM,Conversion")] IC_UoM iC_UoM)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iC_UoM).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(iC_UoM);
        }

        // GET: IC_UoM/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IC_UoM iC_UoM = db.IC_UoM.Find(id);
            if (iC_UoM == null)
            {
                return HttpNotFound();
            }
            return View(iC_UoM);
        }

        // POST: IC_UoM/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            IC_UoM iC_UoM = db.IC_UoM.Find(id);
            db.IC_UoM.Remove(iC_UoM);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
