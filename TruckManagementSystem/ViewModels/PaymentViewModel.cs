﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.ViewModels
{
    public class PaymentViewModel
    {

        public int Id { get; set; }


        public string Order_no { get; set; }
        public string UserName { get; set; }

        public DateTime CreatedAt { get; set; }

        public string JOBNumber { get; set; }

        public int Trip_List_detail_Id { get; set; }

        public string Payment_Number { get; set; }

        public string Vendor_Name { get; set; }

        public decimal? Order_Total { get; set; }

        public int Status { get; set; }

        public string Currency { get; set; }


        public List<Payment_Line_Item> Payment_Line_Item { get; set; }
    }
}