﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.ViewModels
{
    public class PriceListViewModel
    {



        public decimal price { get; set; }

        public int Item_Id { get; set; }



        public int? Taxable { get; set; }

        public int UoM_Id { get; set; }

        public int? Currency_Id { get; set; }

        public decimal Buying_Price_No_Tax { get; set; }

        public decimal Buying_Price { get; set; }

        public decimal? Selling_Price { get; set; }

        public string SubCategory { get; set; }

        public string Location { get; set; }

        public int Vendor_id { get; set; }

        public string Taxable_String
        {

            get
            {

                if (Taxable == 1)
                {
                    return "Taxable";

                }
                else
                {
                    return "Not Taxable";
                }


            }



        }

        public string Unit_Of_Measure
        {
            get
            {
                var db = new ApplicationDbContext();

                var k = db.IC_UoM.Find(UoM_Id).UoM;

                return k;

            }

        }

        public string Vendor_Name
        {
            get
            {
                var db = new ApplicationDbContext();

                var k = db.Vendor.Find(Vendor_id).Supp_Name;

                return k;

            }

        }


        public string currency
        {
            get
            {
                var db = new ApplicationDbContext();
                if (Currency_Id != null)
                {
                    var k = db.Currencies.Find(Currency_Id).Name;

                    return k;
                }
                else
                {
                    return "";
                }
            }

        }

        public string Item_Name
        {
            get
            {
                var db = new ApplicationDbContext();

                var k = db.Items.Find(Item_Id).Item_Name;

                return k;

            }

        }

        public string Part_Number { get; set; }
    }
}