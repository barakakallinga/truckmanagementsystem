﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.Helpers
{
    public static class Values
    {

        public const int  STATUS_PENDING = 0;

        public const int STATUS_WAITING_FOR_OPERATIONS_MANAGER = 1;

        public const int STATUS_DOES_NOT_EXIST_IN_STOCK = 2;

        public const int STATUS_IS_IN_STOCK = 3;

        public const int STATUS_IS_OUT_OF_STOCK = 4;

        public const int STATUS_WAITING_FOR_TRANSPORT_MANAGER = 5;

        public const int STATUS_WAITING_FOR_ACCOUNTANT = 6;

        public const int STATUS_WAITING_FOR_COUNTRY_MANAGER = 7;

        public const int STATUS_PENDING_JOB_CARD = 8;

        public const int STATUS_JOB_CARD_HAS_PENDING_LPOS = 9;

        public const int STATUS_ISSUE_NOTE = 9;

        public const int STATUS_LPO = 10;

        public const int STATUS_SENT_BACK_FOR_EDIT = 11;

        public const int STATUS_PETTY_CASH_VOUCHER = 12;

        public const int LOCATION_STOCK = 13;

        public const int LOCATION_NON_STOCK = 14;

        public const int LPO_FROM_DIRECT_PURCHASE = 15;

        public const int LPO_FROM_MAINTENANCE = 16;

        public const int STATUS_RECEIVED = 17;

        public const int STATUS_ISSUED = 18;

        public const int STATUS_PAID = 19;

        public const int STATUS_COMPLETED_JOBCARD = 20;

        public const int ISSUE_NOTE_MAINTENANCE = 21;

        public const int ISSUE_NOTE_TRIP_FUEL = 22;

        public const int STATUS_APPROVED = 23;





        public static string STATUS_STRING(int Status)
        {

            if(Status == STATUS_PENDING)
            {


                return "Waiting For Workshop Manager";

            }else if(Status == STATUS_WAITING_FOR_OPERATIONS_MANAGER)
            {

                return "Waiting For Operations Manager";
            }else if(Status == STATUS_WAITING_FOR_TRANSPORT_MANAGER)
            {
                return "Waiting for Transport Manager";
            }else if(Status == STATUS_WAITING_FOR_ACCOUNTANT)
            {
                return "Waiting for Accountant";
            }
            else if (Status == STATUS_WAITING_FOR_COUNTRY_MANAGER)
            {
                return "Waiting for country manager";
            }
            else if (Status == STATUS_PENDING_JOB_CARD)
            {
                return "Job Card created";
            }
            else if (Status == STATUS_SENT_BACK_FOR_EDIT)
            {
                return "Sent Back for Edit";
            }else if(Status == STATUS_RECEIVED)
            {
                return "Goods Received";
            }else if(Status == STATUS_ISSUED)
            {
                return "Item Isssued";
            }else if( Status == STATUS_JOB_CARD_HAS_PENDING_LPOS)
            {
                return "Job Card Has Pending Items";
            }else if(Status == STATUS_COMPLETED_JOBCARD)
            {
                return "Job Card Completed";
            }

            return "";

 

        }

        public const string WORKSHOP_MANAGER_ROLE = "Workshop Manager";

        public const string CLERK_ROLE = "Driver/Clerk";

        public const string ADMIN_ROLE = "Administrator";

        public const string TRANSPORT_OFFICER_ROLE = "Transport Officer";

        public const string TRANSPORT_MANAGER_ROLE = "Transport Manager";

        public const string ACCOUNTANT_ROLE = "Accountant";

        public const string COUNTRY_MANAGER = "Country Manager";

        public const string OPERATIONS_MANAGER = "Operations Manager";


        public const string ISSUE_NOTE_VENDOR = "Issue Note";

        public const string CASH_VENDOR = "CASH TZ";

        public const string MAIN_STORE = "Main Store";

        public const string LOCAL_FUEL_STORE = "Local Fuel Store";

        public const string LOCAL_FUEL_VENDOR = "Local Fuel";

        public const string STORE_KEEPER = "Store Keeper";

        public const string TANZANIA_SHILLINGS = "TZS";

        public const string PRODUCTS_CATEGORY = "Products";

        public const string DEFAULT_CATEGORY = "Default";

        public const string GENERAL_EXPENSES = "General Expenses";



        public static string StatusString(int status)
        {
            var s = string.Empty;
            if (status == STATUS_PENDING)
            {
                s = "Pending";

            }           


            return s;

        }

        public static int STORE_LOCATION
        {

            get
            {
                var db = new ApplicationDbContext();
                var l = db.Location.Where(x => x.Name == MAIN_STORE).Select(x => x.Id).FirstOrDefault();

                return l;
            }


        }

       




    }
}