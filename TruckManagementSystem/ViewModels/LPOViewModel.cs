﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.ViewModels
{
    public class LPOViewModel
    {

        public int Id { get; set; }



        public string UserName { get; set; }

        public DateTime CreatedAt { get; set; }

        public string JOBNumber { get; set; }

        public string LPO_Number { get; set; }

        public string Vendor_Name { get; set; }

        public decimal? Order_Total { get; set; }

        public int Status { get; set; }

        public string Currency { get; set; }


        public List<LPO_Line_Item> LPO_Line_Item { get; set; }

    }
}