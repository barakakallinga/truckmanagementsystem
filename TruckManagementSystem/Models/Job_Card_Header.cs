﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TruckManagementSystem.Helpers;

namespace TruckManagementSystem.Models
{
    public class Job_Card_Header
    {

        [Key]
        public int Id { get; set; }

        public int Maintenance_header_Id { get; set; }
        public string Transit_Type { get; set; }

        public string Fleet_No { get; set; }

        public string Truck_head_No { get; set; }

        public string Truck_Trailer_No { get; set; }

        public string Driver_Name { get; set; }

        public string Licence_No { get; set; }

        public int Status { get; set; }

        public string Driver_Clerk { get; set; }

        public string Workshop_Manager { get; set; }

        public string Transport_Officer { get; set; }

        public string Operations_Manager { get; set; }

        public string Transport_Manager { get; set; }

        public string Accountant { get; set; }

        public string Country_Manager { get; set; }

        public DateTime Created_At { get; set; }

        public DateTime Maintenance_End_Date { get; set; }


        public string Status_String
        {

            get
            {
                return Values.STATUS_STRING(Status);

            }

        }


        public string Job_Card_Reference_No { get; set; }

        public decimal? Kilometers { get; set; }




    }
}