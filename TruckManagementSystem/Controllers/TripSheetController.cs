﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Helpers;
using TruckManagementSystem.Models;
using TruckManagementSystem.ViewModels;

namespace TruckManagementSystem.Controllers
{
    public class TripSheetController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        // GET: TripSheet
        public ActionResult Index()
        {

        
            return View();
        }

        public ActionResult TripListIndex()
        {

            var triplist = db.Trip_List_Header.ToList();

            return View(triplist);
        }

        public ActionResult TripListCreate()
        {

            var tlvm = new TripListViewModel();


            var defaultline = new Trip_Line_Item();
            defaultline.Fleet_No = "";           

            tlvm.TripLineItems = new List<Trip_Line_Item>();
            tlvm.Transit_Type = db.Transit_Type.Where(x => x.Name.Trim() == "Local").Select(x => x.Id).FirstOrDefault();

            tlvm.TripLineItems.Add(defaultline);

            var categoryid = db.Categories.Where(x => x.Name == Values.PRODUCTS_CATEGORY).Select(x => x.Id).FirstOrDefault();
            ViewBag.Products = db.Items.Where(x => x.Category == categoryid).ToList();

            return PartialView("_TripListCreate", tlvm);

        }

        [HttpPost]
        public ActionResult TripListCreateLocal(TripListViewModel model)
        {

            if (ModelState.IsValid)
            {
                    var triplistheader = new Trip_List_Header();

                triplistheader.Created_Date = DateTime.Now;
                triplistheader.Created_By = System.Web.HttpContext.Current.User.Identity.Name;
                triplistheader.Transit_Type = model.Transit_Type;
         

                db.Trip_List_Header.Add(triplistheader);

                db.SaveChanges();

                var details = new List<Trip_List_Details>();
                
                    foreach(var item in model.TripLineItems)
                {
                    var detail = new Trip_List_Details();

                    detail.Destination = item.Destination;
                    detail.Driver_Name = item.Driver_Name;
                    detail.Fleet_No = item.Fleet_No;
                    detail.Licence_No = item.Licence_No;
                    detail.Mobile_No = item.Mobile_No;
                    detail.Loading_Point = item.Loading_Point;
                    detail.Product = item.Product;
                    detail.Requested_Qty = item.Requested_Qty;
                    detail.Truck_Head = item.Truck_Head;
                    detail.Truck_Trailer = item.Truck_Trailer;
                    detail.Unit = "Ltr";
                    detail.Trip_List_header_Id = triplistheader.Id;
                    detail.Loading_Point = item.Loading_Point;

                    details.Add(detail);


                }

                db.Trip_List_Details.AddRange(details);
                db.SaveChanges();

              

                return RedirectToAction("Index");
            }

         
            return PartialView("_TripListCreate", model);
        }

        public ActionResult LoadingListDetails(int id)
        {

            var loadingdetails = db.Trip_List_Details.Where(x => x.Id == id).ToList();


            return View(loadingdetails);
        }

        public ActionResult LoadingListVehicleDetails(int id)
        {

            var vehicledetails = db.Trip_List_Details.Find(id);




            return View(vehicledetails);
        }

        public ActionResult NewDepartureDetails(int id)
        {

            var tripdetails = new Trip_Details();

            tripdetails.List_Detail_Id = id;

            ViewBag.Unit = new SelectList(db.IC_UoM.ToList(), "UoM_Id", "UoM");

            return PartialView("_NewDepartureDetails", tripdetails);

        }

        [HttpPost]
        public ActionResult NewDepartureDetails(Trip_Details model)
        {
            
            if(ModelState.IsValid)
            {
                var detail = db.Trip_List_Details.Find(model.List_Detail_Id);

                detail.Order_No = model.Order_No;

                db.Entry(detail).State = System.Data.Entity.EntityState.Modified;


                model.Trip_Status = "Departure";
                model.Created_At = DateTime.Now;
                model.Username = System.Web.HttpContext.Current.User.Identity.Name;
                model.Status = Values.STATUS_PENDING;

                db.Trip_Details.Add(model);
                db.SaveChanges();

               
                return RedirectToAction("LoadingListVehicleDetails", new { id = model.List_Detail_Id });

            }

            return PartialView("_NewDepartureDetails", model);

        }


        public ActionResult LoadingInfo(int id)
        {
            var list = db.Trip_Details.Where(x => x.List_Detail_Id == id && x.Trip_Status == "Departure").ToList();


            return PartialView("_LoadingListPartialView", list);
        }

        public ActionResult NewDeliveryDetails(int id)
        {


            var tripdetails = new Trip_Details();

            var detail = db.Trip_List_Details.Find(id);

            tripdetails.List_Detail_Id = id;
            tripdetails.Order_No = detail.Order_No;

            ViewBag.Unit = new SelectList(db.IC_UoM.ToList(), "UoM_Id", "UoM", tripdetails.Unit);


            return PartialView("_NewDeliveryDetails", tripdetails);

        }

        [HttpPost]
        public ActionResult NewDeliveryDetails(Trip_Details model)
        {

            if (ModelState.IsValid)
            {
                model.Trip_Status = "Delivery";
                model.Created_At = DateTime.Now;
                model.Username = System.Web.HttpContext.Current.User.Identity.Name;
                model.Status = Values.STATUS_PENDING;

                db.Trip_Details.Add(model);
                db.SaveChanges();


                return RedirectToAction("LoadingListVehicleDetails", new { id = model.List_Detail_Id });

            }

            return PartialView("_NewDepartureDetails", model);

        }

        public ActionResult DeliveryInfo(int id)
        {
            var list = db.Trip_Details.Where(x => x.List_Detail_Id == id && x.Trip_Status =="Delivery").ToList();


            return PartialView("_DeliveryListPartialView", list);
        }

        public ActionResult CreateTripExpenseVoucher(int id)
        {

            var model = new PaymentViewModel();

            model.Trip_List_detail_Id = id;
            var detail = db.Trip_List_Details.Find(id);

            model.Order_no = detail.Order_No;
            model.Currency = "TZS";


            model.Payment_Line_Item = new List<Payment_Line_Item>();
            var paymentlineitem = new Payment_Line_Item();

            paymentlineitem.Item_Id = db.Items.Where(x => x.Item_Name.Trim() == "ACCOMODATION").Select(x => x.Item_Id).FirstOrDefault();
            paymentlineitem.Item_Name = "ACCOMODATION";
            paymentlineitem.Price = 40000;
            paymentlineitem.quantity = 1;
            paymentlineitem.Unit = "Pc";
            paymentlineitem.Currency = "TZS";
            paymentlineitem.Total = paymentlineitem.Price * paymentlineitem.quantity;

            model.Payment_Line_Item.Add(paymentlineitem);

            paymentlineitem = new Payment_Line_Item();

            paymentlineitem.Item_Id = db.Items.Where(x => x.Item_Name.Trim() == "DRINKING WATER").Select(x => x.Item_Id).FirstOrDefault();
            paymentlineitem.Item_Name = "DRINKING WATER";
            paymentlineitem.Price = 5000;
            paymentlineitem.quantity = 1;
            paymentlineitem.Unit = "Pc";
            paymentlineitem.Currency = "TZS";
            paymentlineitem.Total = paymentlineitem.Price * paymentlineitem.quantity;
            model.Payment_Line_Item.Add(paymentlineitem);

            paymentlineitem = new Payment_Line_Item();
            paymentlineitem.Item_Id = db.Items.Where(x => x.Item_Name.Trim() == "REPLENISHMENT").Select(x => x.Item_Id).FirstOrDefault();

            paymentlineitem.Item_Name = "REPLENISHMENT";
            paymentlineitem.Price = 25000;
            paymentlineitem.quantity = 1;
            paymentlineitem.Unit = "Pc";
            paymentlineitem.Currency = "TZS";
            paymentlineitem.Total = paymentlineitem.Price * paymentlineitem.quantity;
            model.Payment_Line_Item.Add(paymentlineitem);

            paymentlineitem = new Payment_Line_Item();
            paymentlineitem.Item_Id = db.Items.Where(x => x.Item_Name.Trim() == "AIRTIME FOR COMMUNICATING").Select(x => x.Item_Id).FirstOrDefault();
            paymentlineitem.Item_Name = "AIRTIME FOR COMMUNICATING";
            paymentlineitem.Price = 5000;
            paymentlineitem.quantity = 1;
            paymentlineitem.Unit = "Pc";
            paymentlineitem.Currency = "TZS";
            paymentlineitem.Total = paymentlineitem.Price * paymentlineitem.quantity;
            model.Payment_Line_Item.Add(paymentlineitem);

            paymentlineitem = new Payment_Line_Item();
            paymentlineitem.Item_Id = db.Items.Where(x => x.Item_Name.Trim() == "MINOR REPAIR AND OTHER EXPENSES").Select(x => x.Item_Id).FirstOrDefault();
            paymentlineitem.Item_Name = "MINOR REPAIR AND OTHER EXPENSES";
            paymentlineitem.Price = 25000;
            paymentlineitem.quantity = 1;
            paymentlineitem.Unit = "Pc";
            paymentlineitem.Currency = "TZS";
            paymentlineitem.Total = paymentlineitem.Price * paymentlineitem.quantity;
            model.Payment_Line_Item.Add(paymentlineitem);

            paymentlineitem = new Payment_Line_Item();
            paymentlineitem.Item_Id = db.Items.Where(x => x.Item_Name.Trim() == "BRIDGE CROSSING CHARGES").Select(x => x.Item_Id).FirstOrDefault();
            paymentlineitem.Item_Name = "BRIDGE CROSSING CHARGES";
            paymentlineitem.Price = 25000;
            paymentlineitem.quantity = 2;
            paymentlineitem.Unit = "Pc";
            paymentlineitem.Currency = "TZS";
            paymentlineitem.Total = paymentlineitem.Price * paymentlineitem.quantity;
            model.Payment_Line_Item.Add(paymentlineitem);


            model.Order_Total = model.Payment_Line_Item.Sum(x => x.Total);



            var catid = db.Categories.Where(x => x.Name == Values.GENERAL_EXPENSES).Select(x => x.Id).FirstOrDefault();

            ViewBag.Expense1 = new SelectList(db.Items.Where(x => x.Category == catid).Select(i => new { i.Item_Id, Item_Name = i.Item_Name.Trim() }).ToList(), "Item_Name", "Item_Name");


            return PartialView("_CreateTripExpenseVoucher", model);

           
        }

        [HttpPost]
        public ActionResult CreateTripExpenseVoucher(PaymentViewModel model)
        {

            if (ModelState.IsValid)
            {

                var paymenthead = new Payment_Header();

                paymenthead.CreatedAt = DateTime.Now;
                paymenthead.JOBNumber = 0.ToString();
                paymenthead.Payment_Source = "TripSheetMileageLocal";
                paymenthead.UserName = System.Web.HttpContext.Current.User.Identity.Name;
                paymenthead.Order_Total = model.Order_Total;
                paymenthead.Trip_List_Detail_Id = model.Trip_List_detail_Id;
                paymenthead.Currency = model.Currency;
                paymenthead.Vendor_Id = db.Vendor.Where(x => x.Supp_Name == Values.CASH_VENDOR).Select(x => x.Supp_Id).FirstOrDefault();

                db.Payment_Header.Add(paymenthead);
                db.SaveChanges();

                paymenthead.Payment_Number = "PC" + paymenthead.Id;

                db.Entry(paymenthead).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();
                var detaillist = new List<Payment_Details>();

                foreach(var item in model.Payment_Line_Item)
                {
                    var paymentdetail = new Payment_Details();
                    paymentdetail.Item_Id = item.Item_Id;
                    paymentdetail.Item_Name = item.Item_Name;
                    paymentdetail.quantity = item.quantity;
                    paymentdetail.Price = item.Price;
                    paymentdetail.Unit = item.Unit;
                    paymentdetail.Currency = item.Currency;
                    paymentdetail.Payment_Header_Id = paymenthead.Id;

                    detaillist.Add(paymentdetail);

                }

                db.Payment_Details.AddRange(detaillist);
                db.SaveChanges();

                return RedirectToAction("LoadingListVehicleDetails", new { id = model.Trip_List_detail_Id });


            }


            return PartialView("_CreateTripExpenseVoucher", model);

        }

        public ActionResult CreateTransitReturnVoucher(int id)
        {


            var model = new PaymentViewModel();

            model.Trip_List_detail_Id = id;
            var detail = db.Trip_List_Details.Find(id);

            model.Order_no = detail.Order_No;
            model.Currency = "TZS";


            model.Payment_Line_Item = new List<Payment_Line_Item>();
            var paymentlineitem = new Payment_Line_Item();

            paymentlineitem.Item_Name = "ACCOMODATION";
            paymentlineitem.Price = 30000;
            paymentlineitem.quantity = 1;
            paymentlineitem.Unit = "Pc";
            paymentlineitem.Currency = "TZS";
            paymentlineitem.Total = paymentlineitem.Price * paymentlineitem.quantity;

            model.Payment_Line_Item.Add(paymentlineitem);

            paymentlineitem = new Payment_Line_Item();

            paymentlineitem.Item_Name = "DRINKING WATER";
            paymentlineitem.Price = 5000;
            paymentlineitem.quantity = 1;
            paymentlineitem.Unit = "Pc";
            paymentlineitem.Currency = "TZS";
            paymentlineitem.Total = paymentlineitem.Price * paymentlineitem.quantity;
            model.Payment_Line_Item.Add(paymentlineitem);

            paymentlineitem = new Payment_Line_Item();

            paymentlineitem.Item_Name = "REFRESHMENT";
            paymentlineitem.Price = 10000;
            paymentlineitem.quantity = 1;
            paymentlineitem.Unit = "Pc";
            paymentlineitem.Currency = "TZS";
            paymentlineitem.Total = paymentlineitem.Price * paymentlineitem.quantity;
            model.Payment_Line_Item.Add(paymentlineitem);

            paymentlineitem = new Payment_Line_Item();

            paymentlineitem.Item_Name = "AIRTIME FOR COMMUNICATING";
            paymentlineitem.Price = 5000;
            paymentlineitem.quantity = 1;
            paymentlineitem.Unit = "Pc";
            paymentlineitem.Currency = "TZS";
            paymentlineitem.Total = paymentlineitem.Price * paymentlineitem.quantity;
            model.Payment_Line_Item.Add(paymentlineitem);

                  

            model.Order_Total = model.Payment_Line_Item.Sum(x => x.Total);



            return PartialView("_CreateTransitReturnVoucher", model);




        }

        [HttpPost]
        public ActionResult CreateTransitReturnVoucher(PaymentViewModel model)
        {

            if (ModelState.IsValid)
            {

                var paymenthead = new Payment_Header();

                paymenthead.CreatedAt = DateTime.Now;
                paymenthead.JOBNumber = 0.ToString();
                paymenthead.Payment_Source = "TripSheetTransitReturn";
                paymenthead.UserName = System.Web.HttpContext.Current.User.Identity.Name;
                paymenthead.Order_Total = model.Order_Total;
                paymenthead.Trip_List_Detail_Id = model.Trip_List_detail_Id;
                paymenthead.Currency = model.Currency;
                paymenthead.Vendor_Id = db.Vendor.Where(x => x.Supp_Name == Values.CASH_VENDOR).Select(x => x.Supp_Id).FirstOrDefault();

                db.Payment_Header.Add(paymenthead);
                db.SaveChanges();

                paymenthead.Payment_Number = "PC" + paymenthead.Id;

                db.Entry(paymenthead).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();
                var detaillist = new List<Payment_Details>();

                foreach (var item in model.Payment_Line_Item)
                {
                    var paymentdetail = new Payment_Details();

                    paymentdetail.Item_Name = item.Item_Name;
                    paymentdetail.quantity = item.quantity;
                    paymentdetail.Price = item.Price;
                    paymentdetail.Unit = item.Unit;
                    paymentdetail.Currency = item.Currency;
                    paymentdetail.Payment_Header_Id = paymenthead.Id;

                    detaillist.Add(paymentdetail);

                }

                db.Payment_Details.AddRange(detaillist);
                db.SaveChanges();

                return RedirectToAction("LoadingListVehicleDetails", new { id = model.Trip_List_detail_Id });


            }


            return PartialView("_CreateTripExpenseVoucher", model);


        }

        public ActionResult TripExpenseList(int id)
        {

            var list = db.Payment_Header.Where(x => x.Trip_List_Detail_Id == id).ToList();


            return PartialView("_TripExpenseListPartialView", list);


        }


        public ActionResult NewFuelVoucher(int id)
        {

            var detail = db.Trip_List_Details.Find(id);
            var vm = new IssueNoteViewModel();


            vm.Trip_List_detail_Id = detail.Id;
            vm.Order_No = detail.Order_No;
           

            vm.Trip_List_detail_Id = id;

            vm.IssueNote_Line_Item = new List<IssueNote_Line_Item>();

            var isl = new IssueNote_Line_Item();

            vm.IssueNote_Line_Item.Add(isl);


            var tripdetails = new Trip_Details();


            

            ViewBag.Unit = new SelectList(db.IC_UoM.ToList(), "UoM_Id", "UoM", tripdetails.Unit);

            var locid = db.Location.Where(x => x.Name == Values.LOCAL_FUEL_STORE).Select(x => x.Id).FirstOrDefault();
            ViewBag.PriceItems = new SelectList(db.Price_List.Where(x => x.Location_Id == locid).ToList(), "Item_Id", "Item_Name");


            return PartialView("_NewFuelVoucher", vm);

        }


        public ActionResult GetStock(string search)
        {

            var location = db.Location.Where(x => x.Name == Values.LOCAL_FUEL_STORE).Select(x => x.Id).FirstOrDefault();

            var stockobj = db.Stock.Where(X => X.Item_Name.Contains(search) || X.Part_Number.Contains(search)).Select(x => new StockLedgerViewModel()

            {

                Stock_Id = x.Stock_Id,
                Item_Name = x.Item_Name,

                Location_Id = x.Location_Id,
                Item_Id = x.Item_Id,
                Qty_Bal = x.Qty_Bal,
                Qty_In = x.Qty_In,
                settled = x.Settled

            });

            var stocklocation = stockobj.Where(l => l.Location_Id == location).ToList();

            var trackerobject = db.Qty_Bal_Tracker.ToList();

            List<int> la = new List<int>();

            foreach (var q in stocklocation.GroupBy(x => x.Item_Id))
            {

                var t = q.Select(x => new { Qty_Bal = x.Qty_Bal }).Last().Qty_Bal;

                var k = q.Select(x => x.Item_Id).LastOrDefault();

                var sum = db.Qty_Bal_Tracker.Where(x => x.Item_Id == k && x.Location_Id == location).ToList().Sum(x => x.Qty_Bal);


                if ((t - sum) <= 0)
                {
                    var p = q.Select(x => x.Item_Id).LastOrDefault();

                    la.Add(p);
                }

            }



            var barItems = stockobj.Where(t => !la.Contains(t.Item_Id) && t.Location_Id == location).ToList();



            var groupedList = new List<Stock>();


            foreach (var item in barItems.GroupBy(x => x.Item_Id))
            {
                var tempstock = new Stock();

                var stockcheck = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Id).FirstOrDefault();
                if (stockcheck != 0)
                {



                    tempstock.Stock_Id = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Stock_Id).FirstOrDefault();
                    tempstock.Item_Id = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Id).FirstOrDefault();
                    tempstock.Item_Name = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Name).FirstOrDefault();


                    groupedList.Add(tempstock);
                }
                else
                {
                    tempstock.Stock_Id = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Stock_Id).LastOrDefault();
                    tempstock.Item_Id = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Item_Id).LastOrDefault();
                    tempstock.Item_Name = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Item_Name).LastOrDefault();




                    groupedList.Add(tempstock);

                }
            }


            var json = string.Empty;

            var result = new List<Dictionary<string, string>>();


            var barItem = new[] { new { label = "", value = "" } }.AsEnumerable();

            var culture = CultureInfo.CurrentCulture;


            barItem = groupedList.Where(x => culture.CompareInfo.IndexOf(x.Item_Name ?? "", search, CompareOptions.IgnoreCase) >= 0).ToList().Select(x => new { label = x.Item_Name, value = x.Item_Id.ToString() + "," + x.Stock_Id.ToString() });


            var excludelist = barItem.Select(x => x.label).ToList();

            var locid = db.Location.Where(x => x.Name == Values.LOCAL_FUEL_STORE).Select(x => x.Id).FirstOrDefault();

            var priceexcluded = db.Price_List.Where(x => (!excludelist.Any(e => x.Item_Name.Contains(e))) && x.Location_Id == locid).ToList();
            //get from pricelist

            var nonstockitems = priceexcluded.Where(x => x.Item_Name.ToLower().Contains(search.ToLower()) || (!string.IsNullOrEmpty(x.Part_Number) && x.Part_Number.Contains(search))).Select(x => new { label = x.Item_Name, value = x.Item_Id.ToString() + ",P" + x.Id.ToString() });
            //var nonstockitems = db.Price_List.Where(x => x.Item_Name.ToLower().Contains(search.ToLower())).Select(x => new { label = x.Item_Name, value = x.Item_Id.ToString() });

            barItem = barItem.Concat(nonstockitems);


            // Join bar items without stock

            //
            var baritem1 = new[] { new { label = "", value = "" } }.AsEnumerable();

            baritem1 = barItem.GroupBy(x => x.label).Select(x => new { x.FirstOrDefault().label, x.FirstOrDefault().value });
            string resultobject = JsonConvert.SerializeObject(result);





            return Json(barItem.Take(15), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetStockDetails(string id)
        {

            string[] stringarr = id.Split(',');
            int itemid = 0;
            int stockid = 0;
            int pricelistid = 0;



            var location = db.Location.Where(x => x.Name == Values.MAIN_STORE).Select(x => x.Id).FirstOrDefault();


            if (id.ToLower().Contains(','))
            {
                itemid = Convert.ToInt32(stringarr[0]);


                var isNumeric = int.TryParse(stringarr[1], out int n);

                if (isNumeric)
                {

                    stockid = Convert.ToInt32(stringarr[1]);
                }
                else
                {
                    stockid = 0;

                    string extract = Regex.Replace(stringarr[1], "[^0-9]+", string.Empty);

                    pricelistid = Convert.ToInt32(extract);

                }
            }
            else
            {
                itemid = Convert.ToInt32(id);

            }



            var fleetobjectbefore = db.Stock.Where(x => x.Stock_Id == stockid).FirstOrDefault();


            var fleetobject = new StockDetailsViewModel();

            if (fleetobjectbefore != null)
            {

                fleetobject.Item_Name = fleetobjectbefore.Item_Name;

                fleetobject.Stock_Id = fleetobjectbefore.Stock_Id;

                fleetobject.Part_Number = fleetobjectbefore.Part_Number;
                fleetobject.Unit = fleetobjectbefore.UoM;


                fleetobject.Qty_Bal = db.Stock.Where(y => y.Item_Id == fleetobjectbefore.Item_Id && y.Location_Id == location).OrderByDescending(y => y.Stock_Id).Select(y => y.Qty_Bal).Take(1).FirstOrDefault() - (db.Qty_Bal_Tracker.Where(y => y.Item_Id == fleetobjectbefore.Item_Id && y.Location_Id == location).ToList().Sum(y => (int?)y.Qty_Bal) ?? 0);


                fleetobject.Buy_Price = fleetobjectbefore.Buy_Price;

                if (fleetobjectbefore.Qty_Bal > 0)
                {

                    fleetobject.Vendor_Id = db.Vendor.Where(x => x.Supp_Name == "Issue Note").Select(x => x.Supp_Id).FirstOrDefault();
                }
                else
                {



                }
            }
            else
            {

                var priceobjectbefore = db.Price_List.Find(pricelistid);


                fleetobject.Item_Name = priceobjectbefore.Item_Name;

                fleetobject.Stock_Id = 0;


                fleetobject.Qty_Bal = 0;

                string uomname = db.IC_UoM.Find(priceobjectbefore.UoM_Id).UoM;

                fleetobject.Unit = uomname;

                fleetobject.Part_Number = priceobjectbefore.Part_Number;


                fleetobject.Buy_Price = priceobjectbefore.Buying_Price;

                fleetobject.Vendor_Id = priceobjectbefore.Vendor_Id;





            }

            return Json(fleetobject, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public ActionResult CreateTripFuelVoucher(IssueNoteViewModel vm)
        {


            if (ModelState.IsValid)
            {
                var issuenotehead = new Issue_Note_Header();

                var issueid = db.Vendor.Where(x => x.Supp_Name == Values.LOCAL_FUEL_VENDOR).Select(x => x.Supp_Id).FirstOrDefault();

                issuenotehead.UserName = System.Web.HttpContext.Current.User.Identity.Name;
                issuenotehead.CreatedAt = DateTime.Now;
                issuenotehead.Reference_No = vm.Trip_List_detail_Id;
                issuenotehead.Type = Values.ISSUE_NOTE_TRIP_FUEL;
                issuenotehead.Currency = "TZS";
                issuenotehead.Vendor_Id = issueid;
                issuenotehead.Order_Total = vm.IssueNote_Line_Item.Sum(x => x.Total);
                //   issuenotehead.Status = Values.STATUS_ISSUE_NOTE;

                db.Issue_Note_Header.Add(issuenotehead);
                db.SaveChanges();

                var issuenotedetail = new List<Issue_Note_Details>();

                issuenotehead.Issue_Note_Number = "IS" + issuenotehead.Id;

                db.SaveChanges();


                var qtyballist = new List<Qty_Bal_Tracker>();

                foreach (var item in vm.IssueNote_Line_Item)
                {

                    var obj = new Issue_Note_Details();
                    obj.Issue_Note_Header_Id = issuenotehead.Id; 
                    obj.Item_Name = item.Item_Name;
                    obj.quantity = item.quantity;
                    obj.Price = item.Price;
                    obj.Total = item.Total;
                    obj.Unit = item.Unit;
                    obj.Stock_Id = item.Stock_Id;
                    obj.Currency = "TZS";

                    issuenotedetail.Add(obj);


                    var itemid = db.Stock.Find(obj.Stock_Id).Item_Id;


                    //Add to quantity balance tracker table 

                    var qtybalobj = db.Qty_Bal_Tracker.Where(x => x.Item_Id == itemid && x.Order_Id == issuenotehead.Reference_No).FirstOrDefault();

                    // Insert into temp balance table
                    if (qtybalobj != null)
                    {
                        db.Qty_Bal_Tracker.Remove(qtybalobj);

                    }
                    var baltracker = new Qty_Bal_Tracker();

                    baltracker.Item_Id = itemid;
                    baltracker.Order_Id = issuenotehead.Reference_No;
                    //var track = trackerobject.Where(x => x.Item_Id == baltracker.Item_Id && x.Location_Id == orderdetails.Location_Id).ToList().Sum(x => x.Qty_Bal);
                    //if (track != 0)
                    //{
                    baltracker.Qty_Bal = item.quantity;
                    //}
                    //else
                    //{
                    //    baltracker.Qty_Bal = Convert.ToInt32(_qty[i]);

                    //}


                    baltracker.Location_Id = db.Location.Where(x => x.Name == Values.LOCAL_FUEL_STORE).Select(x => x.Id).FirstOrDefault();

                 

                    qtyballist.Add(baltracker);



                }

                db.Issue_Note_Details.AddRange(issuenotedetail);
                db.SaveChanges();

                return RedirectToAction("LoadingListVehicleDetails", new { id = vm.Trip_List_detail_Id});
            }

            return View();
            }
       
    
        public ActionResult FuelInfo(int id)
        {

            var vm = new IssueNoteViewModel();

            var tripobj = db.Trip_List_Details.Find(id);


            var list = db.Issue_Note_Header.Where(x => x.Reference_No == id).ToList();

           


            var vmlist = new List<IssueNoteViewModel>();

            foreach(var item in list)
            {
                var detail = db.Issue_Note_Details.Where(x => x.Issue_Note_Header_Id == item.Id).ToList();
                var totalqty = detail.Sum(x => x.quantity);
                var singlevm = new IssueNoteViewModel();
                singlevm.Id = item.Id;
                singlevm.Trip_List_detail_Id = id;
                singlevm.CreatedAt = item.CreatedAt;
                singlevm.Currency = item.Currency;
                singlevm.Issue_Note_Number = item.Issue_Note_Number;
                singlevm.Total_Qty = totalqty;
                singlevm.Order_Total = item.Order_Total;
                singlevm.Product = detail.Select(x => x.Item_Name).FirstOrDefault();
                singlevm.Unit = detail.Select(x => x.Unit).FirstOrDefault();
                singlevm.Price = detail.Select(x => x.Price).FirstOrDefault();

                vmlist.Add(singlevm);



            }

            var issvm = new IssueNoteVMListViewModel();

            issvm.Trip_List_detail_Id = id;
            issvm.Order_No = tripobj.Order_No;

            issvm.IssueNoteViewModels = vmlist;
            

            return PartialView("_FuelInfoPartialView", issvm);

        }

        public ActionResult EditFuelVoucher(int id)
        {

   //         var detail = db.Trip_List_Details.Find(id);

            var issuehead = db.Issue_Note_Header.Where(x => x.Id == id && x.Type == Values.ISSUE_NOTE_TRIP_FUEL).FirstOrDefault();

            var issuedetails = db.Issue_Note_Details.Where(x => x.Issue_Note_Header_Id == issuehead.Id).ToList();

            var vm = new IssueNoteViewModel();

            var detail = db.Trip_List_Details.Find(issuehead.Reference_No);


            vm.JOBNumber = detail.Id;
            vm.Order_No = detail.Order_No;

            vm.Reference_No = issuehead.Reference_No;

            vm.Id = issuehead.Id;

            vm.Status = issuehead.Status;


            vm.Trip_List_detail_Id = id;

            vm.IssueNote_Line_Item = new List<IssueNote_Line_Item>();

           foreach(var item in issuedetails)
            {

                var ilitem = new IssueNote_Line_Item();
                ilitem.Stock_Id = item.Stock_Id;
                ilitem.Item_Name = item.Item_Name;
                ilitem.quantity = item.quantity;
                ilitem.Price = item.Price;
                ilitem.Unit = item.Unit;
                ilitem.Total = item.Total;
                vm.IssueNote_Line_Item.Add(ilitem);
            }
                 
       

            var tripdetails = new Trip_Details();
                       

            ViewBag.Unit = new SelectList(db.IC_UoM.ToList(), "UoM_Id", "UoM", tripdetails.Unit);

            var locid = db.Location.Where(x => x.Name == Values.LOCAL_FUEL_STORE).Select(x => x.Id).FirstOrDefault();
            ViewBag.PriceItems = new SelectList(db.Price_List.Where(x => x.Location_Id == locid).ToList(), "Item_Id", "Item_Name");


            return PartialView("_EditFuelVoucher", vm);



        }


        [HttpPost]
        public ActionResult EditTripFuelVoucher(IssueNoteViewModel model)
        {

            if (ModelState.IsValid)
            {

                using (var dbContextTransaction = db.Database.BeginTransaction())
                {

                   // var jobhead = db.Job_Card_Header.Find(model.JOBNumber);

                    var stocklist = new List<Stock>();

                    var locationid = db.Location.Where(x => x.Name == Values.LOCAL_FUEL_STORE).Select(x => x.Id).FirstOrDefault();

                    var currencyobj = db.Currencies.Where(x => x.Name == Values.TANZANIA_SHILLINGS).FirstOrDefault();

                    foreach (var item in model.IssueNote_Line_Item)
                    {

                        // deduct stock     

                        var stockobjcheck = db.Stock.Find(item.Stock_Id);

                        var stock = new Stock();

                        stock.Item_Id = stockobjcheck.Item_Id;
                        stock.Item_Name = item.Item_Name;
                        stock.Location_Id = locationid;
                        stock.Sell_Price = 0;
                        stock.Currency_Id = currencyobj.Id;
                        stock.Currency_Name = currencyobj.Name;
                        stock.Type = "Issue Note Fuel";


                        var ItemUom = db.Price_List.Where(pl => pl.Item_Id == stock.Item_Id).First();

                        var UoMint = ItemUom.UoM_Id;

                        //     var UomName = db.IC_UoM.Where(pl => pl.UoM_Id == UoMint).First();

                        stock.UoM = item.Unit;// uomobj.Where(x => x.UoM_Id == UoMint).Select(x => x.UoM).FirstOrDefault();

                        List<Stock> _checkItem = db.Stock.Where(x => x.Item_Id == stock.Item_Id && x.Location_Id == stock.Location_Id).ToList();


                        //(from ci in db.Stock
                        //                      where ci.Item_Id == stock.Item_Id && ci.Location_Id == stock.Location_Id
                        //                      select ci).ToList();
                        decimal qtybal = 0;


                        if (_checkItem.Count > 0)

                        {
                            var supplycheck = _checkItem.Where(x => x.Item_Id == stock.Item_Id).LastOrDefault();

                            var priceitem = db.Price_List.Where(x => x.Item_Id == stock.Item_Id).ToList().LastOrDefault();
                            stock.Buy_Price = priceitem.Buying_Price;
                            qtybal = supplycheck.Qty_Bal;
                            stock.Qty_Out = item.quantity;
                            stock.Cost_Out = Math.Round((stock.Qty_Out * stock.Buy_Price), 2);
                            stock.Sent_Location = stock.Location_Id;
                            decimal balcost = 0;

                            if (priceitem.curr.Name == "TZS")
                            {
                                balcost = supplycheck.Bal_Cost_TZS;
                                stock.Bal_Cost_TZS = supplycheck.Bal_Cost_TZS - stock.Cost_Out;
                            }
                            else if (priceitem.curr.Name == "USD")
                            {
                                balcost = supplycheck.Bal_Cost_USD;
                                stock.Bal_Cost_USD = supplycheck.Bal_Cost_USD - stock.Cost_Out;
                            }




                            stock.Qty_Bal = qtybal - stock.Qty_Out;

                            if (stock.Qty_Bal < 0)
                            {

                                dbContextTransaction.Rollback();

                                return Json(new { message = stock.Item_Name.ToString() + " will be less than zero. Current stock is ( " + qtybal.ToString() + " )" });

                            }


                            stock.Created_Date = DateTime.Now;
                            stocklist.Add(stock);


                        }
                        else
                        {

                            stock.Qty_Out = item.quantity;

                            stock.Qty_Bal = stock.Qty_In;
                            stock.Cost_In = Math.Round(stock.Qty_In * stock.Buy_Price, 2);

                            if (stock.Currency_Name == "TZS")
                            {

                                stock.Bal_Cost_TZS = stock.Cost_In - stock.Cost_Out;
                            }
                            else if (stock.Currency_Name == "USD")
                            {

                                stock.Bal_Cost_USD = stock.Cost_In - stock.Cost_Out;
                            }

                            stock.Created_Date = DateTime.Now;
                            stocklist.Add(stock);

                        }


                        var FIFO = db.Stock.Find(item.Stock_Id);

                        var quant = (FIFO.FIFO_Quantity ?? 0) + item.quantity;
                        var diff = FIFO.Qty_In - quant;
                        if (diff > 0)
                        {
                            FIFO.FIFO_Quantity = quant;
                            db.Entry(FIFO).State = EntityState.Modified;
                            db.SaveChanges();

                        }
                        else
                        {

                            FIFO.FIFO_Quantity = FIFO.Qty_In;

                            FIFO.Settled = 1;
                            db.Entry(FIFO).State = EntityState.Modified;
                            db.SaveChanges();

                            var stop = false;

                            while (stop == false)
                            {
                                var fifonew = db.Stock.Where(x => x.Location_Id == locationid && x.Qty_In != 0 && x.Settled == 0 && x.Item_Id == stockobjcheck.Item_Id).FirstOrDefault();

                                if (fifonew != null)
                                {
                                    var quant1 = (fifonew.FIFO_Quantity ?? 0) + +Math.Abs(diff);
                                    var diff1 = fifonew.Qty_In - quant1;
                                    if (diff1 > 0)
                                    {
                                        fifonew.FIFO_Quantity = quant1;
                                        db.Entry(fifonew).State = EntityState.Modified;
                                        db.SaveChanges();

                                        stop = true;

                                    }
                                    else
                                    {

                                        fifonew.FIFO_Quantity = fifonew.Qty_In;

                                        fifonew.Settled = 1;
                                        db.Entry(fifonew).State = EntityState.Modified;
                                        db.SaveChanges();

                                        diff = Math.Abs(diff1);
                                    }
                                }
                                else
                                {
                                    stop = true;

                                }


                            }

                        }








                    }


                    db.Stock.AddRange(stocklist);

                    db.SaveChanges();


                    //remove from qty bal tracker

                    var qtybalobj = db.Qty_Bal_Tracker.Where(x => x.Order_Id == model.Reference_No);
                    db.Qty_Bal_Tracker.RemoveRange(qtybalobj);
                    db.SaveChanges();


                    model.Status = Values.STATUS_ISSUED;

                    var issuenote = db.Issue_Note_Header.Find(model.Id);

                    issuenote.Status = Values.STATUS_ISSUED;

                    db.Entry(issuenote).State = EntityState.Modified;

                    db.SaveChanges();


                    //check for other pending items in jobcard

                    


                

                    db.SaveChanges();


                    dbContextTransaction.Commit();

                    //     return Json(new { message = "success", order_id = orderid });

                    var loadingobj = db.Trip_List_Details.Find(model.Trip_List_detail_Id);




                    return RedirectToAction("LoadingListVehicleDetails", new { id = loadingobj.Trip_List_header_Id});




                    //   ViewBag.Vendors = db.Vendor.ToList();


                    //    return View("_IssueNoteEdit", model);

                    //  return RedirectToAction("");

                }

            }




            ViewBag.Unit = new SelectList(db.IC_UoM.ToList(), "UoM_Id", "UoM");

            var locid = db.Location.Where(x => x.Name == Values.LOCAL_FUEL_STORE).Select(x => x.Id).FirstOrDefault();
            ViewBag.PriceItems = new SelectList(db.Price_List.Where(x => x.Location_Id == locid).ToList(), "Item_Id", "Item_Name");


            return PartialView("_EditFuelVoucher", model);


        }

    }

}