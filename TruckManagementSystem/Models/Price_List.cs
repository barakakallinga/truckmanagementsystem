﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Price_List
    {

        [Key]     
        public int Id { get; set; }

        public int Vendor_Id { get; set; }

        [ForeignKey("Vendor_Id")]
        public virtual Vendor Vendor { get; set; }

        public string Item_Name { get; set; }

        public string Part_Number { get; set; } 
        public int Item_Id { get; set; }

        [ForeignKey("Item_Id")]
        public virtual Items Items { get; set; }

        public int UoM_Id { get; set; }

        [ForeignKey("UoM_Id")]
        public virtual IC_UoM UoM { get; set; }

        [Required(ErrorMessage = "Please Choose Currency")]
        public int? Currency { get; set; }

        [ForeignKey("Currency")]
        public virtual Currency curr { get; set; }

        [Required(ErrorMessage = "Please select a Tax option")]
        public int? Taxable { get; set; }


        public decimal Buying_Price_No_Tax { get; set; }

        public decimal Buying_Price { get; set; }

        public decimal? Selling_Price { get; set; }

        public int SubCategory { get; set; }

        public int Location_Id { get; set; }
    }
}