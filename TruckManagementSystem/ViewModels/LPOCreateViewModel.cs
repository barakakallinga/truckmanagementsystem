﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.ViewModels
{
    public class LPOCreateViewModel
    {

        public List<Price_List> Item_List { get; set; }

        public List<Vendor> Vedor_List { get; set; }

        public LPO_Header LPO_Header { get; set; }

        public List<LPO_Details> LPO_Details { get; set; }



    }
}