﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.ViewModels
{
    public class ReconciliationViewModel
    {
        public int Id { get; set; }

        public DateTime Created_Date { get; set; }

        public string Location { get; set; }

        public string UserName { get; set; }

        public int Status_Int { get; set; }
        public string Status { get; set; }
        public List<Reconciliation_Details> Line_item { get; set; }
    }
}