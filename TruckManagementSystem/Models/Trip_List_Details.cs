﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Trip_List_Details
    {

        [Key]
        public int Id { get; set; }

        public int Trip_List_header_Id { get; set; }

        public string Fleet_No { get; set; }

          public string Truck_Head { get; set; }

        public string Truck_Trailer { get; set; }

        public string Driver_Name { get; set; }

        public string Licence_No { get; set; }

        public string Product { get; set; }

        public string Loading_Point { get; set; }
        public string Mobile_No { get; set; }
        public decimal Requested_Qty { get; set; }
        public string  Unit { get; set; }
        public string Destination { get; set; }

        public string Order_No { get; set; }


    }
}