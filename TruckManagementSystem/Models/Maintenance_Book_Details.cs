﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Maintenance_Book_Details
    {
        [Key]
        public int Id { get; set; }

        public int Maintainance_header_id { get; set; }

        public string Item { get; set; }

        public int Status { get; set; }

        public string Repair { get; set; } 

        public string Remarks { get; set; }

        public int? Stock_Id { get; set; }

        public string Stock_Name { get; set; }

        public decimal Qty_Available { get; set; }

        public decimal Qty_Requested { get; set; }

        public decimal Net_Qty_Requested { get; set; }


        public int Transport_Officer { get; set; }

        public decimal? Price { get; set; }

        public int Vendor { get; set; }

        public string Vendor_Name
        {

            get
            {

                using (var db = new ApplicationDbContext())
                {
                    string name = db.Vendor.Where(x => x.Supp_Id == Vendor).Select(x => x.Supp_Name).FirstOrDefault();

                    return name;
                }

            }
        }

     
        public decimal? Total { get; set; }




    }
}