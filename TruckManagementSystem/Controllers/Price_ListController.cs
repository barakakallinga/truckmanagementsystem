﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Helpers;
using TruckManagementSystem.Models;
using TruckManagementSystem.ViewModels;

namespace TruckManagementSystem.Controllers
{
    public class Price_ListController : Controller
    {

        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Price_List
        public ActionResult Index(int? page, string searchString, string currentFilter)
        {
    

            if (searchString != null)
            {
                page = 1;
            }else
            {
                searchString =  currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            int pageSize = 20;
            int pageNumber = (page ?? 1);

            var price_List = new List<Price_List>();
            if (searchString != null)
            {
                price_List = db.Price_List.Where(x => x.Item_Name.Contains(searchString) || x.Part_Number.Contains(searchString)).Include(p => p.curr).Include(p => p.Items).Include(p => p.UoM).Include(p => p.Vendor).ToList();

            }
            else
            {
                price_List = db.Price_List.Include(p => p.curr).Include(p => p.Items).Include(p => p.UoM).Include(p => p.Vendor).ToList();
            }

                return View(price_List.OrderByDescending(x=> x.Id).ToList().ToPagedList(pageNumber, pageSize));
        }

        // GET: Price_List/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Price_List price_List = db.Price_List.Find(id);
            if (price_List == null)
            {
                return HttpNotFound();
            }
            return View(price_List);
        }

        private ActionResult HttpNotFound()
        {
            throw new NotImplementedException();
        }

        // GET: Price_List/Create
        public ActionResult Create()
        {


            ViewBag.Currency = new SelectList(db.Currencies, "Currency_Id", "Currency_Name");
     
            ViewBag.UoM_Id = new SelectList(db.IC_UoM, "UoM_Id", "UoM");
            ViewBag.Vendor_Id = new SelectList(db.Vendor, "Supp_Id", "Supp_Name");

            var list = new List<SelectListItem>();

            var obj = new[] { new SelectListItem { Text = "Taxable", Value = "1" }, new SelectListItem { Text = "Not Taxable", Value = "2" } };

            foreach (var item in obj)
            {

                list.Add(
                   new SelectListItem
                   {
                       Text = item.Text,
                       Value = item.Value
                   });

            }


            var itemwithpartlist = new List<SelectListItem>();

            foreach(var item in db.Items)
            {

                itemwithpartlist.Add(

                    new SelectListItem
                    {
                        Text = item.Item_Name + " " + item.Part_Number,

                        Value = item.Item_Id.ToString()

                    }

                    ) ;
                

            }

            ViewBag.Item_Id = itemwithpartlist;

            ViewBag.Tax = new SelectList(list, "Value", "Text");
            return View();
        }

        // POST: Price_List/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Vendor_Id,Item_Id,UoM_Id,Currency,Buying_Price,Selling_Price,Taxable,Buying_Price_No_Tax")] Price_List price_List)
        {
            if (ModelState.IsValid)
            {
                var item = db.Items.Find(price_List.Item_Id);

                var categoryid = db.Categories.Where(x => x.Name == Values.PRODUCTS_CATEGORY).Select(x => x.Id).FirstOrDefault();

                price_List.Item_Name = item.Item_Name;
                price_List.Part_Number = item.Part_Number;

                if(item.Category == categoryid)
                {


                    var locid = db.Location.Where(x => x.Name == Values.LOCAL_FUEL_STORE).Select(x => x.Id).FirstOrDefault();

                    price_List.Location_Id = locid;
            //        price_List.Location_Id = 

                }else
                {

                    var locid = db.Location.Where(x => x.Name == Values.MAIN_STORE).Select(x => x.Id).FirstOrDefault();

                    price_List.Location_Id = locid;
                }

                //if(item.Category == Values.KITCHEN_FOOD)
                //{
                //    price_List.Location_Id = Values.KITCHEN_LOCATION;
                //}
                //else if(item.Category == Values.BAR_ITEMS)
                //{
                //    price_List.Location_Id = Values.BAR_LOCATION;

                //}

                db.Price_List.Add(price_List);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Currency = new SelectList(db.Currencies, "Currency_Id", "Currency_Name", price_List.Currency);
            ViewBag.Item_Id = new SelectList(db.Items, "Item_Id", "Item_Name", price_List.Item_Id);
            ViewBag.UoM_Id = new SelectList(db.IC_UoM, "UoM_Id", "UoM", price_List.UoM_Id);
            ViewBag.Vendor_Id = new SelectList(db.Vendor, "Supp_Id", "Supp_Name", price_List.Vendor_Id);
            var list = new List<SelectListItem>();

            var obj = new[] { new SelectListItem { Text = "Taxable", Value = "1" }, new SelectListItem { Text = "Not Taxable", Value = "2" } };

            foreach (var item in obj)
            {

                list.Add(
                   new SelectListItem
                   {
                       Text = item.Text,
                       Value = item.Value
                   });

            }

            ViewBag.Taxable = new SelectList(list, "Value", "Text");
            return View(price_List);
        }


        public ActionResult GetVendorCurrency(int vendorid)
        {
            var a = db.Vendor.Find(vendorid);

            var obj = new CurrencyViewModel();
            if (vendorid != 0)
            {
                obj.id = a.Currency;

                obj.name = db.Currencies.Find(a.Currency).Name;
            }

            return Json(obj, JsonRequestBehavior.AllowGet);

        }
                                    
        // GET: Price_List/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Price_List price_List = db.Price_List.Find(id);
            if (price_List == null)
            {
                return HttpNotFound();
            }
            ViewBag.Currency = new SelectList(db.Currencies, "Id", "Name", price_List.Currency);
     
            ViewBag.UoM_Id = new SelectList(db.IC_UoM, "UoM_Id", "UoM", price_List.UoM_Id);
            ViewBag.Vendor_Id = new SelectList(db.Vendor, "Supp_Id", "Supp_Name", price_List.Vendor_Id);
            var list = new List<SelectListItem>();

            var obj = new[] { new SelectListItem { Text = "Taxable", Value = "1" }, new SelectListItem { Text = "Not Taxable", Value = "2" } };

            foreach (var item in obj)
            {

                list.Add(
                   new SelectListItem
                   {
                       Text = item.Text,
                       Value = item.Value
                   });

            }



            var itemwithpartlist = new List<SelectListItem>();

            foreach (var item in db.Items)
            {

                itemwithpartlist.Add(

                    new SelectListItem
                    {
                        Text = item.Item_Name + " " + item.Part_Number,

                        Value = item.Item_Id.ToString()

                    }

                    );


            }

            ViewBag.Item_Id = new SelectList(itemwithpartlist,"Value","Text", price_List.Item_Id);
            ViewBag.Taxable = new SelectList(list, "Value", "Text", price_List.Taxable);

            return View(price_List);
        }

        // POST: Price_List/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Vendor_Id,Item_Id,UoM_Id,Currency,Buying_Price,Selling_Price,Taxable,Buying_Price_No_Tax")] Price_List price_List)
        {
            if (ModelState.IsValid)
            {


                var item = db.Items.Find(price_List.Item_Id);

                var categoryid = db.Categories.Where(x => x.Name == Values.PRODUCTS_CATEGORY).Select(x => x.Id).FirstOrDefault();


                price_List.Item_Name = item.Item_Name;

                price_List.Part_Number = item.Part_Number;

                if (item.Category == categoryid)
                {


                    var locid = db.Location.Where(x => x.Name == Values.LOCAL_FUEL_STORE).Select(x => x.Id).FirstOrDefault();

                    price_List.Location_Id = locid;
                    //        price_List.Location_Id = 

                }
                else
                {

                    var locid = db.Location.Where(x => x.Name == Values.MAIN_STORE).Select(x => x.Id).FirstOrDefault();

                    price_List.Location_Id = locid;
                }

                db.Entry(price_List).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Currency = new SelectList(db.Currencies, "Currency_Id", "Currency_Name", price_List.Currency);
            ViewBag.Item_Id = new SelectList(db.Items, "Item_Id", "Item_Name", price_List.Item_Id);
            ViewBag.UoM_Id = new SelectList(db.IC_UoM, "UoM_Id", "UoM", price_List.UoM_Id);
            ViewBag.Vendor_Id = new SelectList(db.Vendor, "Supp_Id", "Supp_Name", price_List.Vendor_Id);

            var list = new List<SelectListItem>();

            var obj = new[] { new SelectListItem { Text = "Taxable", Value = "1" }, new SelectListItem { Text = "Not Taxable", Value = "2" } };

            foreach (var item in obj)
            {

                list.Add(
                   new SelectListItem
                   {
                       Text = item.Text,
                       Value = item.Value
                   });

            }

            ViewBag.Taxable = new SelectList(list, "Value", "Text", price_List.Taxable);

            return View(price_List);
        }

        // GET: Price_List/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Price_List price_List = db.Price_List.Find(id);
            if (price_List == null)
            {
                return HttpNotFound();
            }
            return View(price_List);
        }

        // POST: Price_List/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Price_List price_List = db.Price_List.Find(id);
            db.Price_List.Remove(price_List);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}