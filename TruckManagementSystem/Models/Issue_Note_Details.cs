﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Issue_Note_Details
    {


        [Key]
        public int Id { get; set; }

        public int Stock_Id { get; set; }

        public string Item_Name { get; set; }

        public decimal Stock_Quantity { get; set; }

        public decimal quantity { get; set; }

        public int Issue_Note_Header_Id { get; set; }

        public string Unit { get; set; }

        public Decimal Price { get; set; }

        public string Currency { get; set; }

        public Decimal Total { get; set; }

        public string Conversion_Unit { get; set; }

    }
}