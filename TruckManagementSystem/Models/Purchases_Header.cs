﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Purchases_Header
    {

        [Key]

        public int Id { get; set; }
        public DateTime TranCreatedTime { get; set; }
        public int Supplier_Id { get; set; }



        public decimal TranTotal { get; set; }

        public int Location_Id { get; set; }
        public string Notes { get; set; }

        public string Invoice_Number { get; set; }

        public string Cash_Vendor_Name { get; set; }

        public string VRN_No { get; set; }


        public string User_Name { get; set; }

        public DateTime Last_Updated { get; set; }

        public int GRN_ID { get; set; }


      




    }
}