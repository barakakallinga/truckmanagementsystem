﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.ViewModels
{
    public class GRNViewModel
    {

    

        public LPO_Header LPO_Header { get; set; }

        public List<LPO_Details> LPO_Details { get; set; }

        public GRN_Header GRN_Header { get; set; }

        public List<GRN_Details> GRN_Details { get; set; }





    }
}