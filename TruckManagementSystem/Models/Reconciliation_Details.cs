﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Reconciliation_Details
    {


        [Key]
        public int Id { get; set; }

        public int ReconcilliationHeaderId { get; set; }
        public int? StockID { get; set; }
        public int Item_Id { get; set; }
        public string Item_name { get; set; }
        public int Initial_Quantity { get; set; }
        public int Used_Quantity { get; set; }
        public int Balance { get; set; }
        public int? Stock_Loss { get; set; }

        public decimal? Buy_Price { get; set; }

        public string Currency { get; set; }
        public string UoM { get; set; }

        public decimal? Sell_Price { get; set; }

        public decimal? Amount { get; set; }

        public string Remarks { get; set; }


    }
}