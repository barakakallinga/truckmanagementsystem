﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Helpers;
using TruckManagementSystem.Models;
using TruckManagementSystem.ViewModels;

namespace TruckManagementSystem.Controllers
{
    [Authorize]
    public class ReportsController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
       

        public ActionResult Index()
        {


            return View();
        }


        public ActionResult PricelistReport(int? currentFilter, int? vendorid, int? itemid, int? page)
        {
            ViewBag.Item_Id = new SelectList(db.Items, "Item_Id", "Item_Name");
            ViewBag.VendorId = new SelectList(db.Vendor, "Supp_Id", "Supp_Name");    // preselect item in selectlist by CompanyID param


            if (vendorid != null)
            {
                page = 1;

            }
            else if (itemid != null && vendorid == null)
            {
                page = 1;
            }
            else
            {
                vendorid = currentFilter;
            }

            ViewBag.CurrentFilter = vendorid;

            var pricevmlist = db.Price_List.Select(x => new PriceListViewModel
            {
                Vendor_id = x.Vendor_Id,
                Item_Id = x.Item_Id,
                Taxable = x.Taxable,
                Buying_Price = x.Buying_Price,
                Buying_Price_No_Tax = x.Buying_Price_No_Tax,
                Currency_Id = x.Currency,
                UoM_Id = x.UoM_Id,
                Selling_Price = x.Selling_Price
            }
            ).ToList();




            if (vendorid != null && itemid == null)
            {

                pricevmlist = pricevmlist.Where(s => s.Vendor_id == vendorid).ToList();

            }
            else if (itemid != null && vendorid == null)
            {
                pricevmlist = pricevmlist.Where(s => s.Item_Id == itemid).ToList();
            }
            else if (itemid != null && vendorid != null)
            {
                pricevmlist = pricevmlist.Where(s => s.Item_Id == itemid && s.Vendor_id == vendorid).ToList();
            }

            int pageSize = 20;
            int pageNumber = (page ?? 1);

            Session.Clear();

            Session["PriceListSession"] = pricevmlist.OrderBy(x => x.Item_Name).ToList();
            return View(pricevmlist.OrderBy(x => x.Item_Name).ToList().ToPagedList(pageNumber, pageSize));

        }

        [Securities.RolesAuthorization(Roles = Values.ADMIN_ROLE + "," + Values.ACCOUNTANT_ROLE)]
        public ActionResult StockListReport(int? currentFilter, int? currentFilter2, int? locationid, int? itemid, int? page)
        {


            ViewBag.Item_Id = new SelectList(db.Items.Where(x => x.Stock_Type == Values.LOCATION_STOCK.ToString()), "Item_Id", "Item_Name");

         

            ViewBag.Location_Id = new SelectList(db.Location.Where(x => x.Location_Type == Values.LOCATION_STOCK), "Id", "Name");

            







            ViewBag.item = itemid;



            if (locationid != null)
            {
                page = 1;

            }
            else
            {
                locationid = currentFilter;
            }

            ViewBag.location = locationid;



            ViewBag.CurrentFilter = locationid;

            if (itemid != null)
            {
                page = 1;

            }
            else
            {
                itemid = currentFilter2;
            }

            ViewBag.CurrentFilter2 = itemid;

            // var mainstock = db.Stock.ToList();

            var mainstock = db.Stock.Select(x => new StockLedgerViewModel()

            {
                Created_Date = x.Created_Date,

                Item_Name = x.Item_Name,

                Location_Id = x.Location_Id,

                UoM = x.UoM,
                Qty_In = x.Qty_In,
                Qty_Out = x.Qty_Out,
                Cost_In = x.Cost_In,
                Cost_Out = x.Cost_Out,
                Qty_Bal = x.Qty_Bal,
                Bal_Cost_TZS = x.Bal_Cost_TZS,
                Bal_Cost_USD = x.Bal_Cost_USD,
                Sent_Location = x.Sent_Location,
                Item_Id = x.Item_Id,
                settled = x.Settled,
                Currency_Name = x.Currency_Name,
                Buy_Price = x.Buy_Price,
                Sell_Price = x.Sell_Price
            });

            var stockobj = mainstock.ToList();

            if (locationid != null && itemid == null)
            {

                stockobj = stockobj.Where(s => s.Location_Id == locationid).ToList();

            }
            else if (itemid != null && locationid == null)
            {
                stockobj = stockobj.Where(s => s.Item_Id == itemid).ToList();
            }
            else if (itemid != null && locationid != null)
            {
                stockobj = stockobj.Where(s => s.Item_Id == itemid && s.Location_Id == locationid).ToList();
            }


            var groupedList = new List<StockLedgerViewModel>();

            var trackerobject = db.Qty_Bal_Tracker.ToList();
            if (locationid != null)
            {
                foreach (var item in stockobj.GroupBy(x => x.Item_Id))
                {
                    var tempstock = new StockLedgerViewModel();

                    // tempstock.Stock_Id = item.Select(x => x.Stock_Id).FirstOrDefault();

                    var stockcheck = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Id).FirstOrDefault();
                    if (stockcheck != 0)
                    {
                        tempstock.Item_Id = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Id).FirstOrDefault();
                        tempstock.Item_Name = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Name).FirstOrDefault();
                        tempstock.Location_Id = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Location_Id).FirstOrDefault();

                        var track = trackerobject.Where(x => x.Item_Id == tempstock.Item_Id && x.Location_Id == tempstock.Location_Id).ToList().Sum(x => x.Qty_Bal);
                        if (track != 0)
                        {
                            tempstock.Qty_Bal = (mainstock.Where(x => x.Location_Id == locationid && x.Item_Id == tempstock.Item_Id).ToList().LastOrDefault()?.Qty_Bal ?? 0) - track;
                        }
                        else
                        {
                            tempstock.Qty_Bal = mainstock.Where(x => x.Location_Id == locationid && x.Item_Id == tempstock.Item_Id).ToList().LastOrDefault()?.Qty_Bal ?? 0;

                        }



                        tempstock.Sell_Price = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Sell_Price).FirstOrDefault();
                        tempstock.Currency_Name = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Currency_Name).FirstOrDefault();
                        //  tempstock.Expiry_Date = item.Select(x => x.Expiry_Date).FirstOrDefault();
                        // tempstock.Barcode = item.Select(x => x.Barcode).FirstOrDefault();
                        tempstock.Buy_Price = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Buy_Price).FirstOrDefault();
                        //   tempstock.Ref_No = item.Select(x => x.Ref_No).FirstOrDefault();
                        tempstock.UoM = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.UoM).FirstOrDefault();

                        if (tempstock.Currency_Name.Trim() == "TZS")
                        {
                            tempstock.Bal_Cost_TZS = Math.Round((tempstock.Qty_Bal * tempstock.Buy_Price), 2);
                        }
                        else
                        {
                            tempstock.Bal_Cost_USD = Math.Round((tempstock.Qty_Bal * tempstock.Buy_Price), 2);
                        }
                        groupedList.Add(tempstock);

                    }
                    else
                    {

                        tempstock.Item_Id = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Item_Id).LastOrDefault();
                        tempstock.Item_Name = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Item_Name).LastOrDefault();
                        tempstock.Location_Id = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Location_Id).LastOrDefault();

                        var track = trackerobject.Where(x => x.Item_Id == tempstock.Item_Id && x.Location_Id == tempstock.Location_Id).ToList().Sum(x => x.Qty_Bal);
                        if (track != 0)
                        {
                            tempstock.Qty_Bal = (mainstock.Where(x => x.Location_Id == locationid && x.Item_Id == tempstock.Item_Id).ToList().LastOrDefault()?.Qty_Bal ?? 0) - track;
                        }
                        else
                        {
                            tempstock.Qty_Bal = mainstock.Where(x => x.Location_Id == locationid && x.Item_Id == tempstock.Item_Id).ToList().LastOrDefault()?.Qty_Bal ?? 0;

                        }



                        tempstock.Sell_Price = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Sell_Price).LastOrDefault();
                        tempstock.Currency_Name = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Currency_Name).LastOrDefault();
                        //  tempstock.Expiry_Date = item.Select(x => x.Expiry_Date).FirstOrDefault();
                        // tempstock.Barcode = item.Select(x => x.Barcode).FirstOrDefault();
                        tempstock.Buy_Price = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Buy_Price).LastOrDefault();
                        //   tempstock.Ref_No = item.Select(x => x.Ref_No).FirstOrDefault();
                        tempstock.UoM = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.UoM).LastOrDefault();

                        if (tempstock.Currency_Name.Trim() == "TZS")
                        {
                            tempstock.Bal_Cost_TZS = Math.Round((tempstock.Qty_Bal * tempstock.Buy_Price), 2);
                        }
                        else
                        {
                            tempstock.Bal_Cost_USD = Math.Round((tempstock.Qty_Bal * tempstock.Buy_Price), 2);
                        }
                        groupedList.Add(tempstock);

                    }



                }

            }

            int pageSize = 20;
            int pageNumber = (page ?? 1);

            Session.Clear();

            Session["StockListSession"] = groupedList.OrderBy(x => x.Item_Name).ToList();
            return View(groupedList.OrderBy(x => x.Item_Name).ToList().ToPagedList(pageNumber, pageSize));



        }


        [Securities.RolesAuthorization(Roles = Values.ADMIN_ROLE + "," + Values.ACCOUNTANT_ROLE)]
        public ActionResult StockLedgerReport(int? currentFilter, int? locationid, int? currentFilter2, string currentFilter3, string currentFilter4, int? itemid, int? page, string FromDate, string ToDate)
        {

            ViewBag.Item_Id = new SelectList(db.Items.Where(x => x.Stock_Type == Values.LOCATION_STOCK.ToString()), "Item_Id", "Item_Name", currentFilter2);




            if (locationid != null)
            {
                page = 1;

            }
            else
            {
                locationid = currentFilter;
            }

            ViewBag.CurrentFilter = locationid;


            if (itemid != null)
            {
                page = 1;
            }
            else
            {
                itemid = currentFilter2;
            }

            ViewBag.CurrentFilter2 = itemid;


            if (FromDate != null)
            {
                page = 1;
            }
            else
            {
                FromDate = currentFilter3;
            }

            ViewBag.CurrentFilter3 = FromDate;



            if (ToDate != null)
            {
                page = 1;
            }
            else
            {
                ToDate = currentFilter4;
            }

            ViewBag.CurrentFilter4 = ToDate;


            {

                ViewBag.Location_Id = new SelectList(db.Location.Where(x => x.Location_Type == Values.LOCATION_STOCK), "Id", "Name", currentFilter);



                ViewBag.location = locationid;

                ViewBag.item = itemid;




                var mainstock = new List<StockLedgerViewModel>();





                mainstock = db.Stock.Select(x => new StockLedgerViewModel()
                {
                    Created_Date = x.Created_Date,

                    Item_Name = x.Item_Name,

                    Location_Id = x.Location_Id,

                    UoM = x.UoM,
                    Qty_In = x.Qty_In,
                    Qty_Out = x.Qty_Out,
                    Cost_In = x.Cost_In,
                    Cost_Out = x.Cost_Out,
                    Qty_Bal = x.Qty_Bal,
                    Bal_Cost_TZS = x.Bal_Cost_TZS,
                    Bal_Cost_USD = x.Bal_Cost_USD,
                    Sent_Location = x.Sent_Location,
                    Item_Id = x.Item_Id
                }




               ).ToList();










                if (locationid != null && itemid != null && FromDate != null && ToDate != null)
                {
                    DateTime datetimeFromDate = Helpers.Helpers.GetDateTime(FromDate);

                    DateTime datetimeToDate = Helpers.Helpers.GetDateTime(ToDate);
                    mainstock = mainstock.Where(x => x.Location_Id == locationid && x.Item_Id == itemid && (x.Created_Date >= datetimeFromDate && x.Created_Date < datetimeToDate.AddDays(1).AddTicks(-1))).ToList();


                }
                else if (locationid != null && itemid == null && FromDate != null && ToDate != null)

                {

                    DateTime datetimeFromDate = Helpers.Helpers.GetDateTime(FromDate);

                    DateTime datetimeToDate = Helpers.Helpers.GetDateTime(ToDate);

                    mainstock = mainstock.Where(x => x.Location_Id == locationid && (x.Created_Date >= datetimeFromDate && x.Created_Date < datetimeToDate.AddDays(1).AddTicks(-1))).ToList();





                }
                else if (locationid == null && itemid == null && FromDate != null && ToDate != null)
                {

                    DateTime datetimeFromDate = Helpers.Helpers.GetDateTime(FromDate);

                    DateTime datetimeToDate = Helpers.Helpers.GetDateTime(ToDate);

                    mainstock = mainstock.Where(x => (x.Created_Date >= datetimeFromDate && x.Created_Date < datetimeToDate.AddDays(1).AddTicks(-1))).ToList();



                }




                int pageSize = 20;
                int pageNumber = (page ?? 1);

                Session.Clear();

                Session["StockListLedgerSession"] = mainstock.OrderBy(x => x.Created_Date).ToList();
                return View(mainstock.OrderBy(x => x.Created_Date).ToList().ToPagedList(pageNumber, pageSize));

            }
        }



        [Securities.RolesAuthorization(Roles = Values.ADMIN_ROLE + "," + Values.ACCOUNTANT_ROLE +","+Values.TRANSPORT_OFFICER_ROLE)]
        public ActionResult SparesAndExpenses(int? page, string searchString, string currentFilter, DateTime? currentFilter3, DateTime? currentFilter4, DateTime? start, DateTime? end)
        {

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;



            if (start != null)
            {
                page = 1;
            }
            else
            {
                start = currentFilter3;
            }

            ViewBag.CurrentFilter3 = start;



            if (end != null)
            {
                page = 1;
            }
            else
            {
                end = currentFilter4;
            }

            ViewBag.CurrentFilter4 = end;

            int pageSize = 10;
            int pageNumber = (page ?? 1);


            var sparesAndExpensesList = new List<SparesAndExpensesViewModel>();


            if (searchString != null)
            {
       //         maintenanceIndex = db.Maintenance_Book_Header.Where(x => x.Fleet_No.Contains(searchString) || x.Job_Card_Reference_No.Contains(searchString)).OrderByDescending(x => x.Id).ToList();

            }
            else
            {
      //          maintenanceIndex = db.Maintenance_Book_Header.OrderByDescending(x => x.Id).ToList();
            }

            var jobheaders = db.Job_Card_Header.ToList();


            if (start != null && end != null)

            {
                end = end.Value.AddDays(1);


                sparesAndExpensesList = db.Job_Card_Header.Where(x => x.Created_At >= start && x.Created_At <= end).

                    Join(db.Job_Card_Details,

                    h => h.Id, d => d.Job_Card_Header_Id,
                     (h, d) => new SparesAndExpensesViewModel()
                     {
                         Id = h.Id,
                         Job_Card_No = h.Id.ToString(),
                         Date = h.Created_At,
                         Fleet_No = h.Fleet_No,
                         Truck_No = h.Truck_head_No,
                         Transit_Type = h.Transit_Type,
                         Trailer_No = h.Truck_Trailer_No,
                         Order_No = d.Order_No,
                         Item_Name = d.Stock_Name,
                         Qty = d.Qty_Requested,
                         Rate = d.Price ?? 0m,
                         Amount = d.Total ?? 0m,
                         Supplier_Int = d.Vendor

                     }).ToList();




            }
            else
            {



                sparesAndExpensesList = db.Job_Card_Header.

                    Join(db.Job_Card_Details,

                    h => h.Id, d => d.Job_Card_Header_Id,
                     (h, d) => new SparesAndExpensesViewModel()
                     {
                         Id = h.Id,
                         Job_Card_No = h.Id.ToString(),
                         Date = h.Created_At,
                         Fleet_No = h.Fleet_No,
                         Truck_No = h.Truck_head_No,
                         Transit_Type = h.Transit_Type,
                         Trailer_No = h.Truck_Trailer_No,
                         Order_No = d.Order_No,
                         Item_Name = d.Stock_Name,
                         Qty = d.Qty_Requested,
                         Rate = d.Price ?? 0m,
                         Amount = d.Total ?? 0m,
                         Supplier_Int = d.Vendor

                     }).ToList();

            }



            if (!string.IsNullOrEmpty(searchString))
            {

                sparesAndExpensesList = sparesAndExpensesList.Where(x => x.Fleet_No.Contains(searchString) || x.Job_Card_No.Contains(searchString)).ToList();

            }


            if (Request.IsAjaxRequest())
            {

        //        return PartialView("_MaintenanceIndexPartialView", maintenanceIndex.ToPagedList(pageNumber, pageSize));
            }
            //      return PartialView("_MaintenanceIndexPartialView", maintenanceIndex.ToPagedList(pageNumber, pageSize));


            Session.Clear();
            Session["SparesAndExpensesReportSession"] = sparesAndExpensesList;

            return View(sparesAndExpensesList.OrderByDescending(x => x.Id).ToPagedList(pageNumber,pageSize));
        }

        [Securities.RolesAuthorization(Roles = Values.ADMIN_ROLE + "," + Values.ACCOUNTANT_ROLE + "," + Values.TRANSPORT_OFFICER_ROLE)]

        public ActionResult CreditSuppliersLedger(int? page, string searchString, string currentFilter, DateTime? currentFilter3, DateTime? currentFilter4, DateTime? start, DateTime? end)
        {


            
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }


            if (start != null)
            {
                page = 1;
            }
            else
            {
                start = currentFilter3;
            }

            ViewBag.CurrentFilter3 = start;



            if (end != null)
            {
                page = 1;
            }
            else
            {
                end = currentFilter4;
            }

            ViewBag.CurrentFilter4 = end;

            ViewBag.CurrentFilter = searchString;
            int pageSize = 10;
            int pageNumber = (page ?? 1);


        //    var sparesAndExpensesList = new List<CreditSuppliersLedgerViewModel>();


            if (searchString != null)
            {
                //         maintenanceIndex = db.Maintenance_Book_Header.Where(x => x.Fleet_No.Contains(searchString) || x.Job_Card_Reference_No.Contains(searchString)).OrderByDescending(x => x.Id).ToList();

            }
            else
            {
                //          maintenanceIndex = db.Maintenance_Book_Header.OrderByDescending(x => x.Id).ToList();
            }

            //  var jobheaders = db.Job_Card_Header.ToList();


    


            IQueryable<CreditSuppliersLedgerViewModel> creditsupplierslist;

            if (start != null && end != null)
            {

                end = end.Value.AddDays(1);
                 creditsupplierslist =
         from h in db.LPO_Header.Where(x => x.CreatedAt >= start && x.CreatedAt <= end )
         join d in db.LPO_Details on h.Id equals d.LPO_Header_Id
         join j in db.Job_Card_Header on h.JOBNumber equals j.Id.ToString()
         join g in db.GRN_Header on h.LPO_Number equals g.PLO_Number
         select new CreditSuppliersLedgerViewModel()
         {
             Id = h.Id,
             Job_Card_No = "JB" + j.Id,
             Date = h.CreatedAt,
             Fleet_No = j.Fleet_No,
             Truck_No = j.Truck_head_No,
             Transit_Type = j.Transit_Type,
             Trailer_No = j.Truck_Trailer_No,
             Order_No = h.LPO_Number,
             Item_Name = d.Item_Name,
             Qty = d.quantity,
             Rate = d.Price,
             Amount = d.Total,
             Supplier_Int = h.Vendor_Id,
             Received = g.Created_At

         };

            }
            else
            {

                creditsupplierslist =
    from h in db.LPO_Header
    join d in db.LPO_Details on h.Id equals d.LPO_Header_Id
    join j in db.Job_Card_Header on h.JOBNumber equals j.Id.ToString()
    join g in db.GRN_Header on h.LPO_Number equals g.PLO_Number
    select new CreditSuppliersLedgerViewModel()
    {
        Id = h.Id,
        Job_Card_No = "JB" + j.Id,
        Date = h.CreatedAt,
        Fleet_No = j.Fleet_No,
        Truck_No = j.Truck_head_No,
        Transit_Type = j.Transit_Type,
        Trailer_No = j.Truck_Trailer_No,
        Order_No = h.LPO_Number,
        Item_Name = d.Item_Name,
        Qty = d.quantity,
        Rate = d.Price,
        Amount = d.Total,
        Supplier_Int = h.Vendor_Id,
        Received = g.Created_At

    };

            }


            if(!string.IsNullOrEmpty(searchString))
            {

                creditsupplierslist = creditsupplierslist.Where(x => x.Fleet_No.Contains(searchString) || x.Job_Card_No.Contains(searchString));

            }



            if (Request.IsAjaxRequest())
            {

                //        return PartialView("_MaintenanceIndexPartialView", maintenanceIndex.ToPagedList(pageNumber, pageSize));
            }
            //      return PartialView("_MaintenanceIndexPartialView", maintenanceIndex.ToPagedList(pageNumber, pageSize));


            Session.Clear();
            Session["CreditSuppliersSession"] = creditsupplierslist;


            return View(creditsupplierslist.ToList().OrderByDescending(x => x.Id).ToPagedList(pageNumber, pageSize));




        }

        [Securities.RolesAuthorization(Roles = Values.ADMIN_ROLE + "," + Values.ACCOUNTANT_ROLE + "," + Values.TRANSPORT_OFFICER_ROLE)]

        public ActionResult CashSuppliersLedger(int? page, string searchString, string currentFilter, DateTime? currentFilter3, DateTime? currentFilter4, DateTime? start, DateTime? end)
        {



            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            if (start != null)
            {
                page = 1;
            }
            else
            {
                start = currentFilter3;
            }

            ViewBag.CurrentFilter3 = start;



            if (end != null)
            {
                page = 1;
            }
            else
            {
                end = currentFilter4;
            }

            ViewBag.CurrentFilter4 = end;



            int pageSize = 10;
            int pageNumber = (page ?? 1);


            var sparesAndExpensesList = new List<CreditSuppliersLedgerViewModel>();


            if (searchString != null)
            {
                //         maintenanceIndex = db.Maintenance_Book_Header.Where(x => x.Fleet_No.Contains(searchString) || x.Job_Card_Reference_No.Contains(searchString)).OrderByDescending(x => x.Id).ToList();

            }
            else
            {
                //          maintenanceIndex = db.Maintenance_Book_Header.OrderByDescending(x => x.Id).ToList();
            }

            //  var jobheaders = db.Job_Card_Header.ToList();

            IQueryable<CreditSuppliersLedgerViewModel> cashsupplierslist;

            if (start != null && end != null && !string.IsNullOrEmpty(searchString))
            {

                end = end.Value.AddDays(1);
                cashsupplierslist =
      from h in db.Payment_Header.Where(x => x.CreatedAt >=start && (x.CreatedAt < end) && ( x.JOBNumber.Contains(searchString)))
      join d in db.Payment_Details on h.Id equals d.Payment_Header_Id
      join j in db.Job_Card_Header on h.JOBNumber equals j.Id.ToString()
           //    join g in db.GRN_Header on h.LPO_Number equals g.PLO_Number
           select new CreditSuppliersLedgerViewModel()
      {
          Id = h.Id,
          Job_Card_No = "JB" + j.Id,
          Date = h.CreatedAt,
          Fleet_No = j.Fleet_No,
          Truck_No = j.Truck_head_No,
          Transit_Type = j.Transit_Type,
          Trailer_No = j.Truck_Trailer_No,
          Order_No = h.Payment_Number,
          Item_Name = d.Item_Name,
          Qty = d.quantity,
          Rate = d.Price,
          Amount = d.Total,
          Supplier_Int = h.Vendor_Id
               //        Received = g.Created_At

           };

            }else if(start != null && end != null && string.IsNullOrEmpty(searchString))
                {               

                    end = end.Value.AddDays(1);
                    cashsupplierslist =
          from h in db.Payment_Header.Where(x => x.CreatedAt >= start && (x.CreatedAt < end ))
          join d in db.Payment_Details on h.Id equals d.Payment_Header_Id
          join j in db.Job_Card_Header on h.JOBNumber equals j.Id.ToString()
      //    join g in db.GRN_Header on h.LPO_Number equals g.PLO_Number
      select new CreditSuppliersLedgerViewModel()
          {
              Id = h.Id,
              Job_Card_No = "JB" + j.Id,
              Date = h.CreatedAt,
              Fleet_No = j.Fleet_No,
              Truck_No = j.Truck_head_No,
              Transit_Type = j.Transit_Type,
              Trailer_No = j.Truck_Trailer_No,
              Order_No = h.Payment_Number,
              Item_Name = d.Item_Name,
              Qty = d.quantity,
              Rate = d.Price,
              Amount = d.Total,
              Supplier_Int = h.Vendor_Id
          //        Received = g.Created_At

      };

                
            }

            else
            {

             cashsupplierslist =
           from h in db.Payment_Header
           join d in db.Payment_Details on h.Id equals d.Payment_Header_Id
           join j in db.Job_Card_Header on h.JOBNumber equals j.Id.ToString()
       //    join g in db.GRN_Header on h.LPO_Number equals g.PLO_Number
       select new CreditSuppliersLedgerViewModel()
           {
               Id = h.Id,
               Job_Card_No = "JB" + j.Id,
               Date = h.CreatedAt,
               Fleet_No = j.Fleet_No,
               Truck_No = j.Truck_head_No,
               Transit_Type = j.Transit_Type,
               Trailer_No = j.Truck_Trailer_No,
               Order_No = h.Payment_Number,
               Item_Name = d.Item_Name,
               Qty = d.quantity,
               Rate = d.Price,
               Amount = d.Total,
               Supplier_Int = h.Vendor_Id
           //        Received = g.Created_At

       };


            }



            if (!string.IsNullOrEmpty(searchString))
            {

                cashsupplierslist = cashsupplierslist.Where(x => x.Fleet_No.Contains(searchString) || x.Job_Card_No.Contains(searchString));

            }

            Session.Clear();
            Session["CashSuppliersSession"] = cashsupplierslist;


            return View(cashsupplierslist.OrderByDescending(x => x.Id).ToPagedList(pageNumber, pageSize));




        }

        public ActionResult ExportReportToExcel()
        {


            if (Session["CashSuppliersSession"] != null)
            {

                var cashsupplierslist = (IQueryable<CreditSuppliersLedgerViewModel>)Session["CashSuppliersSession"];


                string[] columns = { "Date", "Fleet_No", "Truck_No", "Trailer_No", "Transit_Type", "Job_Card_No", "Order_No", "Item_Name", "Qty", "Rate", "Amount", "Supplier", "Received" };


                byte[] filecontent = ExcelExportHelper.ExportExcel(cashsupplierslist.ToList(), "Cash Suppliers Ledger", false, columns);
                if (filecontent != null)
                {
                    Session["CashSuppliersSession"] = null;
                    return File(filecontent, ExcelExportHelper.ExcelContentType, "CashSuppliersLedger.xlsx");
                }

                Session["CashSuppliersSession"] = null;
                return RedirectToAction("CashSuppliersLedger");

            }else if(Session["CreditSuppliersSession"] != null)
            {

                var creditsupplierslist = (IQueryable<CreditSuppliersLedgerViewModel>)Session["CreditSuppliersSession"];


                string[] columns = { "Date", "Fleet_No", "Truck_No", "Trailer_No", "Transit_Type", "Job_Card_No", "Order_No", "Item_Name", "Qty", "Rate", "Amount", "Supplier", "Received" };


                byte[] filecontent = ExcelExportHelper.ExportExcel(creditsupplierslist.ToList(), "Credit Suppliers Ledger", false, columns);
                if (filecontent != null)
                {
                    Session["CreditSuppliersSession"] = null;
                    return File(filecontent, ExcelExportHelper.ExcelContentType, "CreditSuppliersLedger.xlsx");
                }

                Session["CreditSuppliersSession"] = null;
                return RedirectToAction("CreditSuppliersLedger");



            }else if(Session["SparesAndExpensesReportSession"] != null)
            {

                var sparesandexpenseslist = (List<SparesAndExpensesViewModel>)Session["SparesAndExpensesReportSession"];




                string[] columns = { "Date", "Fleet_No", "Truck_No", "Trailer_No", "Transit_Type", "Job_Card_No", "Order_No", "Item_Name", "Qty", "Rate", "Amount", "Supplier" };


                byte[] filecontent = ExcelExportHelper.ExportExcel(sparesandexpenseslist.ToList(), "Spares And Expenses Report", false, columns);
                if (filecontent != null)
                {
                    Session["SparesAndExpensesReportSession"] = null;
                    return File(filecontent, ExcelExportHelper.ExcelContentType, "SparesAndExpensesReport.xlsx");
                }

                Session["SparesAndExpensesReportSession"] = null;
                return RedirectToAction("SparesAndExpenses");
            }else if (Session["PriceListSession"] != null)
            {

                var pricelist = (List<PriceListViewModel>)Session["PriceListSession"];



                string[] columns = { "Vendor_Name", "Item_Name", "Unit_Of_Measure", "Buying_Price", "Buying_Price_No_Tax", "Taxable_String" };


                byte[] filecontent = ExcelExportHelper.ExportExcel(pricelist, "MOIL Pricelist As At " + DateTime.Now.ToShortDateString(), false, columns);
                if (filecontent != null)
                {
                    //              Session["PriceListSession"] = null;
                    return File(filecontent, ExcelExportHelper.ExcelContentType, "PriceList.xlsx");
                }

                //           Session["PriceListSession"] = null;
                return RedirectToAction("PricelistReport");
            }
            else if (Session["StockListSession"] != null)
            {
                var stocklist = (List<StockLedgerViewModel>)Session["StockListSession"];



                string[] columns = { "Location_String", "Item_Name", "Buy_Price", "Sell_Price", "Currency_Name", "UoM", "Qty_Bal", "Bal_Cost_TZS" };


                byte[] filecontent = ExcelExportHelper.ExportExcel(stocklist, "MOIL Stocklist As At " + DateTime.Now.ToShortDateString(), false, columns);
                if (filecontent != null)
                {
                    //             Session["StockListSession"] = null;
                    return File(filecontent, ExcelExportHelper.ExcelContentType, "StockList.xlsx");
                }

                //        Session["StockListSession"] = null;
                return RedirectToAction("StockListReport");

            }
            else if (Session["StockListLedgerSession"] != null)
            {
                var stocklist = (List<StockLedgerViewModel>)Session["StockListLedgerSession"];



                string[] columns = { "Created_Date", "Item_Name", "UoM", "Location_String", "Consumed_Location", "Qty_In", "Qty_Out", "Cost_In", "Cost_Out", "Qty_Bal", "Bal_Cost_TZS", "Bal_Cost_USD" };


                byte[] filecontent = ExcelExportHelper.ExportExcel(stocklist, "MOIL Stockledger As At " + DateTime.Now.ToShortDateString(), false, columns);
                if (filecontent != null)
                {
                    //       Session["StockListLedgerSession"] = null;
                    return File(filecontent, ExcelExportHelper.ExcelContentType, "StockLedger.xlsx");
                }

                //       Session["StockListLedgerSession"] = null;
                return RedirectToAction("StockLedgerReport");

            }


            return Redirect(Request.UrlReferrer.ToString());

        }




            }
        }