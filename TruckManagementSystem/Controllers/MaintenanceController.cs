﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Helpers;
using TruckManagementSystem.Models;
using TruckManagementSystem.ViewModels;

namespace TruckManagementSystem.Controllers
{
    [Authorize]
    public class MaintenanceController : Controller
    {

        ApplicationDbContext db = new ApplicationDbContext();



        public ActionResult Maintenance()
        {




            return View();
        }

        // GET: Maintenance
        public ActionResult Index(int? page, string searchString, string currentFilter, int?statusid, int?currentFilter1)
        {

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;


            if (statusid != null)
            {
                page = 1;
            }
            else
            {
                statusid = currentFilter1;
            }

            ViewBag.CurrentFilter1 = statusid;





            int pageSize = 10;
            int pageNumber = (page ?? 1);


            var maintenanceIndex = new List<Maintenance_Book_Header>();
            if (!string.IsNullOrEmpty(searchString) && statusid == null)
            {
                maintenanceIndex = db.Maintenance_Book_Header.Where(x => x.Fleet_No.Contains(searchString) || x.Job_Card_Reference_No.Contains(searchString)).OrderByDescending(x => x.Id).ToList();

            }else if(!string.IsNullOrEmpty(searchString) && statusid != null)
            {
                maintenanceIndex = db.Maintenance_Book_Header.Where(x => (x.Fleet_No.Contains(searchString) || x.Job_Card_Reference_No.Contains(searchString)) && x.Status == statusid).OrderByDescending(x => x.Id).ToList();


            }
            else if (string.IsNullOrEmpty(searchString) && statusid != null)
            {
                maintenanceIndex = db.Maintenance_Book_Header.Where(x =>  x.Status == statusid).OrderByDescending(x => x.Id).ToList();


            }
            else
            {
                maintenanceIndex = db.Maintenance_Book_Header.OrderByDescending(x => x.Created_At).ToList();
            }



            List<SelectListViewModel> StatusList = new List<SelectListViewModel>();


            var intlist = new List<int>()
            {
               Values.STATUS_PENDING,
                Values.STATUS_WAITING_FOR_TRANSPORT_MANAGER,
                Values.STATUS_WAITING_FOR_ACCOUNTANT,             
                Values.STATUS_WAITING_FOR_COUNTRY_MANAGER,
               
            };

            foreach(var item in intlist)
            {

                var item1 = new SelectListViewModel();
                item1.Text = Values.STATUS_STRING(item);
                item1.Value = item.ToString();
                StatusList.Add(item1);

            }


            //      ViewBag.remark = RemarksList.Add(new SelectListViewModel() { Text = "Northern Cape", Value = "NC" });

            ViewBag.Status_Select = StatusList;
            //var item2 = new SelectListViewModel();
            //item2.Text = "No Action";
            //item2.Value = "No Action";

            //RemarksList.Add(item2);

            if (Request.IsAjaxRequest())
            {

                return PartialView("_MaintenanceIndexPartialView", maintenanceIndex.ToPagedList(pageNumber, pageSize));
            }
            return PartialView("_MaintenanceIndexPartialView", maintenanceIndex.ToPagedList(pageNumber, pageSize));


        }


        public ActionResult Create()
        {

            var repairviewmodel = new RepairViewModel();

            repairviewmodel.Created_Date = DateTime.Now.ToString();
            var defaultline = new Repair_Line_Item();
            defaultline.Item = "";
            repairviewmodel.Line_Item = new List<Repair_Line_Item>();


            repairviewmodel.Line_Item.Add(defaultline);
            if (Request.IsAjaxRequest())
            {



                return PartialView("_MaintenanceCreatePartialView", repairviewmodel);
            }
            return View("_MaintenanceCreatePartialView", repairviewmodel);


        }


        public ActionResult GetFleet(string searchstring)
        {

            var fleetlist = db.Fleets.Where(x => x.Fleet_No.Contains(searchstring)).Select(x => new
            {
                label = x.Fleet_No,
                value = x.Id.ToString()
            });



            return Json(fleetlist.Take(15), JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetFleetDetails(int id)
        {

            var fleetobject = db.Fleets.Find(id);






            return Json(fleetobject, JsonRequestBehavior.AllowGet);






        }

        public ActionResult GetStock(string search)
        {
                       
            var location = db.Location.Where(x => x.Name == Values.MAIN_STORE).Select(x => x.Id).FirstOrDefault();

            var stockobj = db.Stock.Where(X => X.Item_Name.Contains(search) || X.Part_Number.Contains(search)).Select(x => new StockLedgerViewModel()

            {

                Stock_Id = x.Stock_Id,
                Item_Name = x.Item_Name,

                Location_Id = x.Location_Id,
                Item_Id = x.Item_Id,
                Qty_Bal = x.Qty_Bal,
                Qty_In = x.Qty_In,
                settled = x.Settled

            });

            var stocklocation = stockobj.Where(l => l.Location_Id == location).ToList();

            var trackerobject = db.Qty_Bal_Tracker.ToList();

            List<int> la = new List<int>();

            foreach (var q in stocklocation.GroupBy(x => x.Item_Id))
            {

                var t = q.Select(x => new { Qty_Bal = x.Qty_Bal }).Last().Qty_Bal;

                var k = q.Select(x => x.Item_Id).LastOrDefault();

                var sum = db.Qty_Bal_Tracker.Where(x => x.Item_Id == k && x.Location_Id == location).ToList().Sum(x => x.Qty_Bal);


                if ((t - sum) <= 0)
                {
                    var p = q.Select(x => x.Item_Id).LastOrDefault();

                    la.Add(p);
                }

            }



            var barItems = stockobj.Where(t => !la.Contains(t.Item_Id) && t.Location_Id == location).ToList();



            var groupedList = new List<Stock>();


            foreach (var item in barItems.GroupBy(x => x.Item_Id))
            {
                var tempstock = new Stock();

                var stockcheck = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Id).FirstOrDefault();
                if (stockcheck != 0)
                {



                    tempstock.Stock_Id = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Stock_Id).FirstOrDefault();
                    tempstock.Item_Id = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Id).FirstOrDefault();
                    tempstock.Item_Name = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Name).FirstOrDefault();


                    groupedList.Add(tempstock);
                }
                else
                {
                    tempstock.Stock_Id = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Stock_Id).LastOrDefault();
                    tempstock.Item_Id = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Item_Id).LastOrDefault();
                    tempstock.Item_Name = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Item_Name).LastOrDefault();




                    groupedList.Add(tempstock);

                }
            }


            var json = string.Empty;

            var result = new List<Dictionary<string, string>>();


            var barItem = new[] { new { label = "", value = "" } }.AsEnumerable();

            var culture = CultureInfo.CurrentCulture;


            barItem = groupedList.Where(x => culture.CompareInfo.IndexOf(x.Item_Name ?? "", search, CompareOptions.IgnoreCase) >= 0).ToList().Select(x => new { label = x.Item_Name, value = x.Item_Id.ToString() + "," + x.Stock_Id.ToString() });


            var excludelist = barItem.Select(x => x.label).ToList();

            var locid = db.Location.Where(x => x.Name == Values.MAIN_STORE).Select(x => x.Id).FirstOrDefault();

            var priceexcluded = db.Price_List.Where(x => (!excludelist.Any(e => x.Item_Name.Contains(e))) && x.Location_Id == locid).ToList();
            //get from pricelist

            var nonstockitems = priceexcluded.Where(x => x.Item_Name.ToLower().Contains(search.ToLower()) || ( !string.IsNullOrEmpty(x.Part_Number) && x.Part_Number.Contains(search))).Select(x => new { label = x.Item_Name, value = x.Item_Id.ToString() + ",P" + x.Id.ToString() });
            //var nonstockitems = db.Price_List.Where(x => x.Item_Name.ToLower().Contains(search.ToLower())).Select(x => new { label = x.Item_Name, value = x.Item_Id.ToString() });

            barItem = barItem.Concat(nonstockitems);

            
            // Join bar items without stock

            //
            var baritem1 = new[] { new { label = "", value = "" } }.AsEnumerable();

     baritem1 = barItem.GroupBy(x => x.label).Select(x => new { x.FirstOrDefault().label, x.FirstOrDefault().value });
            string resultobject = JsonConvert.SerializeObject(result);





            return Json(barItem.Take(15), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetStockDetails(string id)
        {

            string[] stringarr = id.Split(',');
            int itemid = 0;
            int stockid = 0;
            int pricelistid = 0;



            var location = db.Location.Where(x => x.Name == Values.MAIN_STORE).Select(x => x.Id).FirstOrDefault();


            if (id.ToLower().Contains(','))
            {
                itemid = Convert.ToInt32(stringarr[0]);


                var isNumeric = int.TryParse(stringarr[1], out int n);

                if (isNumeric)
                {

                    stockid = Convert.ToInt32(stringarr[1]);
                }else
                {
                    stockid = 0;

                    string extract = Regex.Replace(stringarr[1], "[^0-9]+", string.Empty);

                    pricelistid = Convert.ToInt32(extract);

                }
            }
            else
            {
                itemid = Convert.ToInt32(id);

            }



            var fleetobjectbefore = db.Stock.Where(x => x.Stock_Id == stockid).FirstOrDefault();


            var fleetobject = new StockDetailsViewModel();

            if (fleetobjectbefore != null)
            { 

            fleetobject.Item_Name = fleetobjectbefore.Item_Name;

            fleetobject.Stock_Id = fleetobjectbefore.Stock_Id;

                fleetobject.Part_Number = fleetobjectbefore.Part_Number;
                fleetobject.Unit = fleetobjectbefore.UoM;


            fleetobject.Qty_Bal = db.Stock.Where(y => y.Item_Id == fleetobjectbefore.Item_Id && y.Location_Id == location).OrderByDescending(y => y.Stock_Id).Select(y => y.Qty_Bal).Take(1).FirstOrDefault() - (db.Qty_Bal_Tracker.Where(y => y.Item_Id == fleetobjectbefore.Item_Id && y.Location_Id == location).ToList().Sum(y => (int?)y.Qty_Bal) ?? 0);


            fleetobject.Buy_Price = fleetobjectbefore.Buy_Price;

            if (fleetobjectbefore.Qty_Bal > 0)
            {

                fleetobject.Vendor_Id = db.Vendor.Where(x => x.Supp_Name == "Issue Note").Select(x => x.Supp_Id).FirstOrDefault();
            }else
                {



                }
        }else
            {

                var priceobjectbefore = db.Price_List.Find(pricelistid);


                fleetobject.Item_Name = priceobjectbefore.Item_Name;

                fleetobject.Stock_Id = 0;


                fleetobject.Qty_Bal = 0;

                string uomname = db.IC_UoM.Find(priceobjectbefore.UoM_Id).UoM; 

                fleetobject.Unit = uomname;

                fleetobject.Part_Number = priceobjectbefore.Part_Number;


                fleetobject.Buy_Price = priceobjectbefore.Buying_Price;

                fleetobject.Vendor_Id = priceobjectbefore.Vendor_Id;





            }

            return Json(fleetobject, JsonRequestBehavior.AllowGet);

        }



        [HttpPost]
        public ActionResult Create(RepairViewModel repairViewModel)
        {
            if (ModelState.IsValid)
            {

                var fleetobject = db.Fleets.Where(x => x.Fleet_No.Equals(repairViewModel.Fleet_No)).FirstOrDefault();
                var maintenancehead = new Maintenance_Book_Header();





                maintenancehead.Transport_Officer = System.Web.HttpContext.Current.User.Identity.Name;






                maintenancehead.Created_At =  Helpers.Helpers.GetDateTime(repairViewModel.Created_Date);
                maintenancehead.Driver_Name = repairViewModel.Driver_Name;
                maintenancehead.Licence_No = repairViewModel.Licence_No;
                maintenancehead.Status = Values.STATUS_PENDING;
                maintenancehead.Truck_head_No = repairViewModel.Truck_Head_No;
                maintenancehead.Truck_Trailer_No = repairViewModel.Truck_Trailer_No;
                maintenancehead.Fleet_No = repairViewModel.Fleet_No;
                maintenancehead.Transit_Type = fleetobject.Transit_Type_String;
                maintenancehead.Kilometers = repairViewModel.Kilometers;

                db.Maintenance_Book_Header.Add(maintenancehead);
                db.SaveChanges();


                var detailList = new List<Maintenance_Book_Details>();
                foreach (var detail in repairViewModel.Line_Item)
                {

                    var bookDetail = new Maintenance_Book_Details();

                    bookDetail.Item = detail.Item;
                    bookDetail.Maintainance_header_id = maintenancehead.Id;

                    detailList.Add(bookDetail);

                }


                db.Maintenance_Book_Details.AddRange(detailList);

                db.SaveChanges();


                return RedirectToAction("Maintenance");



            }



            return View("_MaintenanceCreatePartialView", repairViewModel);
        }


        public ActionResult Edit(int? idparam)
        {

            if (idparam == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var repairentry = db.Maintenance_Book_Header.Find(idparam);
            if (repairentry == null)
            {
                return HttpNotFound();
            }

            var repairentrydetails = db.Maintenance_Book_Details.Where(x => x.Maintainance_header_id == idparam).ToList();
            var repairLineItemList = new List<Repair_Line_Item>();

            foreach (var item in repairentrydetails)
            {
                var itemline = new Repair_Line_Item();

                itemline.Details_Id = item.Id;
                itemline.Item = item.Item;
                itemline.Qty_Requested = item.Qty_Requested;
                itemline.Stock_Id = item.Stock_Id ?? 0;
                itemline.Stock_Name = item.Stock_Name;
                itemline.Stock_Qty = item.Qty_Available;
                itemline.Price = item.Price;
                itemline.Remark = item.Remarks;
                itemline.Status = item.Status;
                itemline.Vendor = item.Vendor;

                if (item.Transport_Officer == 1)
                {
                    itemline.Transport_Officer = true;
                }
                else
                {
                    itemline.Transport_Officer = false;
                }


                repairLineItemList.Add(itemline);


            }

            var truckid = db.Fleets.Where(x => x.Fleet_No == repairentry.Fleet_No).Select(x => x.Truck_Head_Id).FirstOrDefault();

            var make = db.Trucks.Find(truckid).Make;

            var repairvm = new RepairViewModel();
            repairvm.Header_Id = repairentry.Id;
            repairvm.Created_Date = repairentry.Created_At.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
            repairvm.Driver_Name = repairentry.Driver_Name;
            repairvm.Message = repairentry.Message;
            repairvm.Fleet_No = repairentry.Fleet_No;
            repairvm.Make = make;


            repairvm.Licence_No = repairentry.Licence_No;
            repairvm.Truck_Head_No = repairentry.Truck_head_No;
            repairvm.Truck_Trailer_No = repairentry.Truck_Trailer_No;
            repairvm.Line_Item = repairLineItemList;
            repairvm.Status = repairentry.Status;
            repairvm.Job_Card_Reference_No = repairentry.Job_Card_Reference_No;
            repairvm.Kilometers = repairentry.Kilometers;

            List<SelectListViewModel> RemarksList = new List<SelectListViewModel>();

            //      ViewBag.remark = RemarksList.Add(new SelectListViewModel() { Text = "Northern Cape", Value = "NC" });
            var item1 = new SelectListViewModel();
            item1.Text = "Needs Fixing";
            item1.Value = "Needs Fixing";
            RemarksList.Add(item1);

            var item2 = new SelectListViewModel();
            item2.Text = "No Action";
            item2.Value = "No Action";

            RemarksList.Add(item2);


            ViewBag.Remarks = RemarksList;

            ViewBag.Vendors = db.Vendor.ToList();



            if (!string.IsNullOrEmpty(repairvm.Message) && repairvm.Status == Values.STATUS_SENT_BACK_FOR_EDIT)

            {
                TempData["SendBackMessage"] = repairvm.Message;

            }
            else if (!string.IsNullOrEmpty(repairvm.Message) && repairvm.Status != Values.STATUS_SENT_BACK_FOR_EDIT)
            {
                TempData["SendBackMessageProccess"] = repairvm.Message;


            }





            return PartialView("_MaintenanceEditPartialView", repairvm);



        }

        [HttpPost]
        public ActionResult Edit(RepairViewModel repairViewModel)
        {


            if (ModelState.IsValid)
            {

                var header = db.Maintenance_Book_Header.Find(repairViewModel.Header_Id);

                if ((System.Web.HttpContext.Current.User.IsInRole(Values.WORKSHOP_MANAGER_ROLE) || System.Web.HttpContext.Current.User.IsInRole(Values.ADMIN_ROLE)) && (header.Status == Values.STATUS_PENDING || header.Status == Values.STATUS_SENT_BACK_FOR_EDIT))
                {
                    header.Status = Values.STATUS_WAITING_FOR_TRANSPORT_MANAGER;
                    header.Workshop_Manager = System.Web.HttpContext.Current.User.Identity.Name;

                }
                //else if ((System.Web.HttpContext.Current.User.IsInRole(Values.OPERATIONS_MANAGER) || System.Web.HttpContext.Current.User.IsInRole(Values.ADMIN_ROLE)) && header.Status == Values.STATUS_WAITING_FOR_OPERATIONS_MANAGER)
                //{
                //    header.Status = Values.STATUS_WAITING_FOR_TRANSPORT_MANAGER;
                //    header.Operations_Manager = System.Web.HttpContext.Current.User.Identity.Name;

                //}
                else if ((System.Web.HttpContext.Current.User.IsInRole(Values.TRANSPORT_MANAGER_ROLE) || System.Web.HttpContext.Current.User.IsInRole(Values.ADMIN_ROLE)) && header.Status == Values.STATUS_WAITING_FOR_TRANSPORT_MANAGER)
                {
                    header.Status = Values.STATUS_WAITING_FOR_ACCOUNTANT;
                    header.Transport_Manager = System.Web.HttpContext.Current.User.Identity.Name;
                }
                else if ((System.Web.HttpContext.Current.User.IsInRole(Values.ACCOUNTANT_ROLE) || System.Web.HttpContext.Current.User.IsInRole(Values.ADMIN_ROLE)) && header.Status == Values.STATUS_WAITING_FOR_ACCOUNTANT)
                {
                    header.Status = Values.STATUS_WAITING_FOR_COUNTRY_MANAGER;
                    header.Accountant = System.Web.HttpContext.Current.User.Identity.Name;
                }
                else if ((System.Web.HttpContext.Current.User.IsInRole(Values.COUNTRY_MANAGER) || System.Web.HttpContext.Current.User.IsInRole(Values.ADMIN_ROLE)) && header.Status == Values.STATUS_WAITING_FOR_COUNTRY_MANAGER)
                {
                    header.Status = Values.STATUS_PENDING_JOB_CARD;
                    header.Country_Manager = System.Web.HttpContext.Current.User.Identity.Name;
                }

                header.Job_Card_Reference_No = repairViewModel.Job_Card_Reference_No;
                header.Created_At = Helpers.Helpers.GetDateTime(repairViewModel.Created_Date);  
                header.Fleet_No = repairViewModel.Fleet_No;
                header.Driver_Name = repairViewModel.Driver_Name;
                header.Truck_head_No = repairViewModel.Truck_Head_No;
                header.Truck_Trailer_No = repairViewModel.Truck_Trailer_No;
                header.Licence_No = repairViewModel.Licence_No;
                header.Kilometers = repairViewModel.Kilometers;


                db.Entry(header).State = System.Data.Entity.EntityState.Modified;

                var cashvendorid = db.Vendor.Where(x => x.Supp_Name == "Cash TZ").Select(x => x.Supp_Id).FirstOrDefault();


                var locationid = db.Location.Where(x => x.Name == Values.MAIN_STORE).Select(x => x.Id).FirstOrDefault();



                var qtyballist = new List<Qty_Bal_Tracker>();

                var detaillistfordelete = db.Maintenance_Book_Details.Where(x => x.Maintainance_header_id == header.Id).ToList();

                if (detaillistfordelete != null)
                {
                    db.Maintenance_Book_Details.RemoveRange(detaillistfordelete);
                    db.SaveChanges();
                }
                foreach (var item in repairViewModel.Line_Item)
                {

                    var itemid = 0;

                    if (item.Stock_Id != 0)
                    {
                        itemid = db.Stock.Find(item.Stock_Id).Item_Id;
                    }
                    var qtybalobj = db.Qty_Bal_Tracker.Where(x => x.Item_Id == itemid && x.Order_Id == repairViewModel.Header_Id).FirstOrDefault();

                    var singleline = db.Maintenance_Book_Details.Find(item.Details_Id);
                    if (singleline != null)
                    {
                        singleline.Item = item.Item;
                        singleline.Stock_Id = item.Stock_Id;
                        singleline.Stock_Name = item.Stock_Name;
                        
                        singleline.Qty_Available = item.Stock_Qty;
                        singleline.Price = item.Price;
                        singleline.Vendor = item.Vendor;
                        singleline.Qty_Requested = item.Qty_Requested;
                        singleline.Remarks = item.Remark;
                        singleline.Total = Math.Round(item.Price??0 * item.Qty_Requested, 2);

                        if (item.Transport_Officer == true)
                        {
                            singleline.Transport_Officer = 1;
                        }
                        else
                        {
                            singleline.Transport_Officer = 0;

                        }
                        var netqty = singleline.Qty_Available - singleline.Qty_Requested;


                        if (item.Stock_Name == null)
                        {
                            singleline.Status = Values.STATUS_DOES_NOT_EXIST_IN_STOCK;
                            singleline.Net_Qty_Requested = item.Qty_Requested;
                        }
                        else if (item.Stock_Name != null && netqty > 0)
                        {

                            singleline.Status = Values.STATUS_IS_IN_STOCK;
                            singleline.Net_Qty_Requested = item.Qty_Requested;


                            if (header.Status == Values.STATUS_WAITING_FOR_TRANSPORT_MANAGER)
                            {

                                // Insert into temp balance table
                                if (qtybalobj != null)
                                {
                                    db.Qty_Bal_Tracker.Remove(qtybalobj);

                                }
                                var baltracker = new Qty_Bal_Tracker();

                                baltracker.Item_Id = itemid;
                                baltracker.Order_Id = repairViewModel.Header_Id;
                                //var track = trackerobject.Where(x => x.Item_Id == baltracker.Item_Id && x.Location_Id == orderdetails.Location_Id).ToList().Sum(x => x.Qty_Bal);
                                //if (track != 0)
                                //{
                                baltracker.Qty_Bal = item.Qty_Requested;
                                //}
                                //else
                                //{
                                //    baltracker.Qty_Bal = Convert.ToInt32(_qty[i]);

                                //}


                                baltracker.Location_Id = locationid;

                                qtyballist.Add(baltracker);


                            }

                        }
                        else if (item.Stock_Name != null && netqty < 0)
                        {
                            singleline.Status = Values.STATUS_IS_OUT_OF_STOCK;
                            singleline.Net_Qty_Requested = System.Math.Abs(netqty);



                        }


                        db.Entry(singleline).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        var newentry = new Maintenance_Book_Details();

                        newentry.Maintainance_header_id = header.Id;
                        newentry.Item = item.Item;
                        newentry.Stock_Id = item.Stock_Id;
                        newentry.Vendor = item.Vendor;
                        newentry.Price = item.Price;
                        newentry.Stock_Name = item.Stock_Name;
                        newentry.Qty_Available = item.Stock_Qty;
                        newentry.Qty_Requested = item.Qty_Requested;
                        newentry.Remarks = item.Remark;
                        newentry.Total = Math.Round(newentry.Price??0 * newentry.Qty_Requested,2);

                        if (item.Transport_Officer == true)
                        {
                            newentry.Transport_Officer = 1;
                        }
                        else
                        {
                            newentry.Transport_Officer = 0;

                        }
                        var netqty = newentry.Qty_Available - newentry.Qty_Requested;


                        if (item.Stock_Name == null)
                        {
                            newentry.Status = Values.STATUS_DOES_NOT_EXIST_IN_STOCK;
                            newentry.Net_Qty_Requested = item.Qty_Requested;
                        }
                        else if (item.Stock_Name != null && netqty > 0)
                        {

                            newentry.Status = Values.STATUS_IS_IN_STOCK;
                            newentry.Net_Qty_Requested = item.Qty_Requested;


                            if (header.Status == Values.STATUS_WAITING_FOR_TRANSPORT_MANAGER)
                            {

                                // Insert into temp balance table
                                if (qtybalobj != null)
                                {
                                    db.Qty_Bal_Tracker.Remove(qtybalobj);

                                }
                                var baltracker = new Qty_Bal_Tracker();

                                baltracker.Item_Id = itemid;
                                baltracker.Order_Id = repairViewModel.Header_Id;
                                //var track = trackerobject.Where(x => x.Item_Id == baltracker.Item_Id && x.Location_Id == orderdetails.Location_Id).ToList().Sum(x => x.Qty_Bal);
                                //if (track != 0)
                                //{
                                baltracker.Qty_Bal = item.Qty_Requested;
                                //}
                                //else
                                //{
                                //    baltracker.Qty_Bal = Convert.ToInt32(_qty[i]);

                                //}


                                baltracker.Location_Id = locationid;

                                qtyballist.Add(baltracker);


                            }

                        }
                        else if (item.Stock_Name != null && netqty < 0)
                        {
                            newentry.Status = Values.STATUS_IS_OUT_OF_STOCK;
                            newentry.Net_Qty_Requested = System.Math.Abs(netqty);



                        }


                        db.Maintenance_Book_Details.Add(newentry);



                    }





                }

                db.Qty_Bal_Tracker.AddRange(qtyballist);

                db.SaveChanges();

                if (header.Status == Values.STATUS_PENDING_JOB_CARD)
                {

                    var jbheader = new Job_Card_Header();
                    jbheader.Maintenance_header_Id = header.Id;

                    jbheader.Job_Card_Reference_No = header.Job_Card_Reference_No;

                    jbheader.Licence_No = header.Licence_No;
                    jbheader.Status = header.Status;
                    jbheader.Transit_Type = header.Transit_Type;
                    jbheader.Fleet_No = header.Fleet_No;
                    jbheader.Truck_head_No = header.Truck_head_No;
                    jbheader.Truck_Trailer_No = header.Truck_Trailer_No;
                    jbheader.Driver_Name = header.Driver_Name;
                    jbheader.Driver_Clerk = header.Driver_Clerk;
                    jbheader.Operations_Manager = header.Operations_Manager;
                    jbheader.Workshop_Manager = header.Workshop_Manager;
                    jbheader.Transport_Officer = header.Transport_Officer;
                    jbheader.Transport_Manager = header.Transport_Manager;
                    jbheader.Accountant = header.Accountant;
                    jbheader.Country_Manager = header.Country_Manager;
                    jbheader.Created_At = DateTime.Now;
                    jbheader.Kilometers = header.Kilometers;

                    var headercheck = db.Job_Card_Header.Find(jbheader.Id);

                    if (headercheck == null)
                    {
                        db.Job_Card_Header.Add(jbheader);
                        db.SaveChanges();

                        var jobdetails = new List<Job_Card_Details>();

                        foreach (var item in repairViewModel.Line_Item)
                        {
                            var singleline = new Job_Card_Details();
                            singleline.Item = item.Item;
                            singleline.Job_Card_Header_Id = jbheader.Id;
                            singleline.Price = item.Price;
                            singleline.Stock_Id = item.Stock_Id;
                            singleline.Stock_Name = item.Stock_Name;
                            singleline.Qty_Available = item.Stock_Qty;
                            singleline.Qty_Requested = item.Qty_Requested;
                            singleline.Vendor = item.Vendor;
                            singleline.Total =  Math.Round(singleline.Price??0 * singleline.Qty_Requested);

                            if (!string.IsNullOrEmpty(singleline.Stock_Name) && singleline.Qty_Available > 0)
                            {
                                singleline.Status = Values.STATUS_ISSUE_NOTE;
                            }
                            else if (!string.IsNullOrEmpty(singleline.Stock_Name) && singleline.Qty_Available == 0)
                            {
                                singleline.Status = Values.STATUS_LPO;

                            }
                            singleline.Remarks = item.Remark;
                            if (item.Transport_Officer == true)
                            {
                                singleline.Transport_Officer = 1;
                            }
                            else
                            {
                                singleline.Transport_Officer = 0;

                            }
                            var netqty = singleline.Qty_Available - singleline.Qty_Requested;

                            if (item.Stock_Name == null)
                            {
                                singleline.Status = Values.STATUS_LPO;
                                singleline.Net_Qty_Requested = item.Qty_Requested;
                            }
                            else if (item.Stock_Name != null && netqty > 0)
                            {

                                singleline.Status = Values.STATUS_ISSUE_NOTE;
                                singleline.Net_Qty_Requested = item.Qty_Requested;


                            }
                            else if (item.Stock_Name != null && netqty < 0)
                            {
                                singleline.Status = Values.STATUS_LPO;
                                singleline.Net_Qty_Requested = System.Math.Abs(netqty);

                            }

                            if (item.Vendor == cashvendorid)
                            {
                                singleline.Status = Values.STATUS_PETTY_CASH_VOUCHER;
                                singleline.Net_Qty_Requested = System.Math.Abs(netqty);

                            }



                            jobdetails.Add(singleline);
                            db.Job_Card_Details.Add(singleline);
                        }

                        db.SaveChanges();

                        // Create LPOs and Issue Notes

                        jbheader.Status = Values.STATUS_JOB_CARD_HAS_PENDING_LPOS;


                        var headerlist = new List<LPO_Header>();

                        var lpodetaillist = new List<LPO_Details>();

                        var headerlistIssue = new List<Issue_Note_Header>();

                        var detaillistIssue = new List<Issue_Note_Details>();


                        var headerlistpettycash = new List<Payment_Header>();

                        var lpodetaillistpettycash = new List<Payment_Details>();

                        int i = 1;
                        var issueid = db.Vendor.Where(x => x.Supp_Name == "Issue Note").Select(x => x.Supp_Id).FirstOrDefault();

                        foreach (var item in jobdetails)
                        {


                            if (item.Status == Values.STATUS_ISSUE_NOTE && item.Vendor == issueid)
                            {

                                var lpohead = new Issue_Note_Header();
                                if (!headerlistIssue.Select(x => x.Vendor_Id).Contains(item.Vendor))
                                {
                                    //    var lpohead = new LPO_Header();

                                    lpohead.UserName = jbheader.Workshop_Manager;

                                    lpohead.Id = i;


                                    lpohead.Vendor_Id = item.Vendor;

                                    lpohead.Reference_No = jbheader.Id;

                                    lpohead.Type = Values.ISSUE_NOTE_MAINTENANCE;



                                    lpohead.CreatedAt = DateTime.Now;

                                    lpohead.Currency = "TZS";

                                    headerlistIssue.Add(lpohead);

                                    i++;

                                }
                                else
                                {
                                    lpohead = headerlistIssue.Where(x => x.Vendor_Id == item.Vendor).FirstOrDefault();


                                }

                                var issuedetail = new Issue_Note_Details();
                                if (!string.IsNullOrEmpty(item.Stock_Name))
                                {
                                    issuedetail.Item_Name = item.Stock_Name;
                                }
                                else
                                {
                                    issuedetail.Item_Name = item.Item;
                                }
                                issuedetail.Stock_Quantity = item.Qty_Available;
                                issuedetail.quantity = item.Qty_Requested;
                                issuedetail.Stock_Id = item.Stock_Id ?? 0;
                                issuedetail.Price = item.Price ?? 0;
                                issuedetail.Unit = "pc";
                                issuedetail.Currency = "TZS";
                                issuedetail.Issue_Note_Header_Id = lpohead.Id;

                                issuedetail.Total = Math.Round((issuedetail.Price * issuedetail.quantity), 2);

                                detaillistIssue.Add(issuedetail);



                                //issue note

                            }
                            else if (item.Status == Values.STATUS_LPO)
                            {
                                var lpohead = new LPO_Header();
                                if (!headerlist.Select(x => x.Vendor_Id).Contains(item.Vendor))
                                {
                                    //    var lpohead = new LPO_Header();

                                    lpohead.UserName = jbheader.Workshop_Manager;

                                    lpohead.Id = i;


                                    lpohead.Vendor_Id = item.Vendor;

                                    lpohead.JOBNumber = jbheader.Id.ToString();

                                    lpohead.CreatedAt = DateTime.Now;

                                    lpohead.LPO_Type = Values.LPO_FROM_MAINTENANCE;

                                    lpohead.Currency = "TZS";

                                    headerlist.Add(lpohead);

                                    i++;

                                }
                                else
                                {
                                    lpohead = headerlist.Where(x => x.Vendor_Id == item.Vendor).FirstOrDefault();


                                }

                                var lpodetail = new LPO_Details();
                                if (!string.IsNullOrEmpty(item.Stock_Name))
                                {
                                    lpodetail.Item_Name = item.Stock_Name;
                                }
                                else
                                {
                                    lpodetail.Item_Name = item.Item;
                                }


                                lpodetail.quantity = item.Qty_Requested;
                                lpodetail.Price = item.Price ?? 0;
                                lpodetail.Unit = "pc";
                                lpodetail.Currency = "TZS";
                                lpodetail.LPO_Header_Id = lpohead.Id;

                                lpodetail.Total = Math.Round((lpodetail.Price * lpodetail.quantity), 2);

                                lpodetaillist.Add(lpodetail);



                            }
                            else if (item.Status == Values.STATUS_PETTY_CASH_VOUCHER)
                            {

                                var lpohead = new Payment_Header();
                                if (!headerlist.Select(x => x.Vendor_Id).Contains(item.Vendor))
                                {
                                    //    var lpohead = new LPO_Header();

                                    lpohead.UserName = jbheader.Workshop_Manager;
                                   

                                    lpohead.Id = i;


                                    lpohead.Vendor_Id = item.Vendor;

                                    lpohead.JOBNumber = jbheader.Id.ToString();

                                    lpohead.CreatedAt = DateTime.Now;
                                    lpohead.Payment_Source = "Maintenance";
                                    lpohead.Trip_List_Detail_Id = 0;

                                    lpohead.Currency = "TZS";

                                    headerlistpettycash.Add(lpohead);

                                    i++;

                                }
                                else
                                {
                                    lpohead = headerlistpettycash.Where(x => x.Vendor_Id == item.Vendor).FirstOrDefault();


                                }

                                var lpodetail = new Payment_Details();
                                if (!string.IsNullOrEmpty(item.Stock_Name))
                                {
                                    lpodetail.Item_Name = item.Stock_Name;
                                }
                                else
                                {
                                    lpodetail.Item_Name = item.Item;
                                }
                              
                                lpodetail.quantity = item.Qty_Requested;
                                lpodetail.Price = item.Price ?? 0;
                                lpodetail.Unit = "pc";
                                lpodetail.Currency = "TZS";
                                lpodetail.Payment_Header_Id = lpohead.Id;

                                lpodetail.Total = Math.Round((lpodetail.Price * lpodetail.quantity), 2);

                                lpodetaillistpettycash.Add(lpodetail);








                            }

                        }

                        var finaldetaillistlpo = new List<LPO_Details>();
                        if (headerlist.Count > 0)
                        {
                            foreach (var items in headerlist)
                            {
                                finaldetaillistlpo = lpodetaillist.Where(x => x.LPO_Header_Id == items.Id).ToList();

                                db.LPO_Header.Add(items);
                                db.SaveChanges();

                                foreach (var singlelpoline in finaldetaillistlpo)
                                {
                                    singlelpoline.LPO_Header_Id = items.Id;
                                }


                                items.LPO_Number = "LP" + items.Id;

                                items.Order_Total = finaldetaillistlpo.Sum(x => x.Total);

                                db.Entry(items).State = System.Data.Entity.EntityState.Modified;



                                db.LPO_Details.AddRange(finaldetaillistlpo);

                                db.SaveChanges();

                            }
                        }

                        var finaldetaillistissue = new List<Issue_Note_Details>();
                        if (headerlistIssue.Count > 0)
                        {
                            foreach (var items in headerlistIssue)
                            {
                                finaldetaillistissue = detaillistIssue.Where(x => x.Issue_Note_Header_Id == items.Id).ToList();

                                db.Issue_Note_Header.Add(items);
                                db.SaveChanges();

                                foreach (var singlelpoline in finaldetaillistissue)
                                {
                                    singlelpoline.Issue_Note_Header_Id = items.Id;
                                }


                                items.Issue_Note_Number = "IS" + items.Id;

                                items.Order_Total = finaldetaillistissue.Sum(x => x.Total);

                                db.Entry(items).State = System.Data.Entity.EntityState.Modified;



                                db.Issue_Note_Details.AddRange(finaldetaillistissue);

                                db.SaveChanges();

                            }
                        }

                        var finaldetaillistpetty = new List<Payment_Details>();
                        if (headerlistpettycash.Count > 0)
                        {

                            foreach (var items in headerlistpettycash)
                            {
                                finaldetaillistpetty = lpodetaillistpettycash.Where(x => x.Payment_Header_Id == items.Id).ToList();

                                db.Payment_Header.Add(items);
                                db.SaveChanges();

                                foreach (var singlelpoline in finaldetaillistpetty)
                                {
                                    singlelpoline.Payment_Header_Id = items.Id;
                                }


                                items.Payment_Number = "PC" + items.Id;

                                items.Order_Total = finaldetaillistpetty.Sum(x => x.Total);

                                db.Entry(items).State = System.Data.Entity.EntityState.Modified;



                                db.Payment_Details.AddRange(finaldetaillistpetty);

                                db.SaveChanges();

                            }






                        }



                        foreach (var item in jobdetails)
                        {
                            if(item.Status == Values.STATUS_ISSUE_NOTE)
                            {
                                if (headerlistIssue.Count > 0)
                                {
                                    foreach (var items in headerlistIssue)
                                    {
                                        var finaldetaillistagain = finaldetaillistissue.Where(x => x.Issue_Note_Header_Id == items.Id ).ToList();

                                        if (item.Vendor == items.Vendor_Id)
                                        {
                                            item.Order_No = "IS" + finaldetaillistagain.Where(x => x.Item_Name.Trim() == item.Stock_Name.Trim() && x.quantity == item.Qty_Requested).Select(x => x.Issue_Note_Header_Id).FirstOrDefault();

                                            db.Entry(item).State = EntityState.Modified;


                                        }
                                        

                                    }

                                }
                            }else if(item.Status == Values.STATUS_LPO)
                            {

                                if (headerlist.Count > 0)
                                {
                                    foreach (var items in headerlist)
                                    {
                                        var finaldetaillistlponew = finaldetaillistlpo.Where(x => x.LPO_Header_Id == items.Id).ToList();

                                        if (item.Vendor == items.Vendor_Id)
                                        {
                                            item.Order_No = "LP" + finaldetaillistlponew.Where(x => x.Item_Name.Trim() == item.Stock_Name.Trim() && x.quantity == item.Qty_Requested).Select(x => x.LPO_Header_Id).FirstOrDefault();

                                            db.Entry(item).State = EntityState.Modified;


                                        }


                                    }

                                }


                            }
                            else if (item.Status == Values.STATUS_PETTY_CASH_VOUCHER)
                            {

                                if (headerlistpettycash.Count > 0)
                                {
                                    foreach (var items in headerlistpettycash)
                                    {
                                        var finaldetaillistpettynew = finaldetaillistpetty.Where(x => x.Payment_Header_Id == items.Id).ToList();

                                        if (item.Vendor == items.Vendor_Id)
                                        {
                                            item.Order_No = "PC" + finaldetaillistpettynew.Where(x => x.Item_Name.Trim() == item.Stock_Name.Trim() && x.quantity == item.Qty_Requested).Select(x => x.Payment_Header_Id).FirstOrDefault();

                                            db.Entry(item).State = EntityState.Modified;


                                        }


                                    }

                                }



                            }


                        }

                        db.SaveChanges();













                        }



















                    return RedirectToAction("Maintenance");



                }
                else
                {
                    return RedirectToAction("Maintenance");
                }
            }


            List<SelectListViewModel> RemarksList = new List<SelectListViewModel>();

            //      ViewBag.remark = RemarksList.Add(new SelectListViewModel() { Text = "Northern Cape", Value = "NC" });
            var item1 = new SelectListViewModel();
            item1.Text = "Fixed";
            item1.Value = "Fixed";
            RemarksList.Add(item1);

            var item2 = new SelectListViewModel();
            item2.Text = "Not Fixed";
            item2.Value = "Not Fixed";

            RemarksList.Add(item2);


            ViewBag.Remarks = new SelectList(RemarksList, "Text", "Value");

            ViewBag.Vendors = db.Vendor.ToList();




            return View("_MaintenanceEditPartialView", repairViewModel);



        }

        [HttpPost]
        public ActionResult SendBackForEdit(RepairViewModel repairViewModel)
        {

            var entry = db.Maintenance_Book_Header.Find(repairViewModel.Header_Id);

            if (entry != null)
            {

                var role = System.Web.HttpContext.Current.User;

                var userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();

                var roles = userManager.GetRoles(System.Web.HttpContext.Current.User.Identity.GetUserId());



                entry.Message = repairViewModel.Message + " By: " + System.Web.HttpContext.Current.User.Identity.Name + "| Role: " + roles[0];
                entry.Status = Values.STATUS_SENT_BACK_FOR_EDIT;

                db.Entry(entry).State = System.Data.Entity.EntityState.Modified;

                db.SaveChanges();



            }




            return RedirectToAction("Maintenance");

        }



        [HttpGet]
        public ActionResult CreateLPO(int? rowid)
        {

            if (rowid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var repairentry = db.Maintenance_Book_Details.Find(rowid);
            if (repairentry == null)
            {
                return HttpNotFound();
            }

            return PartialView("_CreateLPOPartialView");
        }


        public ActionResult JobCardPending(int? page, string searchString, string currentFilter)
        {


            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            int pageSize = 10;
            int pageNumber = (page ?? 1);


            var jobcardpending = db.Job_Card_Header.OrderByDescending(x => x.Created_At).ToList();


            var lpolist = new List<LPO_Header>();

            var issuenotelist = new List<Issue_Note_Header>();

            var cashpaymentlist = new List<Payment_Header>();


            foreach( var item in jobcardpending)
            {

                var lpos = db.LPO_Header.Where(x => x.JOBNumber == item.Id.ToString() && x.Status != Values.STATUS_RECEIVED).ToList();

         

                var issuenotes = db.Issue_Note_Header.Where(x => x.Reference_No == item.Id && x.Status != Values.STATUS_ISSUED).ToList();

             
                var cashpayments = db.Payment_Header.Where(x => x.JOBNumber == item.Id.ToString() && x.Status != Values.STATUS_PAID).ToList();

           

                if(lpos.Count <= 0 && issuenotes.Count <= 0 && cashpayments.Count <= 0)
                {

                    item.Status = Values.STATUS_COMPLETED_JOBCARD;

                    db.Entry(item).State = EntityState.Modified;



                }




            }

            db.SaveChanges();


            if (!string.IsNullOrEmpty(searchString))
            {
                jobcardpending = jobcardpending.Where(x => x.Fleet_No.Contains(searchString) || x.Job_Card_Reference_No.Contains(searchString)).OrderByDescending(x => x.Id).ToList();

            }
            else
            {
                jobcardpending = jobcardpending.OrderByDescending(x => x.Id).ToList();
            }

            if (Request.IsAjaxRequest())
            {

                return PartialView("_JobCardIndexPartialView", jobcardpending.ToPagedList(pageNumber, pageSize));
            }
            return PartialView("_JobCardIndexPartialView", jobcardpending.ToPagedList(pageNumber, pageSize));


        }

        public ActionResult EditJobCard(int? idparam)
        {

            if (idparam == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var repairentry = db.Job_Card_Header.Find(idparam);
            if (repairentry == null)
            {
                return HttpNotFound();
            }

            var repairentrydetails = db.Job_Card_Details.Where(x => x.Job_Card_Header_Id == idparam).ToList();
            var repairLineItemList = new List<Repair_Line_Item>();

            foreach (var item in repairentrydetails)
            {
                var itemline = new Repair_Line_Item();

                itemline.Details_Id = item.Id;
                itemline.Item = item.Item;
                itemline.Price = item.Price;
                itemline.Stock_Id = item.Stock_Id ?? 0;
                itemline.Qty_Requested = item.Qty_Requested;
                itemline.Stock_Name = item.Stock_Name;
                itemline.Stock_Qty = item.Qty_Available;
                itemline.Remark = item.Remarks;
                itemline.Status = item.Status;
                itemline.Vendor = item.Vendor;

                if (item.Transport_Officer == 1)
                {
                    itemline.Transport_Officer = true;
                }
                else
                {
                    itemline.Transport_Officer = false;
                }


                repairLineItemList.Add(itemline);


            }



            var repairvm = new RepairViewModel();
            repairvm.Header_Id = repairentry.Id;
            repairvm.Created_Date = repairentry.Created_At.ToString();
            repairvm.Driver_Name = repairentry.Driver_Name;
            repairvm.Fleet_No = repairentry.Fleet_No;
            repairvm.Licence_No = repairentry.Licence_No;
            repairvm.Truck_Head_No = repairentry.Truck_head_No;
            repairvm.Truck_Trailer_No = repairentry.Truck_Trailer_No;
            repairvm.Line_Item = repairLineItemList;
            repairvm.Status = repairentry.Status;

            List<SelectListViewModel> RemarksList = new List<SelectListViewModel>();

            //      ViewBag.remark = RemarksList.Add(new SelectListViewModel() { Text = "Northern Cape", Value = "NC" });
            var item1 = new SelectListViewModel();
            item1.Text = "Not Fixed";
            item1.Value = "Not Fixed";
            RemarksList.Add(item1);

            var item2 = new SelectListViewModel();
            item2.Text = "Fixed";
            item2.Value = "Fixed";

            RemarksList.Add(item2);


            ViewBag.Remarks = RemarksList;

            ViewBag.Vendors = db.Vendor.ToList();





            return PartialView("_JobCardEdit", repairvm);



        }

        [HttpPost]
        public ActionResult EditJobCard(RepairViewModel repairViewModel)
        {

            if (ModelState.IsValid)
            {

                var header = db.Job_Card_Header.Find(repairViewModel.Header_Id);

                if (System.Web.HttpContext.Current.User.IsInRole(Values.WORKSHOP_MANAGER_ROLE))
                {
                    header.Status = Values.STATUS_JOB_CARD_HAS_PENDING_LPOS;
                }

                var headerlist = new List<LPO_Header>();

                var lpodetaillist = new List<LPO_Details>();

                var headerlistIssue = new List<Issue_Note_Header>();

                var detaillistIssue = new List<Issue_Note_Details>();

                int i = 1;

                var issueid = db.Vendor.Where(x => x.Supp_Name == "Issue Note").Select(x => x.Supp_Id).FirstOrDefault();

                foreach (var item in repairViewModel.Line_Item)
                {


                    if (item.Status == Values.STATUS_ISSUE_NOTE && item.Vendor == issueid)
                    {

                        var lpohead = new Issue_Note_Header();
                        if (!headerlistIssue.Select(x => x.Vendor_Id).Contains(item.Vendor))
                        {
                            //    var lpohead = new LPO_Header();

                            lpohead.UserName = System.Web.HttpContext.Current.User.Identity.Name;

                            lpohead.Id = i;


                            lpohead.Vendor_Id = item.Vendor;

                            lpohead.Reference_No = header.Id;

                            lpohead.CreatedAt = DateTime.Now;

                            lpohead.Currency = "TZS";

                            headerlistIssue.Add(lpohead);

                            i++;

                        }
                        else
                        {
                            lpohead = headerlistIssue.Where(x => x.Vendor_Id == item.Vendor).FirstOrDefault();


                        }

                        var issuedetail = new Issue_Note_Details();
                        if (!string.IsNullOrEmpty(item.Stock_Name))
                        {
                            issuedetail.Item_Name = item.Stock_Name;
                        }
                        else
                        {
                            issuedetail.Item_Name = item.Item;
                        }
                        issuedetail.Stock_Quantity = item.Stock_Qty;
                        issuedetail.quantity = item.Qty_Requested;
                        issuedetail.Stock_Id = item.Stock_Id;
                        issuedetail.Price = item.Price ?? 0;
                        issuedetail.Unit = "pc";
                        issuedetail.Currency = "TZS";
                        issuedetail.Issue_Note_Header_Id = lpohead.Id;

                        issuedetail.Total = Math.Round((issuedetail.Price * issuedetail.quantity), 2);

                        detaillistIssue.Add(issuedetail);



                        //issue note

                    }
                    else if (item.Status == Values.STATUS_LPO)
                    {
                        var lpohead = new LPO_Header();
                        if (!headerlist.Select(x => x.Vendor_Id).Contains(item.Vendor))
                        {
                            //    var lpohead = new LPO_Header();

                            lpohead.UserName = System.Web.HttpContext.Current.User.Identity.Name;

                            lpohead.Id = i;


                            lpohead.Vendor_Id = item.Vendor;

                            lpohead.JOBNumber = header.Id.ToString();

                            lpohead.CreatedAt = DateTime.Now;

                            lpohead.Currency = "TZS";

                            headerlist.Add(lpohead);

                            i++;

                        }
                        else
                        {
                            lpohead = headerlist.Where(x => x.Vendor_Id == item.Vendor).FirstOrDefault();


                        }

                        var lpodetail = new LPO_Details();
                        if (!string.IsNullOrEmpty(item.Stock_Name))
                        {
                            lpodetail.Item_Name = item.Stock_Name;
                        }
                        else
                        {
                            lpodetail.Item_Name = item.Item;
                        }
                        lpodetail.quantity = item.Qty_Requested;
                        lpodetail.Price = item.Price ?? 0;
                        lpodetail.Unit = "pc";
                        lpodetail.Currency = "TZS";
                        lpodetail.LPO_Header_Id = lpohead.Id;

                        lpodetail.Total = Math.Round((lpodetail.Price * lpodetail.quantity), 2);

                        lpodetaillist.Add(lpodetail);



                    }

                }

                if (headerlist.Count > 0)
                {
                    foreach (var items in headerlist)
                    {
                        var finaldetaillist = lpodetaillist.Where(x => x.LPO_Header_Id == items.Id).ToList();

                        db.LPO_Header.Add(items);
                        db.SaveChanges();

                        foreach (var singlelpoline in finaldetaillist)
                        {
                            singlelpoline.LPO_Header_Id = items.Id;
                        }


                        items.LPO_Number = "LP" + items.Id;

                        items.Order_Total = finaldetaillist.Sum(x => x.Total);

                        db.Entry(items).State = System.Data.Entity.EntityState.Modified;



                        db.LPO_Details.AddRange(finaldetaillist);

                        db.SaveChanges();

                    }
                }

                if (headerlistIssue.Count > 0)
                {
                    foreach (var items in headerlistIssue)
                    {
                        var finaldetaillist = detaillistIssue.Where(x => x.Issue_Note_Header_Id == items.Id).ToList();

                        db.Issue_Note_Header.Add(items);
                        db.SaveChanges();

                        foreach (var singlelpoline in finaldetaillist)
                        {
                            singlelpoline.Issue_Note_Header_Id = items.Id;
                        }


                        items.Issue_Note_Number = "IS" + items.Id;

                        items.Order_Total = finaldetaillist.Sum(x => x.Total);

                        db.Entry(items).State = System.Data.Entity.EntityState.Modified;



                        db.Issue_Note_Details.AddRange(finaldetaillist);

                        db.SaveChanges();

                    }
                }



                return RedirectToAction("Maintenance");
            }

            return RedirectToAction("Maintenance");

        }


        public ActionResult LPOlist(int? page, string searchString, string currentFilter)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            var lpo = new List<LPO_Header>();


            if (!string.IsNullOrEmpty(searchString))
            {
                lpo = db.LPO_Header.Where(x => x.LPO_Number.Contains(searchString) || x.JOBNumber.Contains(searchString)).OrderByDescending(x => x.Id).ToList();

            }
            else
            {
                lpo = db.LPO_Header.OrderByDescending(x => x.CreatedAt).ToList();
            }
            if (Request.IsAjaxRequest())
            {

                return PartialView("_LPOIndexPartialView", lpo.ToPagedList(pageNumber, pageSize));
            }
            return PartialView("_LPOIndexPartialView", lpo.ToPagedList(pageNumber, pageSize));





        }

        public ActionResult editLPO(int? idparam)
        {



            if (idparam == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            var lpoentry = db.LPO_Header.Find(idparam);
            if (lpoentry == null)
            {
                return HttpNotFound();
            }

            var lpoentrydetails = db.LPO_Details.Where(x => x.LPO_Header_Id == idparam).ToList();
            var lpoLineItemList = new List<LPO_Line_Item>();

            foreach (var item in lpoentrydetails)
            {
                var itemline = new LPO_Line_Item();


                itemline.Item_Name = item.Item_Name;
                itemline.quantity = item.quantity;
                itemline.Price = item.Price;
                itemline.Total = item.Total;
                itemline.Unit = item.Unit;
                itemline.Currency = item.Currency;

                lpoLineItemList.Add(itemline);


            }



            var repairvm = new LPOViewModel();
            repairvm.Id = lpoentry.Id;
            repairvm.CreatedAt = lpoentry.CreatedAt;
            repairvm.JOBNumber = lpoentry.JOBNumber;
            repairvm.LPO_Number = lpoentry.LPO_Number;
            repairvm.Order_Total = lpoentry.Order_Total;
            repairvm.Vendor_Name = lpoentry.Vendor_Name;
            repairvm.UserName = lpoentry.UserName;
            repairvm.Status = lpoentry.Status;
            repairvm.LPO_Line_Item = lpoLineItemList;


            ViewBag.Vendors = db.Vendor.ToList();





            return PartialView("_LPOEdit", repairvm);





        }

        public ActionResult IssueNotelist(int? page, string searchString, string currentFilter)
        {

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            int pageSize = 10;
            int pageNumber = (page ?? 1);




            var lpo = new List<Issue_Note_Header>(); 

            if (!string.IsNullOrEmpty(searchString))
            {
                lpo = db.Issue_Note_Header.Where(x => (x.Issue_Note_Number.Contains(searchString) || x.Reference_No.ToString().Contains(searchString))&& x.Type == Values.ISSUE_NOTE_MAINTENANCE).OrderByDescending(x => x.CreatedAt).ToList();

            }
            else
            {
                lpo = db.Issue_Note_Header.Where(x => x.Type == Values.ISSUE_NOTE_MAINTENANCE).OrderByDescending(x => x.CreatedAt).ToList();
            }

            if (Request.IsAjaxRequest())
            {

                return PartialView("_IssueNoteIndexPartialView", lpo.ToPagedList(pageNumber, pageSize));
            }
            return PartialView("_IssueNoteIndexPartialView", lpo.ToPagedList(pageNumber, pageSize));



        }


        public ActionResult editIssueNote(int? idparam)
        {



            if (idparam == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            var issueentry = db.Issue_Note_Header.Find(idparam);
            if (issueentry == null)
            {
                return HttpNotFound();
            }

            var issueentrydetails = db.Issue_Note_Details.Where(x => x.Issue_Note_Header_Id == idparam).ToList();
            var lpoLineItemList = new List<IssueNote_Line_Item>();

            foreach (var item in issueentrydetails)
            {
                var itemline = new IssueNote_Line_Item();


                itemline.Item_Name = item.Item_Name;
                itemline.quantity = item.quantity;

                itemline.Stock_Qty = item.Stock_Quantity;
                itemline.Stock_Id = item.Stock_Id;
                itemline.Price = item.Price;
                itemline.Total = item.Total;
                itemline.Unit = item.Unit;
                itemline.Currency = item.Currency;

                lpoLineItemList.Add(itemline);


            }



            var repairvm = new IssueNoteViewModel();
            repairvm.Id = issueentry.Id;
            repairvm.CreatedAt = issueentry.CreatedAt;
            repairvm.JOBNumber = issueentry.Reference_No;
            repairvm.Issue_Note_Number = issueentry.Issue_Note_Number;
            repairvm.Order_Total = issueentry.Order_Total;
            repairvm.Vendor_Name = issueentry.Vendor_Name;
            repairvm.UserName = issueentry.UserName;
            repairvm.IssueNote_Line_Item = lpoLineItemList;
            repairvm.Status = issueentry.Status;


            ViewBag.Vendors = db.Vendor.ToList();





            return PartialView("_IssueNoteEdit", repairvm);





        }

        public ActionResult Paymentslist(int? page, string searchString, string currentFilter)
        {
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            int pageSize = 10;
            int pageNumber = (page ?? 1);


            var lpo = new List<Payment_Header>();
       


            if (!string.IsNullOrEmpty(searchString))
            {
                lpo = db.Payment_Header.Where(x => x.Payment_Source.Trim() == "Maintenance" && (x.Payment_Number.Contains(searchString) || x.JOBNumber.ToString().Contains(searchString))).OrderByDescending(x => x.CreatedAt).ToList();

            }
            else
            {
                lpo = db.Payment_Header.Where(x => x.Payment_Source.Trim() == "Maintenance").OrderByDescending(x => x.CreatedAt).ToList();
            }

            if (Request.IsAjaxRequest())
            {

                return PartialView("_PaymentsIndexPartialView", lpo.ToPagedList(pageNumber, pageSize));
            }
            return PartialView("_PaymentsIndexPartialView", lpo.ToPagedList(pageNumber, pageSize));



        }

        public ActionResult editPayment(int? idparam)
        {



            if (idparam == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            var lpoentry = db.Payment_Header.Find(idparam);
            if (lpoentry == null)
            {
                return HttpNotFound();
            }

            var lpoentrydetails = db.Payment_Details.Where(x => x.Payment_Header_Id == idparam).ToList();
            var lpoLineItemList = new List<Payment_Line_Item>();

            foreach (var item in lpoentrydetails)
            {
                var itemline = new Payment_Line_Item();


                itemline.Item_Name = item.Item_Name;
                itemline.quantity = item.quantity;
                itemline.Price = item.Price;
                itemline.Total = item.Total;
                itemline.Unit = item.Unit;
                itemline.Currency = item.Currency;

                lpoLineItemList.Add(itemline);


            }



            var repairvm = new PaymentViewModel();
            repairvm.Id = lpoentry.Id;
            repairvm.CreatedAt = lpoentry.CreatedAt;
            repairvm.JOBNumber = lpoentry.JOBNumber;
            repairvm.Payment_Number = lpoentry.Payment_Number;
            repairvm.Order_Total = lpoentry.Order_Total;
            repairvm.Vendor_Name = lpoentry.Vendor_Name;
            repairvm.UserName = lpoentry.UserName;
            repairvm.Payment_Line_Item = lpoLineItemList;


            ViewBag.Vendors = db.Vendor.ToList();





            return PartialView("_PaymentEdit", repairvm);





        }

        [HttpPost]
        public ActionResult EditIssueNote(IssueNoteViewModel model)
        {

            if (ModelState.IsValid)
            {

                using (var dbContextTransaction = db.Database.BeginTransaction())
                {

                    var jobhead = db.Job_Card_Header.Find(model.JOBNumber);

                    var stocklist = new List<Stock>();

                    var locationid = db.Location.Where(x => x.Name == Values.MAIN_STORE).Select(x => x.Id).FirstOrDefault();

                    var currencyobj = db.Currencies.Where(x => x.Name == Values.TANZANIA_SHILLINGS).FirstOrDefault();

                    foreach (var item in model.IssueNote_Line_Item)
                    {

                        // deduct stock     

                        var stockobjcheck = db.Stock.Find(item.Stock_Id);

                        

                        
                        var stock = new Stock();

                        stock.Item_Id = stockobjcheck.Item_Id;
                        stock.Item_Name = item.Item_Name;
                        stock.Location_Id = locationid;
                        stock.Sell_Price = 0;
                        stock.Currency_Id = currencyobj.Id;
                        stock.Currency_Name = currencyobj.Name;
                        stock.Type = "Issue Note";


                        var ItemUom = db.Price_List.Where(pl => pl.Item_Id == stock.Item_Id).First();

                        var UoMint = ItemUom.UoM_Id;

                        //     var UomName = db.IC_UoM.Where(pl => pl.UoM_Id == UoMint).First();

                        stock.UoM = item.Unit;// uomobj.Where(x => x.UoM_Id == UoMint).Select(x => x.UoM).FirstOrDefault();

                        List<Stock> _checkItem = db.Stock.Where(x => x.Item_Id == stock.Item_Id && x.Location_Id == stock.Location_Id).ToList();


                        //(from ci in db.Stock
                        //                      where ci.Item_Id == stock.Item_Id && ci.Location_Id == stock.Location_Id
                        //                      select ci).ToList();
                        decimal qtybal = 0;


                        if (_checkItem.Count > 0)

                        {
                            var supplycheck = _checkItem.Where(x => x.Item_Id == stock.Item_Id).LastOrDefault();

                            var priceitem = db.Price_List.Where(x => x.Item_Id == stock.Item_Id).ToList().LastOrDefault();
                            stock.Buy_Price = priceitem.Buying_Price;
                            qtybal = supplycheck.Qty_Bal;
                            stock.Qty_Out = item.quantity;
                            stock.Cost_Out = Math.Round((stock.Qty_Out * stock.Buy_Price), 2);
                            stock.Sent_Location = stock.Location_Id;
                            decimal balcost = 0;

                            if (priceitem.curr.Name == "TZS")
                            {
                                balcost = supplycheck.Bal_Cost_TZS;
                                stock.Bal_Cost_TZS = supplycheck.Bal_Cost_TZS - stock.Cost_Out;
                            }
                            else if (priceitem.curr.Name == "USD")
                            {
                                balcost = supplycheck.Bal_Cost_USD;
                                stock.Bal_Cost_USD = supplycheck.Bal_Cost_USD - stock.Cost_Out;
                            }




                            stock.Qty_Bal = qtybal - stock.Qty_Out;

                            if (stock.Qty_Bal < 0)
                            {

                                dbContextTransaction.Rollback();

                                return Json(new { message = stock.Item_Name.ToString() + " will be less than zero. Current stock is ( " + qtybal.ToString() + " )" });

                            }


                            stock.Created_Date = DateTime.Now;
                            stocklist.Add(stock);


                        }
                        else
                        {

                            stock.Qty_Out = item.quantity;

                            stock.Qty_Bal = stock.Qty_In;
                            stock.Cost_In = Math.Round(stock.Qty_In * stock.Buy_Price, 2);

                            if (stock.Currency_Name == "TZS")
                            {

                                stock.Bal_Cost_TZS = stock.Cost_In - stock.Cost_Out;
                            }
                            else if (stock.Currency_Name == "USD")
                            {

                                stock.Bal_Cost_USD = stock.Cost_In - stock.Cost_Out;
                            }



                            stock.Created_Date = DateTime.Now;
                            stocklist.Add(stock);

                        }


                        var FIFO = db.Stock.Find(item.Stock_Id);

                        var quant = (FIFO.FIFO_Quantity ?? 0) + item.quantity;
                        var diff = FIFO.Qty_In - quant;
                        if (diff > 0)
                        {
                            FIFO.FIFO_Quantity = quant;
                            db.Entry(FIFO).State = EntityState.Modified;
                            db.SaveChanges();

                        }
                        else
                        {

                            FIFO.FIFO_Quantity = FIFO.Qty_In;

                            FIFO.Settled = 1;
                            db.Entry(FIFO).State = EntityState.Modified;
                            db.SaveChanges();

                            var stop = false;

                            while (stop == false)
                            {
                                var fifonew = db.Stock.Where(x => x.Location_Id == locationid && x.Qty_In != 0 && x.Settled == 0 && x.Item_Id == stockobjcheck.Item_Id).FirstOrDefault();

                                if (fifonew != null)
                                {
                                    var quant1 = (fifonew.FIFO_Quantity ?? 0) + +Math.Abs(diff);
                                    var diff1 = fifonew.Qty_In - quant1;
                                    if (diff1 > 0)
                                    {
                                        fifonew.FIFO_Quantity = quant1;
                                        db.Entry(fifonew).State = EntityState.Modified;
                                        db.SaveChanges();

                                        stop = true;

                                    }
                                    else
                                    {

                                        fifonew.FIFO_Quantity = fifonew.Qty_In;

                                        fifonew.Settled = 1;
                                        db.Entry(fifonew).State = EntityState.Modified;
                                        db.SaveChanges();

                                        diff = Math.Abs(diff1);
                                    }
                                }
                                else
                                {
                                    stop = true;

                                }


                            }

                        }








                    }








                    db.Stock.AddRange(stocklist);

                    db.SaveChanges();


                    //remove from qty bal tracker

                    var qtybalobj = db.Qty_Bal_Tracker.Where(x => x.Order_Id == jobhead.Maintenance_header_Id);
                    db.Qty_Bal_Tracker.RemoveRange(qtybalobj);
                    db.SaveChanges();


                    model.Status = Values.STATUS_ISSUED;

                    var issuenote = db.Issue_Note_Header.Find(model.Id);

                    issuenote.Status = Values.STATUS_ISSUED;

                    db.Entry(issuenote).State = EntityState.Modified;

                    db.SaveChanges();


                    //check for other pending items in jobcard

                                                                   


                    var lpolist = new List<LPO_Header>();

                    var issuenotelist = new List<Issue_Note_Header>();

                    var cashpaymentlist = new List<Payment_Header>();

                                       
                        var lpos = db.LPO_Header.Where(x => x.JOBNumber == jobhead.Id.ToString() && x.Status != Values.STATUS_RECEIVED).ToList();
                                       
                        var issuenotes = db.Issue_Note_Header.Where(x => x.Reference_No == jobhead.Id && x.Status != Values.STATUS_ISSUED).ToList();
                    
                        var cashpayments = db.Payment_Header.Where(x => x.JOBNumber == jobhead.Id.ToString() && x.Status != Values.STATUS_PAID).ToList();
                    

                        if (lpos.Count <= 0 && issuenotes.Count <= 0 && cashpayments.Count <= 0)
                        {

                            jobhead.Status = Values.STATUS_COMPLETED_JOBCARD;

                            db.Entry(jobhead).State = EntityState.Modified;

                        }                    

                    db.SaveChanges();


                    dbContextTransaction.Commit();

                    //     return Json(new { message = "success", order_id = orderid });



                    return RedirectToAction("Maintenance");




                    //   ViewBag.Vendors = db.Vendor.ToList();


                    //    return View("_IssueNoteEdit", model);

                    //  return RedirectToAction("");

                }

            }

            return RedirectToAction("Maintenance");

        }


        [HttpGet]
        public ActionResult Create_GRN(int id)
        {


            //Retrieve LPO Object
            var LPO_Head = db.LPO_Header.Find(id);

            var lpodetails = db.LPO_Details.Where(x => x.LPO_Header_Id == id).ToList();

            // check if item exists in item table

            foreach (var item in lpodetails)
            {

                var itemobject = db.Items.Where(x => x.Item_Name.ToLower().Trim() == item.Item_Name.ToLower().Trim()).FirstOrDefault();


          
                var itemlist = new List<Items>();

                var currency = db.Currencies.Where(x => x.Name == Values.TANZANIA_SHILLINGS).Select(x => x.Id).FirstOrDefault();

                if(itemobject == null)
                {
                    var newitem = new Items();
                    newitem.Item_Name = item.Item_Name;
                    newitem.Taxable = 1;
                    newitem.ItemActive = 1;
                    newitem.UoM_Id = 1;
                    newitem.Stock_Type = Convert.ToString(Values.LOCATION_STOCK);
                    newitem.Category = 0;


                    db.Items.Add(newitem);
                    db.SaveChanges();

                    item.Item_Id = newitem.Item_Id;
                    
                    // add to price list table
                    // check for existance in pricelist table


                    var priceobject = db.Price_List.Where(x => x.Item_Id == item.Item_Id).FirstOrDefault();


                    if (priceobject == null)
                    {



                        var newpricelist = new Price_List();

                        newpricelist.Item_Id = newitem.Item_Id;
                        newpricelist.Item_Name = newitem.Item_Name;
                        newpricelist.Part_Number = newitem.Part_Number;
                        newpricelist.Vendor_Id = LPO_Head.Vendor_Id;
                        newpricelist.UoM_Id = newitem.UoM_Id;
                        newpricelist.Currency = currency;
                        newpricelist.Buying_Price = item.Price;
                        newpricelist.Taxable = 2;
                        newpricelist.Buying_Price_No_Tax = item.Price;
                        newpricelist.Selling_Price = 0;
                        newpricelist.SubCategory = 0;
                        newpricelist.Location_Id = 0;

                        db.Price_List.Add(newpricelist);

                        db.SaveChanges();

                    }else
                    {
                        item.Part_Number = priceobject.Part_Number;
                        priceobject.Buying_Price = item.Price;
                        priceobject.Buying_Price_No_Tax = item.Price;

                        db.Entry(priceobject).State = EntityState.Modified;
                        db.SaveChanges();

                                               
                    }
                }else
                {
                    item.Item_Id = itemobject.Item_Id;



                }




            }


            //Create GRN

            var vm = new GRNViewModel();

            vm.GRN_Header = new GRN_Header();

            vm.GRN_Details = new List<GRN_Details>();

            vm.LPO_Header = new LPO_Header();

            vm.LPO_Details = new List<LPO_Details>();



            vm.GRN_Header.PLO_Number = LPO_Head.LPO_Number;
            vm.GRN_Header.UserName = System.Web.HttpContext.Current.User.Identity.Name;
            vm.LPO_Header.JOBNumber = LPO_Head.JOBNumber;
            
            vm.GRN_Header.Vendor_Id = LPO_Head.Vendor_Id;
            vm.GRN_Header.Currency = LPO_Head.Currency;
            vm.GRN_Header.Order_Total = LPO_Head.Order_Total;
            vm.GRN_Header.Created_At = DateTime.Now;


            foreach (var item in lpodetails)
            {
         

                

                var detail = new GRN_Details();

                detail.Item_Id =item.Item_Id;
                 detail.Item_Name = item.Item_Name;
                detail.Quantity_Received = item.quantity;
                detail.Price = item.Price;
                detail.Part_Number = item.Part_Number;
                detail.quantity = item.quantity;
                detail.Total = item.Total;
                detail.Unit = item.Unit;
                detail.Currency = item.Currency;
                detail.Conversion_Unit = item.Conversion_Unit;
                

                vm.GRN_Details.Add(detail);


            }



            if (vm.GRN_Header != null && vm.GRN_Details.Count > 0)
            {



                vm.GRN_Header.UserName = System.Web.HttpContext.Current.User.Identity.Name;



                vm.GRN_Header.Order_Total = vm.GRN_Details.Sum(x => x.Total);


                db.GRN_Header.Add(vm.GRN_Header);

                db.SaveChanges();

                vm.GRN_Header.GRN_Number = "GRN" + vm.GRN_Header.Id;

                db.Entry(vm.GRN_Header).State = System.Data.Entity.EntityState.Modified;


                foreach (var item in vm.GRN_Details)
                {

                    item.GRN_Header_Id = vm.GRN_Header.Id;



                }

                db.GRN_Details.AddRange(vm.GRN_Details);
                db.SaveChanges();


                AddStock(vm);


                LPO_Head.Status = Values.STATUS_RECEIVED;





                db.Entry(LPO_Head).State = EntityState.Modified;

                db.SaveChanges();


                var lpos = db.LPO_Header.Where(x => x.JOBNumber == LPO_Head.JOBNumber && x.Status != Values.STATUS_RECEIVED).ToList();

                var jobid = Convert.ToInt32(LPO_Head.JOBNumber);

                var issuenotes = db.Issue_Note_Header.Where(x => x.Reference_No == jobid && x.Status != Values.STATUS_ISSUED).ToList();


                var cashpayments = db.Payment_Header.Where(x => x.JOBNumber.Trim() == LPO_Head.JOBNumber.Trim() && x.Status != Values.STATUS_PAID).ToList();

                var jobhead = db.Job_Card_Header.Where(x => x.Id == jobid).FirstOrDefault();

                if (lpos.Count <= 0 && issuenotes.Count <= 0 && cashpayments.Count <= 0)
                {

                    jobhead.Status = Values.STATUS_COMPLETED_JOBCARD;

                    db.Entry(jobhead).State = EntityState.Modified;

                }

                db.SaveChanges();



            }




            return RedirectToAction("Maintenance");





        }



        public void AddStock(GRNViewModel p)
        {

            bool status = false;

            if (p != null)
            {


                //new Supply object using the data from the viewmodel : SendSupplyBarViewModel
                Purchases_Header supply = new Purchases_Header
                {

                    TranCreatedTime = DateTime.Now,
                    Supplier_Id = p.GRN_Header.Vendor_Id,
                    TranTotal = p.GRN_Header.Order_Total ?? 0,
                    User_Name = System.Web.HttpContext.Current.User.Identity.Name,
                    Location_Id = db.Location.Where(x => x.Name == Values.MAIN_STORE).Select(x => x.Id).FirstOrDefault(),
                    Cash_Vendor_Name = p.GRN_Header.Cash_Vendor_Name,
                    Invoice_Number = p.GRN_Header.Invoice_Number,
                    VRN_No = p.GRN_Header.VRN_No,
                    GRN_ID = p.GRN_Header.Id,

                    //IsPaid = p.IsPaid,

                    Last_Updated = DateTime.Now
                };

                db.Purchases_Header.Add(supply);

                db.SaveChanges();


                var SuppDetails = new List<Purchases_Details>();
                var st = new List<Stock>();

                var exchangelist = db.Exchange_Rate.Where(x => x.rate_status == 1).ToList();

                var itempricelist = db.Price_List.ToList();

                int currencyid = db.Currencies.Where(x => x.Name == p.GRN_Header.Currency).Select(x => x.Id).FirstOrDefault();


                var itemobj = db.Items.ToList();

                var uomobj = db.IC_UoM.ToList();

                foreach (var i in p.GRN_Details)
                {
                    Purchases_Details SuppDetailsloop = new Purchases_Details();


                    SuppDetailsloop.Purchases_Header_Id = supply.Id;

                    SuppDetailsloop.Item_Id = i.Item_Id;

                    var itemcheck = itempricelist.Where(x => x.Item_Id == SuppDetailsloop.Item_Id).Select(x => x.Taxable).FirstOrDefault();

                    var priceobj = itempricelist.Where(x => x.Item_Id == SuppDetailsloop.Item_Id).FirstOrDefault();

                    SuppDetailsloop.Item_Name = i.Item_Name;
                    SuppDetailsloop.Currency_Id = currencyid;
                    SuppDetailsloop.Currency_Name = i.Currency;
                 
                    SuppDetailsloop.BuyPrice = i.Price;// itempricelist.Where(x => x.Item_Id == SuppDetailsloop.Item_Id).Select(x => x.Buying_Price_No_Tax).FirstOrDefault();
                    SuppDetailsloop.SellPrice = 0;

                    SuppDetailsloop.SupplyQuantity = i.Quantity_Received ?? 0m;

                    SuppDetailsloop.Location_Id = supply.Location_Id;
                    if (SuppDetailsloop.Currency_Name == "USD")
                    {
                        SuppDetailsloop.ExchangeRate = exchangelist.Where(x => x.from_currency == SuppDetailsloop.Currency_Id).Select(x => x.rate).FirstOrDefault();
                    }
                    else
                    {
                        SuppDetailsloop.ExchangeRate = 1;
                    }

                    if (SuppDetailsloop.SellPrice != null)
                    {

                        SuppDetailsloop.SellPriceUSD = Convert.ToDecimal(SuppDetailsloop.SellPrice);
                    }
                    else
                    {
                        SuppDetailsloop.SellPriceUSD = 0;
                    }





                    SuppDetailsloop.Total_Cost = (SuppDetailsloop.SupplyQuantity * SuppDetailsloop.BuyPrice);

                    if (itemcheck == 1)
                    {
                        SuppDetailsloop.Net_Amount = ((SuppDetailsloop.Total_Cost ?? 0) / (db.Tax.Where(x => x.Name == "TRA").Select(x => x.Tax_Rate).FirstOrDefault() + 100)) * 100;

                        SuppDetailsloop.VAT = Math.Round((SuppDetailsloop.Total_Cost ?? 0) - (SuppDetailsloop.Net_Amount ?? 0), 2);




                    }
                    else
                    {
                        SuppDetailsloop.VAT = 0;

                        SuppDetailsloop.Net_Amount = SuppDetailsloop.Total_Cost;




                    }
                                                                          
                    // + (SuppDetailsloop.VAT ?? 0);


                    SuppDetails.Add(SuppDetailsloop);

                    if (i.Quantity_Received != 0)
                    {

                        Stock stock = new Stock();


                        stock.Item_Id = i.Item_Id;
                        stock.Item_Name = i.Item_Name;
                        stock.Currency_Id = currencyid;
                        stock.Currency_Name = i.Currency;
                        stock.Location_Id = supply.Location_Id;
                        stock.Part_Number = priceobj.Part_Number;

                        stock.Buy_Price = SuppDetailsloop.BuyPrice;

                        stock.Sell_Price = 0;

                        stock.Type = "Issue Note Input";

                        stock.SellPriceUSD = SuppDetailsloop.SellPriceUSD;
                        stock.ExchangeRate = SuppDetailsloop.ExchangeRate;


                        //        var ItemUom = db.Price_List.Where(s => s.Item_Id == stock.Item_Id).FirstOrDefault();

                        //       var UoMint = ItemUom.UoM_Id;

                        //     var UomName = db.IC_UoM.Where(s => s.UoM_Id == UoMint).FirstOrDefault();

                        stock.UoM = i.Unit;// uomobj.Where(x => x.UoM_Id == UoMint).Select(x => x.UoM).FirstOrDefault();

                        List<Stock> _checkItem = (from s in db.Stock
                                                  where s.Item_Id == stock.Item_Id && s.Location_Id == stock.Location_Id
                                                  select s).ToList();
                        decimal qtybal = 0;

                        if (_checkItem.Count > 0)
                        {
                            var supplycheck = _checkItem.Where(x => x.Item_Id == stock.Item_Id).LastOrDefault();


                            qtybal = supplycheck.Qty_Bal;
                            stock.Qty_In = i.Quantity_Received ?? 0;
                            stock.Cost_In = Math.Round((stock.Qty_In * stock.Buy_Price), 2);

                            decimal costin = supplycheck.Cost_In;
                            decimal costout = supplycheck.Cost_Out;
                            decimal costinandout = costin - costout;
                            decimal balcost = 0;
                            if (stock.Currency_Name == "TZS")
                            {
                                balcost = supplycheck.Bal_Cost_TZS;
                                stock.Bal_Cost_TZS = balcost + stock.Cost_In;
                            }
                            else if (stock.Currency_Name == "USD")
                            {
                                balcost = supplycheck.Bal_Cost_USD;
                                stock.Bal_Cost_USD = balcost + stock.Cost_In;
                            }


                            stock.Qty_Bal = qtybal + stock.Qty_In;


                            stock.Created_Date = DateTime.Now;




                            st.Add(stock);
                        }
                        else
                        {

                            stock.Qty_In = i.Quantity_Received ?? 0;

                            stock.Qty_Bal = stock.Qty_In;
                            stock.Cost_In = Math.Round((stock.Qty_In * stock.Buy_Price), 2);

                            if (stock.Currency_Name == "TZS")
                            {

                                stock.Bal_Cost_TZS = stock.Cost_In;
                            }
                            else if (stock.Currency_Name == "USD")
                            {

                                stock.Bal_Cost_USD = stock.Cost_In;
                            }

                            stock.Created_Date = DateTime.Now;
                            st.Add(stock);


                        }

                    }


                }



                db.Purchases_Details.AddRange(SuppDetails);

                db.SaveChanges();
                //InsertSupplyTransactionDetails(SuppDetails);

                db.Stock.AddRange(st);

                db.SaveChanges();

                status = true;




                //deduct added stock automatically





                //update stock
                //var itemObj = db.Items.ToList();
              //  var uomobj = db.IC_UoM.ToList();

                var stocklist = new List<Stock>();



                

                var jobobj = db.Job_Card_Header.Find(Convert.ToInt32(p.LPO_Header.JOBNumber));

                foreach (var item in p.GRN_Details)
                {

                   
                    if (item.Item_Id != 0)
                    {


                        var tempstockid = InternalGetStock(item.Item_Name).Stock_Id; //db.Job_Card_Details.Where(x => x.Item == item.Item_Name).Select(x => x.Stock_Id).FirstOrDefault();
                     

                        var stock = new Stock();

                            stock.Item_Id = item.Item_Id;
                        var priceitem = db.Price_List.Where(x => x.Item_Id == stock.Item_Id).ToList().LastOrDefault();
                        stock.Item_Name = item.Item_Name;
                        stock.Part_Number = priceitem.Part_Number;
                        stock.Location_Id = supply.Location_Id;
                            stock.Sell_Price = 0; 


                            stock.Currency_Id = currencyid;
                            stock.Currency_Name = db.Currencies.Find(stock.Currency_Id).Name;
                            stock.Type = "Issue Note Consumption";
                        stock.Sent_Location = stock.Location_Id;



                            //     var UomName = db.IC_UoM.Where(pl => pl.UoM_Id == UoMint).First();

                            stock.UoM = item.Unit;
                            List<Stock> _checkItem = db.Stock.Where(x => x.Item_Id == stock.Item_Id && x.Location_Id == stock.Location_Id).ToList();


                            //(from ci in db.Stock
                            //                      where ci.Item_Id == stock.Item_Id && ci.Location_Id == stock.Location_Id
                            //                      select ci).ToList();
                            decimal qtybal = 0;


                            if (_checkItem.Count > 0)

                            {
                                var supplycheck = _checkItem.Where(x => x.Item_Id == stock.Item_Id).LastOrDefault();

                                //var priceitem = db.Price_List.Where(x => x.Item_Id == stock.Item_Id).ToList().LastOrDefault();
                                //stock.Buy_Price = priceitem.Buying_Price;
                            stock.Part_Number = priceitem.Part_Number;
                                qtybal = supplycheck.Qty_Bal;
                                stock.Qty_Out = Convert.ToInt32(item.Quantity_Received);
                                stock.Cost_Out = Math.Round((stock.Qty_Out * stock.Buy_Price), 2);
                                stock.Sent_Location = stock.Location_Id;
                                decimal balcost = 0;

                                if (priceitem.curr.Name == "TZS")
                                {
                                    balcost = supplycheck.Bal_Cost_TZS;
                                    stock.Bal_Cost_TZS = supplycheck.Bal_Cost_TZS - stock.Cost_Out;
                                }
                                



                                stock.Qty_Bal = qtybal - stock.Qty_Out;

                                if (stock.Qty_Bal < 0)
                                {

                           //         dbContextTransaction.Rollback();

                             //       return Json(new { message = stock.Item_Name.ToString() + " will be less than zero. Current stock is ( " + qtybal.ToString() + " )" });

                                }


                                stock.Created_Date = DateTime.Now;
                                stocklist.Add(stock);
                            
                            }
                            else
                            {

                                stock.Qty_Out = Convert.ToInt32(item.Quantity_Received);

                                stock.Qty_Bal = stock.Qty_In;
                                stock.Cost_In = (stock.Qty_In * stock.Buy_Price);

                                if (stock.Currency_Name == "TZS")
                                {

                                    stock.Bal_Cost_TZS = stock.Cost_In - stock.Cost_Out;
                                }
                                else if (stock.Currency_Name == "USD")
                                {

                                    stock.Bal_Cost_USD = stock.Cost_In - stock.Cost_Out;
                                }



                                stock.Created_Date = DateTime.Now;
                                stocklist.Add(stock);

                            }


                            var FIFO = db.Stock.Find(tempstockid);

                            var quant = (FIFO.FIFO_Quantity ?? 0) + item.Quantity_Received;
                            var diff = FIFO.Qty_In - quant?? 0;
                            if (diff > 0)
                            {
                                FIFO.FIFO_Quantity = quant;
                                db.Entry(FIFO).State = EntityState.Modified;
                                db.SaveChanges();

                            }
                            else
                            {

                                FIFO.FIFO_Quantity = FIFO.Qty_In;

                                FIFO.Settled = 1;
                                db.Entry(FIFO).State = EntityState.Modified;
                                db.SaveChanges();

                                var stop = false;

                                while (stop == false)
                                {
                                    var fifonew = db.Stock.Where(x => x.Location_Id == stock.Location_Id && x.Qty_In != 0 && x.Settled == 0 && x.Item_Id == item.Item_Id).FirstOrDefault();

                                    if (fifonew != null)
                                    {
                                        var quant1 = (fifonew.FIFO_Quantity ?? 0)  + Math.Abs(diff);
                                        var diff1 = fifonew.Qty_In - quant1;
                                        if (diff1 > 0)
                                        {
                                            fifonew.FIFO_Quantity = quant1;
                                            db.Entry(fifonew).State = EntityState.Modified;
                                            db.SaveChanges();

                                            stop = true;

                                        }
                                        else
                                        {

                                            fifonew.FIFO_Quantity = fifonew.Qty_In;

                                            fifonew.Settled = 1;
                                            db.Entry(fifonew).State = EntityState.Modified;
                                            db.SaveChanges();

                                            diff = Math.Abs(diff1);
                                        }
                                    }
                                    else
                                    {
                                        stop = true;

                                    }


                                }

                            }


                        



                    }






                }

                //InsertStock(stocklist);

                db.Stock.AddRange(stocklist);
                db.SaveChanges();


                var maintenaceobj = db.Maintenance_Book_Header.Find(jobobj.Maintenance_header_Id);

                //remove from qty bal tracker

                var qtybalobj = db.Qty_Bal_Tracker.Where(x => x.Order_Id == maintenaceobj.Id );
                db.Qty_Bal_Tracker.RemoveRange(qtybalobj);
                db.SaveChanges();





            }



            //     return Json(new { status = status });











        }


        public Stock InternalGetStock(string searchstring)
        {

          
                var location = db.Location.Where(x => x.Name == Values.MAIN_STORE).Select(x => x.Id).FirstOrDefault();

                var stockobj = db.Stock.Where(X => X.Item_Name.Contains(searchstring)).Select(x => new StockLedgerViewModel()

                {

                    Stock_Id = x.Stock_Id,
                    Item_Name = x.Item_Name,

                    Location_Id = x.Location_Id,
                    Item_Id = x.Item_Id,
                    Qty_Bal = x.Qty_Bal,
                    Qty_In = x.Qty_In,
                    settled = x.Settled

                });

                var stocklocation = stockobj.Where(l => l.Location_Id == location).ToList();

                var trackerobject = db.Qty_Bal_Tracker.ToList();

                List<int> la = new List<int>();

                foreach (var q in stocklocation.GroupBy(x => x.Item_Id))
                {

                    var t = q.Select(x => new { Qty_Bal = x.Qty_Bal }).Last().Qty_Bal;

                    var k = q.Select(x => x.Item_Id).LastOrDefault();

                    var sum = db.Qty_Bal_Tracker.Where(x => x.Item_Id == k && x.Location_Id == location).ToList().Sum(x => x.Qty_Bal);


                    if ((t - sum) <= 0)
                    {
                        var p = q.Select(x => x.Item_Id).LastOrDefault();

                        la.Add(p);
                    }

                }



                var barItems = stockobj.Where(t => !la.Contains(t.Item_Id) && t.Location_Id == location).ToList();



                var groupedList = new List<Stock>();


                foreach (var item in barItems.GroupBy(x => x.Item_Id))
                {
                    var tempstock = new Stock();

                    var stockcheck = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Id).FirstOrDefault();
                    if (stockcheck != 0)
                    {



                        tempstock.Stock_Id = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Stock_Id).FirstOrDefault();
                        tempstock.Item_Id = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Id).FirstOrDefault();
                        tempstock.Item_Name = item.Where(x => x.Qty_In != 0 && x.settled == 0).Select(x => x.Item_Name).FirstOrDefault();


                        groupedList.Add(tempstock);
                    }
                    else
                    {
                        tempstock.Stock_Id = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Stock_Id).LastOrDefault();
                        tempstock.Item_Id = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Item_Id).LastOrDefault();
                        tempstock.Item_Name = item.Where(x => x.Qty_In != 0 && x.settled == 1).Select(x => x.Item_Name).LastOrDefault();




                        groupedList.Add(tempstock);

                    }
                }


                var json = string.Empty;

                var result = new List<Dictionary<string, string>>();


                if (!string.IsNullOrEmpty(searchstring))
                {

                 

                }
                var barItem = new[] { new { label = "", value = "" } }.AsEnumerable();

                var culture = CultureInfo.CurrentCulture;


                barItem = groupedList.Where(x => culture.CompareInfo.IndexOf(x.Item_Name ?? "", searchstring, CompareOptions.IgnoreCase) >= 0).ToList().Select(x => new { label = x.Item_Name, value = x.Item_Id.ToString() + "," + x.Stock_Id.ToString() });


                // Join bar items without stock

                //

           //     string resultobject = JsonConvert.SerializeObject(result);


            return groupedList.FirstOrDefault();

        }


        public ActionResult JobCardCompleted(int? page, string searchString, string currentFilter)
        {

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            int pageSize = 10;
            int pageNumber = (page ?? 1);

            var jobcardpending = new List<Job_Card_Header>(); 

            if (!string.IsNullOrEmpty(searchString))
            {
                jobcardpending = db.Job_Card_Header.Where(x => (x.Status == Values.STATUS_COMPLETED_JOBCARD) && x.Job_Card_Reference_No.Contains(searchString) || x.Fleet_No.Contains(searchString)).OrderByDescending(x => x.Created_At).ToList();

            }
            else
            {
                jobcardpending = db.Job_Card_Header.Where(x => x.Status == Values.STATUS_COMPLETED_JOBCARD).OrderByDescending(x => x.Created_At).ToList();
            }



            if (Request.IsAjaxRequest())
            {

                return PartialView("_JobCardCompletedIndexPartialView", jobcardpending.ToPagedList(pageNumber, pageSize));
            }
            return PartialView("_JobCardCompletedIndexPartialView", jobcardpending.ToPagedList(pageNumber, pageSize));


        }

        public ActionResult PrintLPO(int? idparam)
        {


            if (idparam == null)
            {

                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }


            var lpoentry = db.LPO_Header.Find(idparam);
            if (lpoentry == null)
            {
                return HttpNotFound();
            }

            var lpoentrydetails = db.LPO_Details.Where(x => x.LPO_Header_Id == idparam).ToList();
            var lpoLineItemList = new List<LPO_Line_Item>();

            foreach (var item in lpoentrydetails)
            {
                var itemline = new LPO_Line_Item();


                itemline.Item_Name = item.Item_Name;
                itemline.quantity = item.quantity;
                itemline.Price = item.Price;
                itemline.Total = item.Total;
                itemline.Unit = item.Unit;
                itemline.Currency = item.Currency;

                lpoLineItemList.Add(itemline);


            }



            var repairvm = new LPOViewModel();
            repairvm.Id = lpoentry.Id;
            repairvm.CreatedAt = lpoentry.CreatedAt;
            repairvm.JOBNumber = lpoentry.JOBNumber;
            repairvm.LPO_Number = lpoentry.LPO_Number;
            repairvm.Order_Total = lpoentry.Order_Total;
            repairvm.Vendor_Name = lpoentry.Vendor_Name;
            repairvm.Currency = lpoentry.Currency;
            repairvm.UserName = lpoentry.UserName;
            repairvm.Status = lpoentry.Status;
            repairvm.LPO_Line_Item = lpoLineItemList;


            ViewBag.Vendors = db.Vendor.ToList();


         //      return View("LPOPdf", repairvm);

            return new Rotativa.ViewAsPdf("LPOPdf", repairvm)
            {
                PageSize = Rotativa.Options.Size.A4,
                PageOrientation = Rotativa.Options.Orientation.Portrait,
                // CustomSwitches = "--disable-smart-shrinking",
                CustomSwitches = "--viewport-size 1920x1080",
                PageMargins = new Rotativa.Options.Margins(0, 2, 0, 2),
                //PageWidth = 210,
                //PageHeight = 297

                //   PageWidth = 58, // it's in millimeters
            };

        }


        

    }



}