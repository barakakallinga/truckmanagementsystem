﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    [Table("Vendor")]
    public class Vendor
    {

        [Key]
        public int Supp_Id { get; set; }
        public string Supplier_Code { get; set; }
        public string Supp_Name { get; set; }
        public int Status { get; set; }
        public string SupCity { get; set; }

        public string SupAddress { get; set; }

        public string SupEmailAddress { get; set; }

        public DateTime SupCreatedDate { get; set; }
        public string SupTINNumber { get; set; }
        public string SupVATNumber { get; set; }
        public string SupPhoneNumber { get; set; }
        public string SupFaxNumber { get; set; }
        public string SupNotes { get; set; }
        public int defaultSupplier { get; set; }
        public int Currency { get; set; }

    }
}