﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    [Table("Stock")]
    public class Stock
    {

        [Key]

        public int Stock_Id { get; set; }

        public int Location_Id { get; set; }

        //public string Location_String
        //{
        //    get
        //    {
        //        var db = new ApplicationDbContext();
        //        var locs = db.Location.Find(Location_Id).Name;
        //        return locs;
        //    }



        //}

        [Required]


        public int Item_Id { get; set; }

        [Display(Name = "Item Name")]
        public string Item_Name { get; set; }


        [ForeignKey("Item_Id")]
        public virtual Items Item { get; set; }

        // [StringLength(20, ErrorMessage = "Too long. Plese check again!")]
        public string Ref_No { get; set; }

 
        public decimal Qty_In { get; set; }

 
        [Display(Name = "Buy Price")]
        public decimal Buy_Price { get; set; }



        [Display(Name = "Selling Price")]
        public decimal? Sell_Price { get; set; }

        public int Currency_Id { get; set; }

        [Display(Name = " Selling Currency")]
        public string Currency_Name { get; set; }

        public DateTime? Expiry_Date { get; set; }

        public bool Item_Expired { get; set; }

        public bool Stop_Notification { get; set; }

        public int? Purchase_ID { get; set; }

        public int? Sales_Id { get; set; }

        public DateTime? Created_Date { get; set; }

        public string Type { get; set; }

        [Display(Name = "Unit")]
        public string UoM { get; set; }


        public decimal Qty_Out { get; set; }

        [Display(Name = "Qty")]
        public decimal Qty_Bal { get; set; }

        public decimal Stock_Value
        {


            get
            {
                var tot = Math.Round(Qty_Bal * Buy_Price);

                return tot;
            }




        }

        public decimal Cost_In { get; set; }

        public decimal Cost_Out { get; set; }

        public decimal Bal_Cost_TZS { get; set; }

        public decimal Bal_Cost_USD { get; set; }





        public string Barcode { get; set; }

        public Decimal BuyPriceUSD { get; set; }

        public Decimal SellPriceUSD { get; set; }

        public decimal ExchangeRate { get; set; }

        public int Settled { get; set; }

        public decimal? FIFO_Quantity { get; set; }

        public int? Sent_Location { get; set; }

        public string Part_Number { get; set; }



    }
}