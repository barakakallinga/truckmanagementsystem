﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.ViewModels
{
    public class TripListViewModel
    {

        public DateTime CreatedAt {get; set; }

        public string UserName { get; set; }

        public int Transit_Type { get; set; }



        public List<Trip_Line_Item> TripLineItems { get; set; }



    }
}