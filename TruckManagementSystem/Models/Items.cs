﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TruckManagementSystem.Helpers;

namespace TruckManagementSystem.Models
{
    public class Items
    {


        [Key]

        public int Item_Id { get; set; }
        public string Item_Name { get; set; }
        public string Description { get; set; }
        [Display(Name = "Created Date")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime? ItemDateCreated { get; set; }
        [Display(Name = "Last Update")]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}")]
        public DateTime? ItemDateUpdate { get; set; }
        public int ItemActive { get; set; }
        public int UoM_Id { get; set; }
        public string Barcode { get; set; }
        public string Part_Number { get; set; }
        public int Category { get; set; }
        public int? SubCategory { get; set; }
        public int Taxable { get; set; }
        [Display(Name = "Stock Type")]
        public string Stock_Type { get; set; }

        public string UOM_String
        {
            get
            {
                using ( var db  =  new ApplicationDbContext())
                {

                    var s = db.IC_UoM.Find(UoM_Id);

                    if(s != null)
                    {
                        return s.UoM;

                    }else
                    {
                        return "";
                    }




                }




            }
        }

        public string Stock_Type_Converted
        {
            get
            {
                if(Stock_Type == Values.LOCATION_STOCK.ToString())
                {
                    return "Stock Item";

                }else if(Stock_Type == Values.LOCATION_NON_STOCK.ToString())
                {
                    return "Non Stock Item";
                }else
                {
                    return "";
                }
            }



        }

        public string Vehicle_Type { get; set; }

    }
}