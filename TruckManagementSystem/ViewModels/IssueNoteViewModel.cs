﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.ViewModels
{
    public class IssueNoteViewModel
    {

        public int Id { get; set; }



        public int Reference_No { get; set; }

        public string UserName { get; set; }

        public DateTime CreatedAt { get; set; }

        public int JOBNumber { get; set; }

        public string Issue_Note_Number { get; set; }

        public string Vendor_Name { get; set; }

        public decimal? Order_Total { get; set; }

        public int Status { get; set; }

        public string Currency { get; set; }

        public int Vendor_Id { get; set; }

        public string Order_No { get; set; }

        public int Trip_List_detail_Id { get; set; }

        public decimal Total_Qty { get; set; }

        public string Product { get; set; }

        public string Unit { get; set; }

        public decimal Price { get; set; }

        public List<IssueNote_Line_Item> IssueNote_Line_Item { get; set; }

    }
}