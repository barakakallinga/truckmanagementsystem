﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.ViewModels
{
    public class LocationViewModel
    {

        public int Id { get; set; }

        public string Name { get; set; }

        public int? Location_Type { get; set; }

        public Location location { get; set; }
        public IEnumerable<SelectListItem> LocationTypes { get; set; }
    }
}