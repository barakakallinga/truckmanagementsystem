﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Helpers;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.Controllers
{
    [Securities.RolesAuthorization(Roles = Values.ADMIN_ROLE)]

    public class VendorsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Vendors
        public ActionResult Index(int? page)
        {
            CreateVendorIfNeeded();

            int pageSize = 20;
            int pageNumber = (page ?? 1);


            return View(db.Vendor.ToList().ToPagedList(pageNumber, pageSize));
        }

        // GET: Vendors/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vendor vendor = db.Vendor.Find(id);
            if (vendor == null)
            {
                return HttpNotFound();
            }
            return View(vendor);
        }

        // GET: Vendors/Create
        public ActionResult Create()
        {

            ViewBag.Currency = new SelectList(db.Currencies, "Id", "Name");
            return View();
        }

        // POST: Vendors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Supp_Id,Supplier_Code,Supp_Name,Status,SupCity,SupAddress,SupEmailAddress,SupTINNumber,SupVATNumber,SupPhoneNumber,SupFaxNumber,SupNotes, defaultSupplier, Currency")] Vendor vendor)
        {
            if (ModelState.IsValid)
            {
                vendor.SupCreatedDate = DateTime.Now;
                db.Vendor.Add(vendor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Currency = new SelectList(db.Currencies, "Id", "Name");

            return View(vendor);
        }

        // GET: Vendors/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vendor vendor = db.Vendor.Find(id);
            if (vendor == null)
            {
                return HttpNotFound();
            }

            ViewBag.Currency = new SelectList(db.Currencies, "Id", "Name", vendor.Currency);
            return View(vendor);
        }

        // POST: Vendors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Supp_Id,Supplier_Code,Supp_Name,Status,SupCity,SupAddress,SupEmailAddress,SupCreatedDate,SupTINNumber,SupVATNumber,SupPhoneNumber,SupFaxNumber,SupNotes, Currency")] Vendor vendor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(vendor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(vendor);
        }

        // GET: Vendors/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Vendor vendor = db.Vendor.Find(id);
            if (vendor == null)
            {
                return HttpNotFound();
            }
            return View(vendor);
        }

        // POST: Vendors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Vendor vendor = db.Vendor.Find(id);
            db.Vendor.Remove(vendor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        #region
        private void CreateVendorIfNeeded()
        {
            
            //Create Issue Note Vendor
            if (db.Vendor.Where(x => x.Supp_Name == "Issue Note").ToList().Count == 0)
            {
                string VendorIssueNote = ConfigurationManager.AppSettings["IssueNoteVendorName"];
                int VendorValueCashTZ = Convert.ToInt32(ConfigurationManager.AppSettings["CashTZVendorValue"]);

                var objNewVendorIssueNote = new Vendor { Supp_Name = VendorIssueNote, defaultSupplier = 0, Status =1, Currency = 1, SupCreatedDate = DateTime.Now };
                db.Vendor.Add(objNewVendorIssueNote);
                db.SaveChanges();

            }     

        }

        #endregion

    }
}