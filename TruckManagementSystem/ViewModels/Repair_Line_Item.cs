﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.ViewModels
{
    public class Repair_Line_Item
    {
        public int Details_Id { get; set; }

    
        public string Item { get; set; }

        public string Remark { get; set; }

        public int Stock_Id { get; set; }

        public string Stock_Name { get; set; }
        
        public decimal Stock_Qty { get; set; }

        public decimal Qty_Requested { get; set; }

        public int Status { get; set; }

        public bool Transport_Officer { get; set; }

        public int Vendor { get; set; }

        public decimal? Price { get; set; }

        public string Currency { get; set; }

        public string Vendor_Name
        {

            get
            {

                using (var db = new ApplicationDbContext())
                {
                    string name = db.Vendor.Where(x => x.Supp_Id == Vendor).Select(x => x.Supp_Name).FirstOrDefault();

                    return name;
                }

            }
        }

        public string Part_Number { get; set; }


    }
}