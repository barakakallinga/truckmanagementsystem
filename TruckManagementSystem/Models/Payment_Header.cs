﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TruckManagementSystem.Helpers;

namespace TruckManagementSystem.Models
{
    public class Payment_Header
    {

        [Key]
        public int Id { get; set; }

        public string UserName { get; set; }

        public DateTime CreatedAt { get; set; }

        public string JOBNumber { get; set; }

        public int Trip_List_Detail_Id { get; set; }

        public string Payment_Number { get; set; }

        public int Vendor_Id { get; set; }

        public decimal? Order_Total { get; set; }

        public int Status { get; set; }

        public string Currency { get; set; }


        public string Status_String
        {

            get
            {
                return Values.STATUS_STRING(Status);

            }

        }


        public string Vendor_Name
        {

            get
            {

                using (var db = new ApplicationDbContext())
                {
                    string name = db.Vendor.Where(x => x.Supp_Id == Vendor_Id).Select(x => x.Supp_Name).FirstOrDefault();

                    return name;
                }

            }
        }

        public string Payment_Source { get; set; }

    }


}