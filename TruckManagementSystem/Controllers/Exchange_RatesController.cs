﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.Controllers
{
    public class Exchange_RatesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Exchange_Rates
        public ActionResult Index()
        {
            var er = new List<Exchange_Rate>();
            if (db.Exchange_Rate != null)
        { 
             er = db.Exchange_Rate.Include(i => i.Curr).ToList();
        }
            return View(er);
        }

        public ActionResult Create()
        {


            ViewBag.currency = SelectCurrencyList();
            ViewBag.currency1 = SelectCurrencyList();



            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Exchange_Rate exchange, FormCollection form)
        {
            if (ModelState.IsValid)
            {
                var from = Convert.ToInt32(form["currency"].ToString());

                var to = Convert.ToInt32(form["currency1"].ToString());


                var old_currency = db.Exchange_Rate.Where(x => x.from_currency == from && x.to_currency == to && x.rate_status == 1).FirstOrDefault();

                if (old_currency != null)
                {

                    old_currency.rate_status = 0;

                    db.Entry(old_currency).State = EntityState.Modified;

                    db.SaveChanges();
                }


                exchange.from_currency = Convert.ToInt32(form["currency"].ToString());
                exchange.to_currency = Convert.ToInt32(form["currency1"].ToString());


                exchange.exchange_date = DateTime.Now;
                exchange.rate_status = 1;


                db.Exchange_Rate.Add(exchange);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.currency1 = SelectCurrencyList();
            ViewBag.currency = SelectCurrencyList();
            return View(exchange);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Exchange_Rate exchange_rate = db.Exchange_Rate.Find(id);
            if (exchange_rate == null)
            {
                return HttpNotFound();
            }

            ViewBag.from_currency = new SelectList(db.Currencies, "Currency_Id", "Currency_Name", exchange_rate.from_currency);
            ViewBag.to_currency = new SelectList(db.Currencies, "Currency_Id", "Currency_Name", exchange_rate.to_currency);


            return View(exchange_rate);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Exchange_Rate_Id,from_currency,to_currency,rate,rate_description,rate_status")] Exchange_Rate exchange_rate)
        {
            if (ModelState.IsValid)
            {
                exchange_rate.exchange_date = DateTime.Now;
                db.Entry(exchange_rate).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            ViewBag.from_currency = new SelectList(db.Currencies, "Currency_Id", "Currency_Name", exchange_rate.from_currency);
            ViewBag.to_currency = new SelectList(db.Currencies, "Currency_Id", "Currency_Name", exchange_rate.to_currency);


            return View(exchange_rate);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Exchange_Rate exchange_Rate = db.Exchange_Rate.Find(id);
            if (exchange_Rate == null)
            {
                return HttpNotFound();
            }
            return View(exchange_Rate);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Exchange_Rate exchange_Rate = db.Exchange_Rate.Find(id);
            db.Exchange_Rate.Remove(exchange_Rate);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }





        #region

        private List<SelectListItem> SelectCurrencyList()
        {

            List<SelectListItem> SelectCurrencyListItems =
             new List<SelectListItem>();
            //queries all the suppliers for its ID and Name property.
            var VendorList = (from v in db.Currencies
                              select new { v.Id, v.Name }).ToList();

            //save list of suppliers to the _supplierList
            SelectCurrencyListItems.Add(
                new SelectListItem
                {
                    Text = "Select",
                    Value = "0"
                });

            foreach (var item in VendorList)
            {
                SelectCurrencyListItems.Add(
                    new SelectListItem
                    {
                        Text = item.Name,
                        Value = item.Id.ToString()
                    });
            }


            return SelectCurrencyListItems;


        }


        #endregion
    }
}