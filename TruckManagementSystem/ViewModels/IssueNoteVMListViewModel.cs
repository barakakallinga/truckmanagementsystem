﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.ViewModels
{
    public class IssueNoteVMListViewModel
    {

        public int Trip_List_detail_Id { get; set; }

        public string Order_No { get; set; }

      


        public List<IssueNoteViewModel> IssueNoteViewModels { get; set; }
    }
}