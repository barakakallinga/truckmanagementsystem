﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.ViewModels
{
    public class Payment_Line_Item
    {

        public int Item_Id { get; set; }
        public string Item_Name { get; set; }

        public decimal quantity { get; set; }

        public int Payment_Header_Id { get; set; }

        public string Unit { get; set; }

        public Decimal Price { get; set; }

        public string Currency { get; set; }

        public Decimal Total { get; set; }

    }
}