﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.ViewModels
{
    public class StoreReconciliationViewModel
    {

        public int Item_Id { get; set; }

        public string Item_Name { get; set; }

        public int Qty { get; set; }

        public decimal Amount { get; set; }

        public DateTime date { get; set; }
    }
}