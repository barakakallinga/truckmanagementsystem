﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Products
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public int Unit { get; set; }


        public string Unit_String
        {
            get
            {

                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    string TransitType = db.IC_UoM.Where(x => x.UoM_Id == Unit).Select(x => x.UoM).FirstOrDefault();

                    return TransitType;


                }

            }

        }
    }
}