﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class GRN_Header
    {

        [Key]
        public int Id { get; set; }

        public string UserName { get; set; }

        public DateTime Created_At { get; set; }

        public string PLO_Number { get; set; }

        public string GRN_Number { get; set; }

        public string Invoice_Number { get; set; }

        public string Cash_Vendor_Name { get; set; }

        public string VRN_No { get; set; }

        public int Vendor_Id { get; set; }

        public int Status { get; set; }

        public decimal? Order_Total { get; set; }

        public string Currency { get; set; }


        public string Vendor_Name
        {

            get
            {

                using (var db = new ApplicationDbContext())
                {
                    string name = db.Vendor.Where(x => x.Supp_Id == Vendor_Id).Select(x => x.Supp_Name).FirstOrDefault();

                    return name;
                }

            }
        }

    }
}