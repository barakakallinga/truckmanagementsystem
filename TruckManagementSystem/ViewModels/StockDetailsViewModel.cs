﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.ViewModels
{
    public class StockDetailsViewModel
    {


        public string Item_Name { get; set; }
        public decimal Qty_Bal { get; set; }

        public decimal Buy_Price { get; set; }

        public int Vendor_Id { get; set; }

        public int Stock_Id { get; set; }

        public string Part_Number { get; set; }

        public string Unit { get; set; }

        public int Item_Id { get; set; }


    }
}