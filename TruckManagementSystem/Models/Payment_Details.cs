﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Payment_Details
    {

        [Key]
        public int Id { get; set; }



        public int Item_Id { get; set; }

        public string Item_Name { get; set; }

        public decimal quantity { get; set; }

        public int Payment_Header_Id { get; set; }

        public string Unit { get; set; }

        public decimal Price { get; set; }

        public string Currency { get; set; }

        public Decimal Total { get; set; }

        public string Conversion_Unit { get; set; }
    }
}