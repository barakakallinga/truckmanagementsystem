﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TruckManagementSystem.Startup))]
namespace TruckManagementSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
