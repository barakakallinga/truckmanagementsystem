﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Exchange_Rate
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Exchange_Rate_Id { get; set; }

        public int from_currency { get; set; }




        public int to_currency { get; set; }

        public DateTime exchange_date { get; set; }

        public decimal rate { get; set; }



        public string rate_description { get; set; }

        public int rate_status { get; set; }

        [ForeignKey("from_currency")]

        public virtual Currency Curr { get; set; }

        [ForeignKey("to_currency")]

        public virtual Currency Curr1 { get; set; }

        public string Status_String
        {

            get
            {
                var stat = "";

                if (rate_status == 1)
                {
                    stat = "Active";

                }
                else
                {
                    stat = "In Active";
                }

                return stat;



            }


        }

    }
}