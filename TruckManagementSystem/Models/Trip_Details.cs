﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Trip_Details
    {
        [Key]
        public int Id { get; set; }
        public int List_Detail_Id { get; set; }

        public string Order_No { get; set; }
        public decimal Compartment_1 { get; set; }

        public decimal Compartment_2 { get; set; }

        public decimal Compartment_3 { get; set; }

        public decimal Compartment_4 { get; set; }

        public decimal Compartment_5 { get; set; }

        public decimal Compartment_6 { get; set; }

        public string Trip_Status { get; set; }

        public int Status { get; set; }

        public DateTime? Created_At { get; set; }

        public string Username { get; set; }

        public string Delivery_No { get; set; }

        public int Unit { get; set; }

        public string Unit_String
        {
            get
            {
                using (var db = new ApplicationDbContext())
                {

                    var s = db.IC_UoM.Find(Unit);

                    if (s != null)
                    {
                        return s.UoM;

                    }
                    else
                    {
                        return "";
                    }

                }




            }
        }




    }
}