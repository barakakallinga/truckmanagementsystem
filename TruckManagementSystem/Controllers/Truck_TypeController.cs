﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Helpers;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.Controllers
{
    [Securities.RolesAuthorization(Roles = Values.ADMIN_ROLE)]
    public class Truck_TypeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Truck_Type
        public ActionResult Index()
        {
            return View(db.Truck_Type.ToList());
        }

        // GET: Truck_Type/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Truck_Type truck_Type = db.Truck_Type.Find(id);
            if (truck_Type == null)
            {
                return HttpNotFound();
            }
            return View(truck_Type);
        }

        // GET: Truck_Type/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Truck_Type/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] Truck_Type truck_Type)
        {
            if (ModelState.IsValid)
            {
                db.Truck_Type.Add(truck_Type);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(truck_Type);
        }

        // GET: Truck_Type/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Truck_Type truck_Type = db.Truck_Type.Find(id);
            if (truck_Type == null)
            {
                return HttpNotFound();
            }
            return View(truck_Type);
        }

        // POST: Truck_Type/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] Truck_Type truck_Type)
        {
            if (ModelState.IsValid)
            {
                db.Entry(truck_Type).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(truck_Type);
        }

        // GET: Truck_Type/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Truck_Type truck_Type = db.Truck_Type.Find(id);
            if (truck_Type == null)
            {
                return HttpNotFound();
            }
            return View(truck_Type);
        }

        // POST: Truck_Type/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Truck_Type truck_Type = db.Truck_Type.Find(id);
            db.Truck_Type.Remove(truck_Type);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
