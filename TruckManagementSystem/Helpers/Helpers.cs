﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Helpers
{
    public static class Helpers
    {

        public static DateTime GetDateTime(string date)

        {

            string inputDate = date;
            string[] inputFormats = new string[] {  "dd/MM/yyyy",  "dd-MMM-yy hh:mm:ss tt", "dd-MMM-yy h:mm:ss tt" };
         //   System.Globalization.CultureInfo provider = new System.Globalization.CultureInfo. ;
            var ci = new CultureInfo("en-GB");
            DateTime resultDate = new DateTime(1900, 1, 1);

            foreach (string format in inputFormats)
            {

                if (DateTime.TryParseExact(inputDate, format, ci, System.Globalization.DateTimeStyles.None, out resultDate))
                {
                   return resultDate;
                   
                }
            }


            return new DateTime();
        }


        public static void TraceService(string content)
        {

            //set up a filestream
            FileStream fs = new FileStream(@"C:\Logs\ReportErrors.txt", FileMode.OpenOrCreate, FileAccess.Write);

            //set up a streamwriter for adding text
            StreamWriter sw = new StreamWriter(fs);

            //find the end of the underlying filestream
            sw.BaseStream.Seek(0, SeekOrigin.End);

            //add the text
            sw.WriteLine(content);
            //add the text to the underlying filestream

            sw.Flush();
            //close the writer
            sw.Close();
        }


    }
}