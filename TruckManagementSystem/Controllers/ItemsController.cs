﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Helpers;
using TruckManagementSystem.Models;
using TruckManagementSystem.ViewModels;

namespace TruckManagementSystem.Controllers
{
    [Securities.RolesAuthorization(Roles = Values.ADMIN_ROLE)]

    public class ItemsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Items
        public ActionResult Index(int? page, string searchString, string currentFilter)
        {


            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;
            int pageSize = 20;
            int pageNumber = (page ?? 1);

            var items = new List<Items>();
            if (searchString != null)
            {
                items = db.Items.Where(x => x.Item_Name.Contains(searchString) || x.Part_Number.Contains(searchString)).ToList();

            }
            else
            {
                items = db.Items.ToList();
            }

            return View(items.OrderByDescending(x => x.Item_Id).ToList().ToPagedList(pageNumber, pageSize));


            
        }

        // GET: Items/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Items items = db.Items.Find(id);
            if (items == null)
            {
                return HttpNotFound();
            }
            return View(items);
        }

        // GET: Items/Create
        public ActionResult Create()
        {

            List<SelectListViewModel> RemarksList = new List<SelectListViewModel>();

            //      ViewBag.remark = RemarksList.Add(new SelectListViewModel() { Text = "Northern Cape", Value = "NC" });
            var item1 = new SelectListViewModel();
            item1.Text = "SCANIA";
            item1.Value = "SCANIA";
            RemarksList.Add(item1);

            var item2 = new SelectListViewModel();
            item2.Text = "IVECO";
            item2.Value = "IVECO";

            RemarksList.Add(item2);


            ViewBag.UoM_Id = new SelectList(db.IC_UoM, "UoM_Id", "UoM");


           
            var l = db.Categories.Where(x => x.Name == Values.DEFAULT_CATEGORY).Select(x => x.Id).FirstOrDefault();


            ViewBag.Category = new SelectList(db.Categories, "Id", "Name", l);



            ViewBag.Vehicle = RemarksList;


            var selectlistitems = new List<SelectListItem>()
    {
        new SelectListItem() {Text="Stock Item", Value= Values.LOCATION_STOCK.ToString()},
        new SelectListItem() { Text="Non Stock Item", Value= Values.LOCATION_NON_STOCK.ToString()}};

            ViewBag.Stock_Type = new SelectList(selectlistitems, "Value", "Text", Values.LOCATION_STOCK.ToString());

            return View();
        }

        // POST: Items/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Item_Id,Item_Name,Description,ItemDateCreated,ItemDateUpdate,ItemActive,UoM_Id,Barcode,Part_Number,Vehicle_Type,Category,SubCategory,Taxable,Stock_Type")] Items items)
        {
            //if (ModelState.IsValid)
            //{

            items.Item_Name = items.Item_Name + " " + items.Vehicle_Type;
                items.ItemActive = 1;
           
                items.Taxable = 1; 
                items.Stock_Type =  Convert.ToString(items.Stock_Type);

                db.Items.Add(items);
                db.SaveChanges();
                return RedirectToAction("Index");
       //     }

      //      return View(items);
        }

        // GET: Items/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Items items = db.Items.Find(id);
            if (items == null)
            {
                return HttpNotFound();
            }




            List<SelectListViewModel> RemarksList = new List<SelectListViewModel>();

            //      ViewBag.remark = RemarksList.Add(new SelectListViewModel() { Text = "Northern Cape", Value = "NC" });
            var item1 = new SelectListViewModel();
            item1.Text = "SCANIA";
            item1.Value = "SCANIA";
            RemarksList.Add(item1);

            var item2 = new SelectListViewModel();
            item2.Text = "IVECO";
            item2.Value = "IVECO";

            RemarksList.Add(item2);

            ViewBag.Vehicle = RemarksList;


            ViewBag.UoM_Id = new SelectList(db.IC_UoM, "UoM_Id", "UoM",items.UoM_Id);


            var l = db.Categories.Where(x => x.Name == Values.DEFAULT_CATEGORY).Select(x => x.Id).FirstOrDefault();


            ViewBag.Category = new SelectList(db.Categories, "Id", "Name", items.Category);



            var selectlistitems = new List<SelectListItem>()
    {
        new SelectListItem() {Text="Stock Item", Value= Values.LOCATION_STOCK.ToString()},
        new SelectListItem() { Text="Non Stock Item", Value= Values.LOCATION_NON_STOCK.ToString()}};

            ViewBag.Stock_Type = new SelectList(selectlistitems, "Value", "Text", Values.LOCATION_STOCK);




            return View(items);
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Item_Id,Item_Name,Description,ItemDateCreated,ItemDateUpdate,ItemActive,UoM_Id,Barcode,Part_Number,Category,SubCategory,Taxable,Stock_Type, Vehicle_Type")] Items items)
        {
            if (ModelState.IsValid)
            {
             
                    items.Item_Name = items.Item_Name + " " + items.Vehicle_Type;


                items.Item_Name = items.Item_Name.Trim();
                
               
                db.Entry(items).State = EntityState.Modified;
                db.SaveChanges();

                var priceobject = db.Price_List.Where(x => x.Item_Id == items.Item_Id).ToList();

                foreach(var item in priceobject)
                {

                    item.Item_Name = items.Item_Name;
                    item.Part_Number = items.Part_Number;

                    db.Entry(items).State = EntityState.Modified;
                }

                var stockobj = db.Stock.Where(x => x.Item_Id == items.Item_Id).ToList();

                foreach(var stck in stockobj)
                {

                    stck.Item_Name = items.Item_Name;
                    stck.Part_Number = items.Part_Number;

                    db.Entry(stck).State = EntityState.Modified;
                }

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(items);
        }

        // GET: Items/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Items items = db.Items.Find(id);
            if (items == null)
            {
                return HttpNotFound();
            }
            return View(items);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Items items = db.Items.Find(id);
            db.Items.Remove(items);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
