﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.Controllers
{
    [Authorize]
    public class Transit_TypeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Transit_Type
        public ActionResult Index()
        {
            return View(db.Transit_Type.ToList());
        }

        // GET: Transit_Type/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transit_Type transit_Type = db.Transit_Type.Find(id);
            if (transit_Type == null)
            {
                return HttpNotFound();
            }
            return View(transit_Type);
        }

        // GET: Transit_Type/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Transit_Type/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] Transit_Type transit_Type)
        {
            if (ModelState.IsValid)
            {
                db.Transit_Type.Add(transit_Type);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(transit_Type);
        }

        // GET: Transit_Type/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transit_Type transit_Type = db.Transit_Type.Find(id);
            if (transit_Type == null)
            {
                return HttpNotFound();
            }
            return View(transit_Type);
        }

        // POST: Transit_Type/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] Transit_Type transit_Type)
        {
            if (ModelState.IsValid)
            {
                db.Entry(transit_Type).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(transit_Type);
        }

        // GET: Transit_Type/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Transit_Type transit_Type = db.Transit_Type.Find(id);
            if (transit_Type == null)
            {
                return HttpNotFound();
            }
            return View(transit_Type);
        }

        // POST: Transit_Type/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Transit_Type transit_Type = db.Transit_Type.Find(id);
            db.Transit_Type.Remove(transit_Type);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
