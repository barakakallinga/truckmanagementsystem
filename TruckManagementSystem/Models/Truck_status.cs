﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Truck_Status
    {
        [Key]
        public int Id { get; set; }

        public string Fleet_No { get; set; }

        public int Status { get; set; }
    }
}