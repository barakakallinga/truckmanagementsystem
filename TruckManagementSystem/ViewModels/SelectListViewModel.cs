﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.ViewModels
{
    public class SelectListViewModel
    {

        public string Text { get; set; }
        public string Value { get; set; }
    }
}