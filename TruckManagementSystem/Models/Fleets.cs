﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
    public class Fleets
    {
        [Key]
        public int Id { get; set; }

        public string Fleet_No { get; set; }

        public int Truck_Head_Id { get; set; }

        public int? Truck_Trailer_Id { get; set; }

        public int Transit_Type { get; set; }


        public string Truck_Header_Number
        {

            get
            {

                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    string Truckno = db.Trucks.Where(x => x.Id == Truck_Head_Id).Select(x => x.Reg_No).FirstOrDefault();

                    return Truckno;


                }

            }

        }


        public string Truck_Trailer_Number
        {

            get
            {

                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    string Truckno = db.Trucks.Where(x => x.Id == Truck_Trailer_Id).Select(x => x.Reg_No).FirstOrDefault();

                    return Truckno;


                }

            }

        }

        public string Transit_Type_String
        {

            get
            {

                using (ApplicationDbContext db = new ApplicationDbContext())
                {

                    string TransitType = db.Transit_Type.Where(x => x.Id == Transit_Type).Select(x => x.Name).FirstOrDefault();

                    return TransitType;


                }


            }


        }

    }
}