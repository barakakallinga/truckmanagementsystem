﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TruckManagementSystem.Models
{
  
        public class ExpandedUserDTO
        {
            [Key]
            public string Id { get; set; }
            [Display(Name = "User Name")]
            public string UserName { get; set; }
            public string Email { get; set; }
            public string Password { get; set; }
            [Display(Name = "Lockout End Date Utc")]
            public DateTime? LockoutEndDateUtc { get; set; }
            public int AccessFailedCount { get; set; }
            public string PhoneNumber { get; set; }
            public string role { get; set; }
            //    public int Location { get; set; }

            public string Location_String { get; set; }
            public IEnumerable<UserRolesDTO> Roles { get; set; }
            public IEnumerable<UserAndRolesDTONew> Rolas { get; set; }
            //  public IEnumerable<Location> Locations { get; set; }

        }


        public class UserRolesDTO
        {

            [Key]
            [Display(Name = "Role Name")]
            public string RoleName { get; set; }

        }

        public class UserRoleDTO
        {
            [Key]
            [Display(Name = "User Name")]
            public string UserName { get; set; }
            [Display(Name = "Role Name")]
            public string RoleName { get; set; }
            public string Name { get; set; }

        }

        public class RoleDTO
        {
            [Key]
            public string Id { get; set; }
            [Display(Name = "Role Name")]
            public string RoleName { get; set; }

        }

        public class UserAndRolesDTO
        {

            [Key]
            [Display(Name = "User Name")]
            public string UserName { get; set; }
            public List<UserRoleDTO> ColUserRoleDTO { get; set; }

        }

        public class UserAndRolesDTONew
        {

            [Key]
            [Display(Name = "User Name")]
            public string Id { get; set; }
            public string Name { get; set; }
            public List<IdentityUserRole> ColUserRoleDTO { get; set; }

        }
    
}