﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TruckManagementSystem.Models;

namespace TruckManagementSystem.ViewModels
{
    public class StockLedgerViewModel
    {

        public DateTime? Created_Date { get; set; }

        public int Stock_Id { get; set; }

        public int Currency_Id { get; set; }

        public string Item_Name { get; set; }

        public int Location_Id { get; set; }

        public string Type { get; set; }

        public string UoM { get; set; }

        public decimal Qty_In { get; set; }

        public decimal Qty_Out { get; set; }

        public decimal Cost_In { get; set; }

        public decimal Cost_Out { get; set; }

        public decimal Qty_Bal { get; set; }

        public decimal Bal_Cost_TZS { get; set; }

        public decimal Bal_Cost_USD { get; set; }

        public int? Sent_Location { get; set; }

        public int Item_Id { get; set; }

        public int settled { get; set; }

        public decimal Buy_Price { get; set; }

        public string Currency_Name { get; set; }


        public decimal? Sell_Price { get; set; }
















        public string Location_String
        {
            get
            {
                if (Location_Id != 0)
                {
                    var db = new ApplicationDbContext();
                    var locs = db.Location.Find(Location_Id).Name;
                    return locs;

                }else
                {
                    return "";
                }
            }



        }


        public string Consumed_Location
        {

            get
            {
                var locs = string.Empty;
                if (Sent_Location != null)
                {
                    var db = new ApplicationDbContext();
                    locs = db.Location.Find(Sent_Location).Name;
                    return locs;
                }
                else
                {
                    locs = "";
                    return locs;

                }

            }


        }

    }
}